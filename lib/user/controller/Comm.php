<?php
namespace app\user\controller;
use think\Controller;
use think\Db;
class Comm extends Controller{
   	public function start(){
		if($this->request->instance()->isAjax()){
		$Url = param_url();
		$p=input('p/d',1);
		config('model',$this->url);
		$JumpUrl = param_jump($Url);
		$JumpUrl['p'] = 'zanpianpage';
		config('params',$JumpUrl);
		$result = array("ajaxtxt"=>'');
		$total = 0 ;
		if(!empty($Url['id'])){
            //获取其它数据信息
			$start = array();
			$markdata = model("VodMark")->getMark($Url['id']);
			$value = model("VodMark")->getMarkValue($Url['id'],get_client_ip());
			if($value > 0){
				$start['hadpingfen'] = 1 ;
				$start['mystars'] = $value ;
			}else{
				$start['mystars'] = 0 ;
			}
			$start['curpingfen'] = array();
			$vod_gold=Db::name('vod')->where(array('vod_id'=>$Url['id'],'vod_status'=>1))->value('vod_gold');
			if($markdata != null){
				$start['curpingfen']['a'] = $markdata['F5'];
				$start['curpingfen']['b'] = $markdata['F4'];
				$start['curpingfen']['c'] = $markdata['F3'];
				$start['curpingfen']['d'] = $markdata['F2'];
				$start['curpingfen']['e'] = $markdata['F1'];
				$rate = $this->getPingfen($markdata) ;
				if(count($rate) > 0){
					$start['curpingfen']['pinfenb'] = $rate['R2'];
					$start['curpingfen']['pinfen'] = $rate['R1'];
				}else{
					$start['curpingfen']['a'] = 0;
				    $start['curpingfen']['b'] = 0;
				    $start['curpingfen']['c'] = 0;
				    $start['curpingfen']['d'] = 0;
				    $start['curpingfen']['e'] = 0;
					$start['curpingfen']['pinfenb'] = $vod_gold*10;
					$start['curpingfen']['pinfen'] = $vod_gold;}
			}
		   $start['curpingfen']['num'] = $total;
			//是否收藏
			if(user_islogin()){
				$favorite=Db::name('favorite')->where(array('favorite_vid'=>$Url['id'],'favorite_uid'=>user_islogin()))->value('favorite_id');
				if($favorite){
					$start['loveid']=$Url['id'];
				}
				$remin=Db::name('remind')->where(array('remind_vid'=>$Url['id'],'remind_uid'=>user_islogin()))->value('remind_id');
				if($remin){
				$start['remindid']=$Url['id'];
				}
			}
			$result['star']=$start ;
	   }	
	return json($result);		
	}	
	}
   	public function cm(){
		if($this->request->instance()->isAjax()){
		$Url = param_url();
		$p=input('p/d',1);
		config('model',$this->url);
		$JumpUrl = param_jump($Url);
		$JumpUrl['p'] = 'zanpianpage';
		config('params',$JumpUrl);
		$result = array("ajaxtxt"=>'');
		$total = 0 ;
		if(!empty($Url['id']) && !empty($Url['sid'])){
			$userconfig=F('_data/userconfig_cache');
			$rs = model("Cm");
			$where = array('cm_vid'=>$Url['id'],'cm_sid'=>$Url['sid'],'cm_pid'=>0,'cm_status'=>1);
			$total = $rs->getcmcount($where);
			if($total > 0){
				$limit = $userconfig['user_cmnum'] ;
				$page = array('limit'=>$limit,'currentpage'=>$Url['p']);
				$list = $rs->getcomments($where,$page);  //查询所有结果集
				$data =$list->all();
				foreach ($list as $k => $v) {
                 $data[$k]['cm_sub'] = $rs->getpidcomments(array('cm_pid'=>$v['cm_id'],'cm_sid'=>$v['cm_sid'],'cm_status'=>1));
               }
			  // print_r($data) ;
			   $result['ajaxtxt'] = $this->outputPublicCommns($data,$list->total(),$limit,$p);
			   $pages="<span class='total'>共".$list->total()."条评论&nbsp;".$list->currentPage()."/".$list->lastPage()."页</span>";//数字分页
			   $pages.=getpage(config('model'),config('params'),$list->currentPage(),$list->total(),$limit,config('home_pagenum'));//数字分页
			   $pagebox = "<span class='total'>".$list->currentPage()."/".$list->lastPage()."</span>" ;
			   $pagebox.=gettoppage(config('model'),config('params'),$list->currentPage(),$list->lastPage(),config('home_pagenum'),$JumpUrl['url'],'pagego(\''.$JumpUrl['url'].'\','.$list->lastPage().')');//数字分页
			   $result['pagetop']=$pagebox;
			   $result['pages']=$pages;
			}
	   }	
	// print_r($result);  
	return json($result);		
	}	
	}	
   	public function get(){
		if($this->request->instance()->isAjax()){
		$Url = param_url();
		$p=input('p/d',1);
		config('model',$this->url);
		$JumpUrl = param_jump($Url);
		$JumpUrl['p'] = 'zanpianpage';
		config('params',$JumpUrl);
		$result = array("ajaxtxt"=>'');
		$total = 0 ;
		if(!empty($Url['id']) && !empty($Url['sid'])){
			$userconfig=F('_data/userconfig_cache');
			$rs = model("Cm");
			$where = array('cm_vid'=>$Url['id'],'cm_sid'=>$Url['sid'],'cm_pid'=>0,'cm_status'=>1);
			$total = $rs->getcmcount($where);
			if($total > 0){
				$limit = $userconfig['user_cmnum'] ;
				$page = array('limit'=>$limit,'currentpage'=>$Url['p']);
				$list = $rs->getcomments($where,$page);  //查询所有结果集
				$data =$list->all();
				foreach ($list as $k => $v) {
                 $data[$k]['cm_sub'] = $rs->getpidcomments(array('cm_pid'=>$v['cm_id'],'cm_sid'=>$v['cm_sid'],'cm_status'=>1));
               }
			  // print_r($data) ;
			   $result['ajaxtxt'] = $this->outputPublicCommns($data,$list->total(),$limit,$p);
			   $pages="<span class='total'>共".$list->total()."条评论&nbsp;".$list->currentPage()."/".$list->lastPage()."页</span>";//数字分页
			   $pages.=getpage(config('model'),config('params'),$list->currentPage(),$list->total(),$limit,config('home_pagenum'));//数字分页
			   $pagebox = "<span class='total'>".$list->currentPage()."/".$list->lastPage()."</span>" ;
			   $pagebox.=gettoppage(config('model'),config('params'),$list->currentPage(),$list->lastPage(),config('home_pagenum'),$JumpUrl['url'],'pagego(\''.$JumpUrl['url'].'\','.$list->lastPage().')');//数字分页
			   $result['pagetop']=$pagebox;
			   $result['pages']=$pages;
			}
            //获取其它数据信息
			$start = array();
			$markdata = model("VodMark")->getMark($Url['id']);
			$value = model("VodMark")->getMarkValue($Url['id'],get_client_ip());
			if($value > 0){
				$start['hadpingfen'] = 1 ;
				$start['mystars'] = $value ;
			}else{
				$start['mystars'] = 0 ;
			}
			$start['curpingfen'] = array();
			$vod_gold=Db::name('vod')->where(array('vod_id'=>$Url['id'],'vod_status'=>1))->value('vod_gold');
			if($markdata != null){
				$start['curpingfen']['a'] = $markdata['F5'];
				$start['curpingfen']['b'] = $markdata['F4'];
				$start['curpingfen']['c'] = $markdata['F3'];
				$start['curpingfen']['d'] = $markdata['F2'];
				$start['curpingfen']['e'] = $markdata['F1'];
				$rate = $this->getPingfen($markdata) ;
				if(count($rate) > 0){
					$start['curpingfen']['pinfenb'] = $rate['R2'];
					$start['curpingfen']['pinfen'] = $rate['R1'];
				}else{
					$start['curpingfen']['a'] = 0;
				    $start['curpingfen']['b'] = 0;
				    $start['curpingfen']['c'] = 0;
				    $start['curpingfen']['d'] = 0;
				    $start['curpingfen']['e'] = 0;
					$start['curpingfen']['pinfenb'] = $vod_gold*10;
					$start['curpingfen']['pinfen'] = $vod_gold;}
			}
		   $start['curpingfen']['num'] = $total;
			//是否收藏
			if(user_islogin()){
				$favorite=Db::name('favorite')->where(array('favorite_vid'=>$Url['id'],'favorite_uid'=>user_islogin()))->value('favorite_id');
				if($favorite){
					$start['loveid']=$Url['id'];
				}
				$remin=Db::name('remind')->where(array('remind_vid'=>$Url['id'],'remind_uid'=>user_islogin()))->value('remind_id');
				if($remin){
				$start['remindid']=$Url['id'];
				}
			}
			$result['star']=$start ;
	   }	
	// print_r($result);  
	return json($result);		
	}	
	}
	private function outputPublicCommns($list,$total,$limit,$p){
		$ajaxtxt = "" ;
		foreach ($list as $key =>$value){
			$avatar = ps_getavatar($value['cm_uid'],$value['pic'],$value['api'][0]['avatar']);
		    $image = $avatar['small'];
			$lou=$total-(($p-1)*$limit)-$key;
			$ajaxtxt.="<li id='li".$value['cm_id']."' class='comment-item fn-clear'>" ;
			$ajaxtxt.="<div class='comment-avatar'><a href='".zanpian_user_url('user/home/index',array('id'=>$value['cm_uid']))."' target='_blank'><img src='".$image."' alt='".$value['nickname']."'></a><div class='comment-lou'>".$lou."楼</div></div>";
			$ajaxtxt.="<div class='comment-post'>";
			$ajaxtxt.="<div class='comment-post-arrow'></div>";
			$ajaxtxt.="<div class='comment-post-cnt'>";
			$ajaxtxt.="<div class='comment-body'>";
			$ajaxtxt.="<div class='comment-top'><span class='user'><a href='".zanpian_user_url('user/home/index',array('id'=>$value['cm_uid']))."' target='_blank'>".$value['nickname']."</a></span>:<span class='time'>".date("m-d H:i",$value['cm_addtime'])."</span></div>";
			$ajaxtxt.="<div class='comment-text'>".get_emots(remove_xss(strip_tags($value['cm_content'])))."</div>";
			$ajaxtxt.="<div class='comment-assist fn-clear'>" ;
			$ajaxtxt.="<p class='fn-left'><span class='digg'><a href='javascript:void(0)' class='sup' data='".zp_url('user/comm/digg',array('id'=>$value['cm_id'],'type'=>"sup"))."'>支持(<strong>".$value['cm_support']."</strong>)</a></span><span class='digg'><a href='javascript:void(0)' class='opp' data='".zp_url('user/comm/digg',array('id'=>$value['cm_id'],'type'=>"opp"))."'>反对(<strong>".$value['cm_oppose']."</strong>)</a></span></p>" ;
			$ajaxtxt.="<p class='fn-right'><a href='javascript:void(0)' data-id='".$value['cm_id']."' data-tuid='".$value['cm_uid']."' class='reply'>回复</a></p>";
			$ajaxtxt.="</div>";
			$ajaxtxt.="<div id='rep".$value['cm_id']."' class='comms'></div>";
			$ajaxtxt.="</div>";
			if($value['cm_sub']){
				foreach ($value['cm_sub'] as $k =>$val){
				  $sublou=count($value['cm_sub'])-$k;
				  $ajaxtxt.="<div class='comment-sub'>";	
				  $ajaxtxt.="<div class='comment-top'><span class='sublou'>".$sublou."楼</span><span class='user'><a href='".zanpian_user_url('user/home/index',array('id'=>$val['cm_uid']))."' target='_blank'>".$val['nickname']."</a></span>:".gettan($val['cm_tid'])."<span class='time'>".date("m-d H:i",$val['cm_addtime'])."</span></div>";
			      $ajaxtxt.="<div class='comment-text'>".get_emots(remove_xss(strip_tags($val['cm_content'])))."</div>";
			      $ajaxtxt.="<div class='comment-assist fn-clear'>" ;
			      $ajaxtxt.="<p class='fn-left'><span class='digg'><a href='javascript:void(0)' class='sup' data='".zp_url('user/comm/digg',array('id'=>$val['cm_id'],'type'=>"sup"))."'>支持(<strong>".$val['cm_support']."</strong>)</a></span><span class='digg'><a href='javascript:void(0)' class='opp' data='".zp_url('user/comm/digg',array('id'=>$val['cm_id'],'type'=>"opp"))."'>反对(<strong>".$val['cm_oppose']."</strong>)</a></span></p>" ;
			      $ajaxtxt.="<p class='fn-right'><a href='javascript:void(0)' data-id='".$val['cm_id']."' data-pid='".$value['cm_id']."'  data-tuid='".$val['cm_uid']."' class='reply'>回复</a></p>";
		     	  $ajaxtxt.="</div>";
			      $ajaxtxt.="<div id='rep".$val['cm_id']."' class='comms'></div>";
			      $ajaxtxt.="</div>";
			   }
			}
	        // print_r($list);
			$ajaxtxt.="</div>";
			$ajaxtxt.="<div class='fn-clear'></div>" ;
			$ajaxtxt.="</div>";
			$ajaxtxt.="</li>";
		}
		return $ajaxtxt;
	}
    //获取评分
	private function getPingfen($data){
		$f1 = $data['F1'] ;
		$f2 = $data['F2'] ;
		$f3 = $data['F3'] ;
		$f4 = $data['F4'] ;
		$f5 = $data['F5'] ;
		$pftotal = $f1 + $f2 +  $f3 + $f4 + $f5 ;
		$array = array();
		if($pftotal > 0){
			$rating = ($f1/$pftotal)*1 + ($f2/$pftotal)*2  + ($f3/$pftotal)*3 + ($f4/$pftotal)*4 + ($f5/$pftotal)*5 ;
			$r1 = round($rating * 2,1) ;
			$array["R1"] = number_format($r1,1) ;
			$array["R2"] = $r1*10 ;
		}
		return $array;
	}
    //增加评分	
	public function mark(){	
		$vod_id = input('id/d',0);
		$value = input('val/d',0);
		$result =  array("msg"=>"提交评分失败","rcode" =>"-1");
		if(isset($vod_id) && isset($value)){
			if($value == "1" || $value == "2" || $value == "3" || $value == "4" || $value == "5"){
				//查询一下
				$rs=model("VodMark");
				$ip = ip2long(get_client_ip());
				$count = $rs->where(array('mark_vid'=>$vod_id,'mark_ip'=>$ip))->count();
				if($count > 0){ //存在
					$result['msg'] = "已经评分,请务重复评分" ;
					$result['rcode'] = -2 ;
				}else{ //增加评分
					$mark = array();
					$mark['mark_vid'] = $vod_id;
					$mark['mark_ip'] = $ip;
					$mark['mark_addtime'] = time();
					$mark['F'.$value] = 1 ;
					$id=$rs->insertGetId($mark);
					if($id>0){
						$result['msg'] = "提交评分成功" ;
						$result['rcode'] = 1 ;
						//
						$data = $rs->getMark($vod_id);
						$rate = $this->getPingfen($data) ;
						if(count($rate) > 0){
                        Db::name('vod')->where('vod_id',$vod_id)->setField('vod_gold',$rate['R1']);
						}
					}
					//重新计算一下
				}
			}
		}
		return json($result);
	}    
	//评论顶踩	
	public function digg(){
	    $Url = param_url();		
		$comment_id = $Url['id'];
		$type = $Url['type'];
		$success = false;
		//不需要登录
		if(!empty($comment_id) && !empty($type)){
			$ip = ip2long(get_client_ip());
			$count = Db::name('cm_opinion')->where(array('opinion_cmid'=>$comment_id,'opinion_ip'=>$ip))->count();
			if($count > 0){ //已经
				$result = array("msg"=>"已经点评","rcode" =>"-1");
				$success = true ;
			}else{
				$opinion = -1 ;
				$key = '';
				if($type == 'sup'){
					$opinion = 1 ;
					$key = 'cm_support' ;
				}else if($type == 'opp'){
					$opinion = 0 ;
					$key = 'cm_oppose' ;
				}
				if($opinion>=0){
					$data['opinion_cmid'] = $comment_id ;
					$data['opinion_opinion'] = $opinion ;
					$data['opinion_addtime'] = time() ;
					$data['opinion_ip'] = $ip ;
					$id = Db::name('cm_opinion')->insertGetId($data);
					if($id > 0){
						//修改值
						$addValue = Db::name('Cm')->where('cm_id',$comment_id)->setInc($key);
						$result = array("msg"=>"成功点评","rcode" =>"1");
						$success = true ;
					}
				}
			}
		}
		if(!$success){
			$result = array("msg"=>"点评失败","rcode" =>"-1");
		}
		return json($result);
	}
	public function savelove(){	
	if ($this->request->instance()->isAjax()){
	       $result = array("msg"=>"登录超时","rcode" =>-1);
	       $id = input('id/d',0); //影片ID
		   $cid = input('cid/d',0); //影片CID
		   $uid=user_islogin();
	       if($uid && $id && $cid){
			$favorite=Db::name('favorite')->where(array('favorite_vid'=>$id,'favorite_uid'=>$uid))->value('favorite_id');
			if($favorite){
				$result = array("msg"=>"已经收藏","rcode" =>-1,"yjdy"=>1);
			}else{
				$data['favorite_vid'] = $id ;
				$data['favorite_uid'] = $uid;
				$data['favorite_cid'] = getlistpid($cid);
				$data['favorite_addtime'] = time();
				$id = Db::name('favorite')->insertGetId($data);
				if($id > 0){
					$result['msg'] = "成功收藏" ;
					$result['rcode'] = 1 ;
				}	
		}
	  }
	  return json($result); 
	  }
  }	
	public function dellove(){
		 if ($this->request->instance()->isAjax()){
	    $id = input('id/d',0); //影片ID
		$cid = input('cid/d',0); //影片CID	
		$uid=user_islogin();
		$result = array("msg"=>"取消收藏失败","rcode" =>-1);
		if($id && $cid && $uid){
		$favorite=Db::name('favorite')->where(array('favorite_vid'=>$id,'favorite_uid'=>$uid))->delete();
		if($favorite){
		$result = array("msg"=>"取消收藏成功","rcode" =>1);
		}
		}
		return json($result);
		}
	}  
     public function saveremind(){
		   if ($this->request->instance()->isAjax()){
		   $result = array("msg"=>"登录超时","rcode" =>-1);
	       $id = input('id/d',0); //影片ID
		   $cid = input('cid/d',0); //影片CID
		   $uid=user_islogin();
	       if($uid && $id && $cid){
			$uid=user_islogin();
			$favorite=Db::name('remind')->where(array('remind_vid'=>$id,'remind_uid'=>$uid))->value('remind_id');
			if($favorite){
				$result = array("msg"=>"已经订阅","rcode" =>-1,"yjdy"=>1);
			}else{
				$vod_addtime=get_vod_info($id,'vod_id','vod_addtime');
				$data['remind_vid'] = $id ;
				$data['remind_uid'] = $uid;
				$data['remind_cid'] = getlistpid($cid);
				$data['remind_addtime'] = time();
				$data['remind_uptime'] = $vod_addtime;
				$id = Db::name('remind')->insertGetId($data);
				if($id > 0){
					$result['msg'] = "成功订阅" ;
					$result['rcode'] = 1 ;
				}	
		}
	  }
	  return json($result);
	  }
  }	
	public function delremind(){
		if ($this->request->instance()->isAjax()){
	    $id = input('id/d',0); //影片ID
		$cid = input('cid/d',0); //影片CID	
		$uid=user_islogin();
		$result = array("msg"=>"取消订阅失败","rcode" =>-1);
		if($id && $cid && $uid){
		$remind=Db::name('remind')->where(array('remind_vid'=>$id,'remind_uid'=>$uid))->delete();
		if($remind){
		$result = array("msg"=>"取消订阅成功","rcode" =>1);
		}
		}
		return json($result);
		}
	}
	public function getplaylog(){
		if(user_islogin() && $this->request->instance()->isAjax()){
			$where = array('log_uid'=>user_islogin());
		    $page = array('limit'=>4,'currentpage'=>$Url['p']);
			$rs = model("Playlog");
		    $list = $rs->getplaylog($where,$page);//查询所有结果集
			if($list->total() > 0){
				$array = array();
				foreach ($list as $key =>$value){
					$data[$key]['id'] = $value['log_id'];
					$data[$key]['vod_name'] = $value['vod_name'];
					$data[$key]['url_name'] = $value['log_urlname'];
					$data[$key]['vod_readurl'] = zanpian_data_url('home/vod/read',array('id'=>$value['log_vid'],'pinyin'=>$value['vod_letters'],'cid'=>$value['vod_cid'],'dir'=>getlistname($value['vod_cid'],'list_dir'),'jumpurl'=>$value['vod_jumpurl']));
					$data[$key]['vod_palyurl'] = zanpian_play_url(array('id'=>$value['log_vid'],'pinyin'=>$value['vod_letters'],'cid'=>$value['vod_cid'],'dir'=>getlistname($value['vod_cid'],'list_dir'),'sid'=>$value['log_sid'],'pid'=>$value['log_pid']));
					if($value['log_pid'] < $value['log_maxnum']){
					$data[$key]['vod_playnext']=zanpian_play_url(array('id'=>$value['log_vid'],'pinyin'=>$value['vod_letters'],'cid'=>$value['vod_cid'],'dir'=>getlistname($value['vod_cid'],'list_dir'),'sid'=>$value['log_sid'],'pid'=>$value['log_pid']+1));
					}
				}
				$result["r"] =  $data;
				$result["rcode"] =  $list->total() ;
			}
	  }else{
		   $result["rcode"] = -1;
		  }
	    return json($result);
	}		
	public function addplaylog(){
		if ($this->request->isPost() && user_islogin()){
		    $data=input('post.vod_id/d');	
			$vod_id = input('post.vod_id/d');
			Db::name('playlog')->where(array("log_vid"=>$vod_id,"log_uid"=>user_islogin()))->delete(); //先删除
			$data  = array();
			$data["log_vid"] = $vod_id ;
			$data["log_sid"] = input('vod_sid/d');
			$data["log_pid"] = input('vod_pid/d') ;
			$data["log_urlname"] = input('url_name/s');
			$data["log_maxnum"] = input('vod_maxnum/d');
			$data["log_uid"] = user_islogin();
			$data["log_addtime"] = time() ;
			Db::name('playlog')->insert($data);
	   }else{
		  return json(array("rcode" =>"-1")) ;
		   }
	}	
	public function emptyhistory(){
		$result =  array("rcode" =>"-1");
		if(user_islogin()){
			Db::name('playlog')->where('log_uid',user_islogin())->delete();
			$result =  array("rcode" =>"1");
		}
		return json($result) ;
	}	
	public function delplaylog(){
	    $id=input('id/d');	
		$result =  array("rcode" =>"-1");
		if(user_islogin() && !empty($id)){
			Db::name('playlog')->where(array('log_id'=>$id,'log_uid'=>user_islogin()))->delete();
			$result =  array("rcode" =>"1");
		}
		return json($result) ;
	}		
}
// +----------------------------------------------------------------------
// | ZanPianCMS [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.zanpian.com All rights reserved.
// +----------------------------------------------------------------------
// | BBS:  <http://www.feifeicms.cc>
// +----------------------------------------------------------------------

