<?php
namespace app\user\controller;
use think\Controller;
class Playlog extends Controller{
    public function get(){
		//print_r(cookie('zanpian_playlog')) ;
		$list=model('playlog')->find();
        $this->assign('list',$list);
		return view('/playlog/get');
    }
    public function set(){
		    if(request()->isPost()){
            $data['log_vid'] = input('log_vid/d');
			$data['log_sid'] = input('log_sid/d');
			$data['log_pid'] = input('log_pid/d') ;
			$data['log_urlname'] = input('log_urlname/s');
			$data['log_maxnum'] = input('log_maxnum/d');
			return model('playlog')->add($data);
			}
    }
    public function clear(){
		if(user_islogin()){	
		   $result=db('playlog')->where('log_uid',user_islogin())->delete(); 	
		}else{
		   cookie('zanpian_playlog',null);
		}
		return ['msg'=>'清空成功','rcode' =>'1']; 
    }
    public function del(){
		    if(request()->isPost()){
            $data['log_id'] = input('log_id/d');
			$data['log_vid'] = input('log_vid/d');
			return model('playlog')->del($data);
			}
    }		

}
// +----------------------------------------------------------------------
// | ZanPianCMS [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.zanpian.com All rights reserved.
// +----------------------------------------------------------------------
// | BBS:  <http://www.feifeicms.cc>
// +----------------------------------------------------------------------
