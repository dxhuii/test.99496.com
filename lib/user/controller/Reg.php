<?php
namespace app\user\controller;
use app\common\controller\User;
use app\common\library\Email;
class Reg extends User{
	public function ajax() {
		return view('reg/ajax');
	}	
	public function index() {
		$userconfig=F('_data/userconfig_cache');
		$auth_key=!empty($userconfig['user_key']) ? $userconfig['user_key'] : 'zanpiancms'; 
		if($userconfig['user_email_auth'] && session('user_auth.groupid')==1 || session('user_auth_temp.groupid')==1){
			return $this->redirect(zanpian_user_url('user/reg/auth'));
		}
		if($userconfig['user_reg']){
		   $user_api=session('user_auth_api');
		   if ($this->request->isPost()){
				   $data=input('post.');
				   if($userconfig['user_code']){
			          //验证码验证
				       $result=$this->checkcode($data['validate']);	
				       if($result!=1){
					       return json(['msg'=>"验证码错误",'rcode'=>-1]);  
		               }
				   }
				   $result = $this->validate($data,'User.reg');
				   if($result!=1){
					       return json(['msg'=>$result,'rcode'=>-1]);  
				   }
		           $user = model('User');
			       $info  = $user->reg($data['username'],$data['email'],$data['password']);
				   if($info['userid']){
					   //判断是否开启邮件认证
					  if($userconfig['user_email_auth'] && empty($user_api['openid'])){
						$email = new Email;  
						$result = $email->regsendemail();
		                if($result){
			                return json(['msg'=>"注册成功,邮件发送成功",'rcode'=>1,'redir'=>zanpian_user_url('user/reg/auth'),'wantredir'=>1]); 
						    }else{
			                return json(['msg'=>"注册成功,邮件发送失败,请联系管理员",'rcode'=>-1,'redir'=>zanpian_user_url('user/reg/auth'),'wantredir'=>1]);
	                    }
					  } 
					  //是否需要跳转
					  if($data['jump']){
					     return json(['msg'=>"注册成功",'rcode'=>1,'redir'=>zanpian_user_url('user/center/index'),'wantredir'=>1]);
					  }else{
						 return json(['msg'=>"注册成功",'rcode'=>1]);
					  }
		           }
		     }else {
				if(!empty($user_api['openid']) && !empty($userconfig['api_login_auth'])){
				   //print_r($user_api) ;
			       $this->assign($user_api);
				   return view('/reg_api');
				}else{
				   $Url = param_url();
				   if($Url['uid']){
					   cookie('zanpiancms_register_pid', intval($Url['uid']), time()+86400);
		           }
			       return view('/reg');
				}
		     }
		}else{
			return $this->error('对不起,暂未对外开放用户注册!',config('site_url'));
	    }
	}
	public function agreement() {
	    return view('/agreement');	
	}
	public function code() {
		$code=htmlspecialchars(input('verify/s',''));
		$userconfig=F('_data/userconfig_cache');
		$auth_key=!empty($userconfig['user_key']) ? $userconfig['user_key'] : 'zanpiancms'; 
	    $cms_auth_key = md5($auth_key);
	    $code_res = sys_auth($code,'DECODE',$cms_auth_key);
		if(empty($code_res)){
		   return $this->error('验证地址解析错误请重新发送验证邮件',zanpian_user_url('user/login/auth'));	
		}
		$code_arr = explode('|', $code_res);
		$userid = isset($code_arr[0]) ? $code_arr[0] : '';
		$codetime=isset($code_arr[2]) ? $code_arr[2] : 0;
		$codecache=cache('user_regverify_'.$userid);
		if($codecache!=$code){
		   return $this->error('验证地址已失效',zanpian_user_url('user/reg/auth'));	
		}
		if(time()-$codetime > 3600){
		   return $this->error('验证连接超时请重新发送验证邮件',zanpian_user_url('user/reg/auth'));	
		}else{	
		$rs=model('User');
		$user =$rs->where('userid',$userid)->find();
		if($user){
		$rs->where('userid',$user['userid'])->update(['groupid' =>2,'iemail'=>$user['email']]);
		/* 记录登录SESSION和COOKIES */
		$auth = array(
			'userid'  => $user['userid'],
			'username'  => $user['username'],
			'nickname'  => $user['nickname'],
			'useremail'  => $user['email'],
			'groupid'  => 2,
			'lastdate' => $user['lastdate'],
			'lastip' => $user['lastip'],
			'user_email_auth' => $userconfig['user_email_auth'],
		);
		cookie('user_auth', $auth);
		cookie('user_auth_sign', data_auth_sign($auth));
		session('user_auth', $auth);
		session('user_auth_sign', data_auth_sign($auth));
		session('user_auth_temp',$null);
		}else{
			return $this->redirect(zanpian_user_url('user/login/index'));	
			}
		}		
		cache('user_regverify_'.$user['userid'],NULL);
		if(user_islogin()){
		    return $this->redirect(zanpian_user_url('user/center/index'));
		}else{
		    return $this->redirect(zanpian_user_url('user/login/index'));	
		}
	}	
	public function auth() {
	    $userconfig=F('_data/userconfig_cache');
		$session=session('user_auth_temp');
		if(!$session['userid']){
		  return $this->redirect(zanpian_user_url('user/login/index'));			
		}
		if($userconfig['user_email_auth'] && $session['groupid']==1){
			$user['userid']=$session['userid'];
			$user['email']=$session['useremail'];
			$user['username']=$session['username'];
			$this->assign($user);
			return view('/auth');
	     }else{
		    return $this->redirect(zanpian_user_url('user/center/index'));	 
		 }	
	}
	public function send(){
	     $userconfig=F('_data/userconfig_cache');
		 $sessions=session('user_auth');
	     $session=session('user_auth_temp');
		  if(!empty($userconfig['user_email_auth']) && $sessions['groupid']==2){ 
			 return json(['msg'=>"邮箱地址已验证",'rcode'=>1,"redir"=>zanpian_user_url('user/center/index'),"wantredir"=>1]); 
		  }	
		  if(!$session['userid']){
		     return $this->redirect(zanpian_user_url('user/login/index'));			
		  }			  
	     if($this->request->isPost()){
          $email = new Email;			 
		  $result = $email->regsendemail();
		  if($result){
			 return json(['msg'=>"发送成功",'rcode'=>1]);
		  }else{
			 return json(['msg'=>"发送失败,请联系管理员",'rcode'=>-1]); 
	      }
	    }else{
		  return $this->redirect(zanpian_user_url('user/login/index'));	  
		}			
    }
}
// +----------------------------------------------------------------------
// | ZanPianCMS [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.zanpian.com All rights reserved.
// +----------------------------------------------------------------------
// | BBS:  <http://www.feifeicms.cc>
// +----------------------------------------------------------------------
