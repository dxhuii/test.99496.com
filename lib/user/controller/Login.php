<?php
namespace app\user\controller;
use app\common\library\Email;
use app\common\controller\User;	
class Login extends User{
	public function ajax() {
		return view('login/ajax');
	}
	public function index($username = '', $password = '', $validate = '',$jump=false) {
		if(user_islogin()){
			return $this->redirect(zanpian_user_url('user/center/index'));
		}
		$userconfig=F('_data/userconfig_cache');
		if ($this->request->isPost()){
			if (!$username || !$password) {
				return $this->error('用户名或者密码不能为空！', '');
			}
			 if($userconfig['user_code']){
			    //验证码验证
			    $result=$this->checkcode($validate);	
				if($result!=1){
				   return json(['msg'=>"验证码错误",'rcode'=>-1]);  
		        }
			}   
			$user = model('User');
			$info  = $user->login($username,$password);
			if($info['userid']>0){
				//判断是否开启邮箱认证
				if($userconfig['user_email_auth'] && $info['groupid']==1){
				return json(array("msg"=>"登录成功,邮箱需要验证","rcode" =>"-1","redir"=>zanpian_user_url('user/reg/auth'),"wantredir"=>1));	
				}
				//是否需要跳转
				if($jump){
					return json(['msg'=>"登录成功",'rcode'=>1,'redir'=>zanpian_user_url('user/center/index'),'wantredir'=>1]);
				}else{
					return json(['msg'=>"登录成功",'rcode'=>1]);
				 }				
			}else {
				switch ($info) {
				case -1:$error = '用户不存在！';
					break; //系统级别禁用
				case -2:$error = '密码错误！';
					break;
				case -3:$error = '用户被锁定！';
					break;					
				default:$error = '未知错误！';
					break; // 0-接口参数错误（调试阶段使用）
				}
				return json(["msg"=>$error,"rcode" =>-1]);
			}
		} else {
			return view('/login');
		}
	}	
  public function forgetpwd($email='',$validate='') {
	     $userconfig=F('_data/userconfig_cache');
	     if ($this->request->isPost()){
			 $emailarray = explode("@",$email);
			 $rs=model('User');
             $result=$this->checkcode($validate);	
			 if($result!=1){
				   return json(['msg'=>"验证码错误",'rcode'=>-1]);  
		     }			 
			 $user =$rs->where('email',$email)->find();
			 $email = new Email; 
			 $result = $email->forgetpwdemail($user);
			if($result){
			   return json(['msg'=>"系统已经将链接发至您的邮箱，请登录您邮箱进行密码重置",'rcode'=>1,'redir'=>"http://mail.".$emailarray[1],'wantredir'=>1]);
			}
			else{
			   return json(["msg"=>$email."请联系网站管理","rcode" =>"-1"]);				
			}
			 
		 }
         return view('/forgetpwd');
	}			
	
   public function repass() {
	   	   $code=htmlspecialchars(input('code/s',''));
		   $userconfig=F('_data/userconfig_cache');
		   $auth_key=!empty($userconfig['user_key']) ? $userconfig['user_key'] : 'zanpiancms'; 
	       $cms_auth_key = md5($auth_key);
	       $code_res = sys_auth($code,'DECODE',$cms_auth_key);
		   if(empty($code_res)){
		       return $this->error('验证地址错误请重新找回密码',zanpian_user_url('user/login/forgetpwd'));	
		   }
		   $code_arr = explode('#', $code_res);
		   $userid = isset($code_arr[0]) ? $code_arr[0] : '';
		   $codetime=isset($code_arr[1]) ? $code_arr[1] : 0;	   
		   $codecache=cache('user_forgetpwd_'.$userid);
		    if($codecache!=$code){
			   return $this->error('验证连接已使用，请重新找回密码',zanpian_user_url('user/login/forgetpwd'));	
			}
	        if($this->request->isPost()){
			$data['password']=htmlspecialchars(input('password/s'));
			$data['repassword']=htmlspecialchars(input('repassword/s'));
			$data['encrypt'] = rand_string(6);
			$data['userid']=$userid;
			$result = $this->validate($data,'user.password');
			if($result!=1){
				return json(['msg'=>$result,'rcode'=>-1]);  
			}
			$rs = model('User');
			$rs->save($data,array('userid'=>$userid));
			cache('user_forgetpwd_'.$userid,NULL);
			if($rs){
			    return json(["msg"=>"密码重置成功，请使用新密码登录!","rcode" =>"1","redir"=>zanpian_user_url('user/login/index'),"wantredir"=>1]);	
			}else{
			    return json(["msg"=>"密码重置失败，请重新获取找回验证码","rcode" =>"-1","redir"=>zanpian_user_url('user/login/forgetpwd'),"wantredir"=>1]);	
			}	
		}else{
		   if(time()-$codetime > 3600){
		      return $this->error('验证连接超时请重发验证码',zanpian_user_url('user/login/forgetpwd'));	
		   }else{
		      return view('/repass');
		    }
	   }
	}		
}
// +----------------------------------------------------------------------
// | ZanPianCMS [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.zanpian.com All rights reserved.
// +----------------------------------------------------------------------
// | BBS:  <http://www.feifeicms.cc>
// +----------------------------------------------------------------------
