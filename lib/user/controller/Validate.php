<?php
namespace app\user\controller;
use think\Controller;
class Validate extends Controller{ 
	public function index(){
		$data['email']=input('email/s','');
        $data['username']=input('username/s','');
        $data['imgcode']=input('imgcode/s','');
		if($data['email']){
		$result = $this->validate($data,'User.email');
		}
		if($data['username']){
		$result = $this->validate($data,'User.username');	
		}
		if($data['imgcode']){
		$result=$this->checkcode($data['imgcode']);	
		}
		if($result==1){
		$info['info']=1;
		$info['status']=1;
		$info['data']=$result;
		}else{
        $info['info']=0;
		$info['status']=0;	
		$info['data']=$result;
		}
		return json($info);
		
	}
	public function useranme($username='') {
	}
	public function imgcode($imgcode='') {
		
	}
	/**
	 * 验证码
	 * @param  integer $id 验证码ID
	 */
	function verify($id = 1) {
        $verify = new \org\Verify(array('length' => 4,'imageH'=>35,'imageW'=>120,'fontSize'=>14,'reset' =>false,'expire'=>60));
		$verify->entry($id);
	}
    /**
	 * 检测验证码
	 * @param  integer $id 验证码ID
	 * @return boolean     检测结果
	 */
	function checkVerify($code, $id = 1) {
		if ($code) {
			$verify = new \org\Verify();
			$result = $verify->check($code, $id);
			if (!$result) {
				return $this->error("验证码错误！");
			}
		} else {
			return $this->error("验证码为空！");
		}
	}
	function checkcode($code, $id = 1) {
		if ($code) {
			$verify = new \org\Verify(array('reset' =>false));
			$result = $verify->check($code, $id);
			if (!$result) {
				return "验证码错误";
			}else{
			    return true;
			}
		} else {
			return "验证码为空";
		}
	}			
}
// +----------------------------------------------------------------------
// | ZanPianCMS [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.zanpian.com All rights reserved.
// +----------------------------------------------------------------------
// | BBS:  <http://www.feifeicms.cc>
// +----------------------------------------------------------------------
