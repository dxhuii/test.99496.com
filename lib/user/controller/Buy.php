<?php
namespace app\user\controller;
use app\common\controller\User;
class Buy extends User{	
    public function index(){
		if ($this->request->isPost()){
			if(!user_islogin()){
			    $result = array('msg'=>'请先登录','rcode' =>-1);
			}else{
			$user = db('user')->field('userid,score,viptime')->find(user_islogin());
		    if(!$user){
			    $result = array('msg'=>'用户不存在','rcode' =>-1);
		    }
               if(input('post.vip_day/d')>=1){				   
			    $result = model('Score')->user_viptime($user['userid'], $user['viptime'], $user['score'], intval(input('post.vip_day/d')));
			   }else{
				$result = array('msg'=>'购买天数必须大于1','rcode' =>-1);
			   }
			}
		    return json($result);
		}else{
		    return view('index');
		}
		
		
    }

}
// +----------------------------------------------------------------------
// | ZanPianCMS [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.zanpian.com All rights reserved.
// +----------------------------------------------------------------------
// | BBS:  <http://www.feifeicms.cc>
// +----------------------------------------------------------------------