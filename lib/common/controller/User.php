<?php
namespace app\common\controller;
use app\common\controller\All;
class User extends All{
	protected $url;
	protected $module;
	protected $controller;
	protected $action;
	protected $userinfo;
    public function _initialize() {
		parent::_initialize();
		//获取request信息
		$this->requestInfo();
		$userconfig=F('_data/userconfig_cache'); 
		//检查登录状态
		if (!user_islogin() and !in_array($this->url, array('user/home/index','user/login/index','user/login/ajax','user/login/verify','user/reg/index','user/reg/ajax','user/reg/agreement','user/reg/verify','user/reg/code','user/reg/auth','user/reg/send','user/login/forgetpwd','user/login/repass','user/login/login','user/center/getplaylog','user/center/addplaylog','user/center/flushinfo')) and !in_array($this->module.'/'.$this->controller, array('user/validate','user/avatar','user/home','user/snslogin'))){
		  return $this->redirect(zanpian_user_url('user/login/index'));
		}
		$this->check_member();
	}	
	//request信息
	protected function requestInfo(){
		$this->assign('user_nav',F('_data/usernavcenter'));
		$this->assign('user_nav_index',F('_data/usernavindex'));
		$this->assign($this->Lable_Style());
		$this->module = strtolower($this->request->module());
		$this->controller = strtolower($this->request->controller());
		$this->action = strtolower($this->request->action());
		$this->url = strtolower($this->request->module() . '/' . $this->request->controller() . '/' . $this->request->action());
	}
	
    final public function check_member() {
		$session=session('user_auth');
	    $rs = model("User");
		$userinfo=$rs->getuserinfo($session['userid']);
		$this->assign("avatarlist",ps_getavatar($userinfo['userid'],$userinfo['pic'],$userinfo['api'][0]['avatar']));
		$this->assign("userinfo",$userinfo);
		$this->userinfo=$userinfo;
		if(!empty($userinfo['api'])){
			foreach ($userinfo['api'] as $key=>$value){
				$syncs[$value['channel']]=$value['openid'];
			
			}
		$this->assign("syncs",$syncs);	
		}
	 }
	/**
	 * 验证码
	 * @param  integer $id 验证码ID
	 */
	public function verify($id = 1) {
        $verify = new \org\Verify(array('length' => 4,'imageH'=>35,'imageW'=>120,'fontSize'=>14,'reset' =>false,'expire'=>60));
		$verify->entry($id);
	}
    /**
	 * 检测验证码
	 * @param  integer $id 验证码ID
	 * @return boolean     检测结果
	 */
	public function checkVerify($code, $id = 1) {
		if ($code) {
			$verify = new \org\Verify();
			$result = $verify->check($code, $id);
			if (!$result) {
				return $this->error("验证码错误！");
			}
		} else {
			return $this->error("验证码为空！");
		}
	}
	public function checkcode($code, $id = 1) {
		if ($code) {
			$verify = new \org\Verify(array('reset' =>false));
			$result = $verify->check($code, $id);
			if (!$result) {
				return "验证码错误";
			}else{
			    return true;
			}
		} else {
			return "验证码为空";
		}
	}				 
	protected function Sign_out(){
		    cookie('user_auth', null);
		    cookie('user_auth_sign', null);
		    session('user_auth', null);
		    session('user_auth_sign', null);
			session('user_auth_temp',null);
	}
}
?>