<?php
namespace app\common\library;
class Baidu{	
    public function baidutui($urls){
        if(is_array($urls)){
             $urls = $urls;
        }else{
              $urls = array(rtrim(rtrim(config('site_url'),'/').config('site_path'), '/').$urls);	
        }	
        $api = config('baidu_tui');
        $ch = curl_init();
        $options =  array(
        CURLOPT_URL => $api,
        CURLOPT_POST => true,
        CURLOPT_RETURNTRANSFER => true,
	    CURLOPT_CONNECTTIMEOUT => 8,
	    CURLOPT_TIMEOUT => 6,
        CURLOPT_POSTFIELDS => implode("\n", $urls),
        CURLOPT_HTTPHEADER => array('Content-Type: text/plain'),
        );
        curl_setopt_array($ch, $options);
        $result = curl_exec($ch);
        $success=json_decode($result,true);
        curl_close($ch);
        if($success['success']){
           return "百度数据推送<font color=red>".$success['success']."</font>条，今日剩余<font color=red>".$success['remain']."</font>条";
        }else{
           return "百度数据推送失败";
        }
    }
 
}
