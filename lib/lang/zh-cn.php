<?php
// +----------------------------------------------------------------------
// | SentCMS [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.tensent.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: molong <molong@tensent.cn> <http://www.tensent.cn>
// +----------------------------------------------------------------------

return array(
	'zanpiancms_version'   => 'v8.20190419',
	'zanpiancms_name'   => '赞片CMS',
	'zanpiancms_title'   => 'ZanPianCms',
	'zanpiancms_url'   => 'http://www.zanpiancms.com/',
	'zanpiancms_bbs'   => 'http://bbs.zanpiancms.com/',
);