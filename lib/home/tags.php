<?php
return [
    'module_init' =>[
        'app\\home\\behavior\\ReadHtmlCacheBehavior','app\\home\\behavior\\SetTheme',
    ],
    'view_filter' => [
        'app\\home\\behavior\\WriteHtmlCacheBehavior',
    ],
];