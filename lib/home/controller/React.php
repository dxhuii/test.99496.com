<?php
namespace app\home\controller;

use app\common\controller\Home;
use app\common\controller\User;
use app\common\library\Email;
use think\Db;
use think\Cache;
use think\Request;

class React extends Home
{
	public function index()
	{
		return 'react';
	}

	// 各种配置参数
	public function config()
	{
		$array = array();
		$list = array();
		$Url = param_url();
		$name = $Url['name'];
		if ($name == 'menu' || $name == 'list') {
			$menu = F('_data/listtree');
			foreach ($menu as $i => $val) {
				$listMenu[$i]['id'] = $val['list_id'];
				$listMenu[$i]['title'] = $val['list_name'];
				$listMenu[$i]['name'] = $val['list_dir'];
				if ($val['son']) {
					foreach ($val['son'] as $k => $v) {
						$listMenu[$i]['son'][$k]['id'] = $v['list_id'];
						$listMenu[$i]['son'][$k]['title'] = $v['list_name'];
						$listMenu[$i]['son'][$k]['name'] = $v['list_dir'];
					}
				}
			}
		}
		if ($name == 'menu') {
			$array = $listMenu;
		} elseif ($name == 'sns') {
			$array = get_addon_config('snslogin');
		} elseif ($name == 'user') {
			$array = F('_data/userconfig_cache');
		} elseif ($name == 'pay') {
			$array = F('_data/payconfig_cache');
		} elseif ($name == 'emot') {
			$array = F('_data/list_emots');
		} elseif ($name == 'list') {
			$k = 0;
			foreach (F('_data/mcid') as $i => $val) {
				if ($val['m_list_id'] == 3) {
					$list[$k]['id'] = $val['m_cid'];
					$list[$k]['title'] = $val['m_name'];
					$list[$k]['name'] = $val['m_ename'];
					$list[$k]['order'] = $val['m_order'];
					$k = $k + 1;
				}
			}
			array_multisort(array_column($list, 'order'), SORT_ASC, $list); // 按order排序
			$array['mcid'] = $list;
			$array['menu'] = $listMenu[0];
			$array['area'] = explode(',', config('play_area'));
			// $array['language'] = explode(',',config('play_language'));
			$array['year'] = explode(',', config('play_year'));
			$array['letter'] = (range(A, Z));
			// $array['order'] = array('addtime','hits','gold');
			// $array['areaAll'] = F('_data/area');
			// $array['picm'] = array('1','2');
		} else {
			$array['domain'] = strtolower($this->request->domain());
			$array['title'] = config('site_title');
			$array['name'] = config('site_name');
			$array['url'] = config('site_url');
			$array['murl'] = config('mobile_url');
			$array['hotkey'] = hot_keywords(config('site_hotkeywords'));
			$array['keywords'] = config('site_keyword');
			$array['description'] = config('site_description');
			$array['email'] = config('site_email');
			$array['copyright'] = config('site_copyright');
			$array['tongji'] = config('site_tongji');
			$array['video_copyright'] = config('copyright_txt');
		}
		return json(['data' => $array, 'info' => 'ok', 'status' => 1]);
	}

	// 轮播图 循环查询相关标签幻灯片
	function slide()
	{
		$data = zanpian_mysql_slide('limit:6;nocid:-1;order:slide_oid asc');
		if ($data) {
			foreach ($data as $i => $val) {
				$list[$i]['title'] = $val['slide_name'];
				$list[$i]['pic'] = $val['slide_pic'];
				$list[$i]['url'] = $val['slide_url'];
			}
			return json(['data' => $list, 'info' => 'ok', 'status' => 1]);
		} else {
			return json(['data' => [], 'info' => 'error', 'status' => 0]);
		}
	}
	
	/**
	 * 视频详情接口
	 * @param id 视频ID
	 * @param pinyin 视频拼音
	 */
	public function Detail()
	{
		$Url = param_url();
		if (!empty($Url['pinyin']) && empty($Url['id'])) {
			$Url['id'] = get_vod_info($Url['pinyin'], 'vod_letters', 'vod_id');
		}
		$array_detail = $this->get_cache_detail($Url['id']);
		$vid = $Url['id'];
		if ($array_detail) {
			$show = $array_detail['show'];
			$read = $array_detail['read'];
			$list['listId'] = $show['list_id'];
			$list['listName'] = $show['list_name'];
			$list['listPid'] = $show['list_pid'];
			$list['listNameBig'] = $show['list_name_big'];
			$list['id'] = $read['vod_id'];
			$list['cid'] = $read['vod_cid'];
			$list['title'] = $read['vod_name'];
			$list['aliases'] = $read['vod_aliases'];
			$list['keywords'] = $read['vod_keywords'];
			$list['area'] = $read['vod_area'];
			$list['year'] = $read['vod_year'];
			$list['mcid'] = mcat_json($read['vod_mcid'], $read['vod_cid']);
			$list['time'] = zanpian_from_time($read['vod_addtime']);
			$list['updateDate'] = date('Y-m-d H:i:s', $read['vod_addtime']);
			$list['language'] = $read['vod_language'];
			if (!empty($read['vod_original'])) {
				$list['original'] = get_star_json($read['vod_original']);
			}
			if (!empty($read['vod_actor'])) {
				$list['actor'] = get_star_json($read['vod_actor']);
			}
			if (!empty($read['vod_director'])) {
				$list['director'] = get_star_json($read['vod_director']);
			}
			$list['total'] = empty($read['vod_total']) ? 0 : (int)$read['vod_total'];
			$list['gold'] = $read['vod_gold'];
			$list['filmtime'] = empty($read['vod_filmtime']) ? '' : date('Y-m-d', $read['vod_filmtime']);
			$list['company'] = $read['vod_company'];
			$list['website'] = $read['vod_website'];
			$list['tvcont'] = $read['vod_tvcont'];
			$list['repair'] = $read['vod_repair'];
			$list['repairtitle'] = $read['vod_repairtitle'];
			$list['pan'] = $read['vod_pan'];
			$list['pantitle'] = $read['vod_pantitle'];
			$list['letters'] = $read['vod_letters'];
			$list['hits'] = $read['vod_hits'];
			$list['content'] = preg_replace("/<(.*?)>/", '', $read['vod_content']);
			$list['pic'] = $read['vod_picurl'];
			if (is_numeric($read['vod_continu'])) {
				if ($read['vod_continu'] > 0) {
					$read['vod_continu'] = '连载至' . $read['vod_continu'] . '话';
				} else {
					$read['vod_continu'] = '完结';
				}
			}
			$list['status'] = $read['vod_continu'];
			$list['storyId'] = $read['story_id'];
			$list['actorId'] = $read['actor_id'];
			$list['copyright'] = $read['vod_reurl'];
			$list['jump'] = $read['vod_jumpurl'];
			$newsText = $this->newsListData($read['vod_name'], $read['vod_id'], '214,215,216,217,218,219,220,223', 1, 100);
			$newsPic = $this->newsListData($read['vod_name'], $read['vod_id'], '211,206,205,207,208,209,212,213,221,222');
			$story = $this->storydetaillist($vid, 1);
			$role = $this->roleListData($vid);
			if ($newsText) {
				$list['newsTextlist'] = $newsText;
			}
			if ($newsPic) {
				$list['newsPiclist'] = $newsPic;
			}
			if ($story) {
				$list['storylist'] = $story;
			}
			if ($role) {
				$list['role'] = $role;
			}
			return json(['data' => $list, 'info' => 'ok', 'status' => 1]);
		} else {
			return json(['data' => [], 'info' => 'error', 'status' => 0]);
		}
	}

	// 获取播放数据全部JSON
	public function play()
	{
		header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
		header('Access-Control-Allow-Methods: GET, POST');
		$Url = param_url();
		$pid = $Url['pid'];
		if (!empty($Url['pinyin']) && empty($Url['id'])) {
			$Url['id'] = get_vod_info($Url['pinyin'], 'vod_letters', 'vod_id');
		}
		if (config('url_cache_on') && config('url_cache_play')) {
			Request::instance()->cache('url_cache_vod_playjs_' . $Url['id'], config('url_cache_play'), '', 'url_cache_vod_' . $Url['id']);
		}
		$array_detail = $this->get_cache_detail($Url['id']);
		$params = empty($pid) ? '' : $pid;
		$json = json(['data' => [], 'info' => 'error', 'status' => 0]);
		$authcode = new \com\AuthCode();
		$auth = md5('plain-'.md5(date('Y-m-d-H').$Url['id'].'_'.$pid));  // 加密密钥
		if (!empty($array_detail)) {
			$data = $this->playlist_json(array($array_detail["read"]["vod_name"], $array_detail["show"]["list_name"], $array_detail["show"]["list_name_big"], $array_detail["show"]['list_id'], $array_detail["show"]['list_pid']), $array_detail["read"]["vod_playlist"], $params, 1);
			$playInfo = $this->Lable_Vod_Play($array_detail['read'], $array_detail['show'], array('id' => $Url['id'], 'sid' => 0, 'pid' => $pid));
			if (!empty($pid)) {
				$read = $array_detail["read"];
				$list['title'] = $data['Vod'][0];
				$list['listName'] = $data['Vod'][1];
				$list['listNameBig'] = $data['Vod'][2];
				$list['listId'] = $data['Vod'][3];
				$list['listIdBig'] = $data['Vod'][4];
				$list['subTitle'] = $data['Data'][0]['playurls'][0];
				$list['actor'] = $read['vod_actor'];
				$list['storyId'] = $read['story_id'];
				$list['actorId'] = $read['actor_id'];
				$list['pic'] = $read['vod_picurl'];
				$list['copyright'] = $read['vod_reurl'];
				$list['up'] = $read['vod_up'];
				$list['down'] = $read['vod_down'];
				$list['count'] = $playInfo['play_count'];
				$list['mcid'] = mcat_json($read['vod_mcid'], $read['vod_cid']);
				$list['pan'] = $read['vod_pan'];
				$list['key'] = $auth;
				if ($pid - 1 > 0) {
					$list['prev'] = $pid - 1;
				}
				if ($pid + 1 <= $playInfo['play_count']) {
					$list['next'] = $pid + 1;
				}
				$i = 0;
				foreach ($data['Data'] as $key => $val) {
					if ($val['playurls'][0] && $val['playurls'][1]) {
						$list['list'][$i]['playName'] = $val['playname'];
						$list['list'][$i]['playTitle'] = $val['playtitle'];
						$list['list'][$i]['title'] = $val['playurls'][0];
						$list['list'][$i]['vid'] = base64_encode($authcode->authcode($val['playurls'][1], "ENCODE", $auth, 0));
						if ($val['playurls'][3]) {
							$list['list'][$i]['pic'] = $val['playurls'][3];
						}
						if ($val['playurls'][4]) {
							$list['list'][$i]['isVip'] = $val['playurls'][4];
						}
						$i = $i + 1;
					}
				}
			} else {
				if ($data) {
					foreach ($data['Data']['playurls'] as $key => $val) {
						$list[$key]['title'] = $val[0];
						$list[$key]['episode'] = $val[1];
						if ($val[2]) {
							$list[$key]['isVip'] = $val[2];
						}
						if ($val[3]) {
							$list[$key]['pic'] = $val[3];
						}
					}
				} else {
					$list = [];
				}
			}
			$json = json(['data' => $list, 'info' => 'ok', 'status' => 1]);
		}
		return $json;
	}

	/**
	 * 列表，列表筛选，搜索页API接口
	 * 
	 * @param id 视频分类ID
	 * @param mcid 视频小分类ID
	 * @param area 地区
	 * @param year 年份
	 * @param letter 视频首字母
	 * @param order 排序 默认：addtime 按最新 2. hits 总排行 3.hits_week 周排行 4.hits_day 日排行 5.hits_month 月排行
	 * @param lz 1.连载 2 完结
	 * @param language 语言 日语 国语等
	 * @param day 按天获取数据
	 * @param wd 按关键词搜索，标题、别名、拼音、声优、tag
	 * @return json
	 * @param react 1 为追番周期表，空为正常列表
	 */
	public function list()
	{
		$Url = param_url();
		if ($Url['dir']) {
			$Url['id'] = getlist($Url['dir'], 'list_dir', 'list_id');
			$Url['dir'] = getlist($Url['id'], 'list_id', 'list_dir');
		} else {
			$Url['dir'] = getlist($Url['id'], 'list_id', 'list_dir');
		}
		$Url['limit'] = $Url['limit'] ? $Url['limit'] : 10;
		config('model', $this->request->module() . '/' . $this->request->controller() . '/' . $this->request->action());
		config('params', array('id' => $Url['id'], 'dir' => $Url['dir'], 'mcid' => $Url['mcid'], 'area' => $Url['area'], 'year' => $Url['year'], 'letter' => $Url['letter'], 'order' => $Url['order'], 'p' => "zanpianpage"));
		config('currentpage', $Url['page']);
		$List = list_search(F('_data/list'), 'list_id=' . $Url['id']);
		if ($List && $List[0]['list_sid'] == 1) {
			$ajax_list = zanpian_mysql_vod('mcid:' . $Url['mcid'] . ';cid:' . $Url['id'] . ';ids:' . $Url['ids'] . ';year:' . $Url['year'] . ';actor:' . $Url['actor'] . ';lz:' . $Url['lz'] . ';no:' . $Url['no'] . ';letter:' . $Url['letter'] . ';area:' . $Url['area'] . ';language:' . $Url['language'] . ';day:' . $Url['day'] . ';wd:' . $Url['wd'] . ';field:vod_id,vod_cid,vod_name,vod_area,vod_prty,vod_weekday,vod_director,vod_letters,vod_pic,vod_bigpic,vod_play,vod_url,vod_keywords,vod_gold,vod_continu,vod_title,vod_total,vod_actor,vod_mcid,vod_year,vod_addtime,vod_filmtime,vod_hits,vod_hits_day,vod_hits_week,vod_hits_month,vod_content;limit:' . $Url['limit'] . ';page:true;p:' . $Url['page'] . ';order:vod_' . $Url['order'] . ' desc;');
			if ($Url['react']) {
				return $this->format_data($ajax_list, 0, 1);
			} else {
				return $this->format_data($ajax_list, $Url['page'], 0, $Url['order']);
			}
		}
	}

	// 月份追番表
	public function month()
	{
		$Url = param_url();
		$month = input('month/d', date('Ym')) . '01';
		$lastday = date('Y-m-d', strtotime("$month +1 month -1 day"));
		$firstday = date("Ym01", strtotime($month));
		if ($Url['dir']) {
			$Url['id'] = getlist($Url['dir'], 'list_dir', 'list_id');
			$Url['dir'] = getlist($Url['id'], 'list_id', 'list_dir');
		} else {
			$Url['dir'] = getlist($Url['id'], 'list_id', 'list_dir');
		}
		$List = list_search(F('_data/list'), 'list_id=' . $Url['id']);
		if ($List && $List[0]['list_sid'] == 1) {
			$ajax_list = zanpian_mysql_vod('cid:' . $Url['id'] . ';limit:150;field:vod_id,vod_cid,vod_name,vod_play,vod_letters,vod_pic,vod_director,vod_gold,vod_title,vod_total,vod_actor,vod_mcid,vod_year,vod_addtime,vod_content,vod_continu,vod_area,vod_filmtime;order:vod_addtime desc;tj:vod_filmtime|between|' . strtotime($firstday) . ',' . strtotime($lastday) . '');
			return $this->format_data($ajax_list);
		}
	}

	public function storylist()
	{
		$Url = param_url();
		if ($Url['dir']) {
			$Url['id'] = getlist($Url['dir'], 'list_dir', 'list_id');
			$Url['dir'] = getlist($Url['id'], 'list_id', 'list_dir');
		} else {
			$Url['dir'] = getlist($Url['id'], 'list_id', 'list_dir');
		}
		$Url['limit'] = $Url['limit'] ? $Url['limit'] : 10;
		config('model', $this->request->module() . '/' . $this->request->controller() . '/' . $this->request->action());
		config('params', array('id' => $Url['id'], 'dir' => $Url['dir'], 'p' => "zanpianpage"));
		config('currentpage', $Url['page']);
		$List = list_search(F('_data/list'), 'list_id=' . $Url['id']);
		if ($List && $List[0]['list_sid'] == 4) {
			$channel = $this->Lable_List($Url, $List[0]);
			$this->assign('param', $Url);
			$this->assign($channel);
			$ajax_list = zanpian_mysql_story('ids:' . $Url['ids'] . ';cid:' . $Url['id'] . ';vcid:' . $Url['vcid'] . ';vid:' . $Url['vid'] . ';name:' . $Url['name'] . ';tj:' . $Url['tj'] . ';stars:' . $Url['stars'] . ';day:' . $Url['day'] . ';lz:' . $Url['lz'] . ';hits:' . $Url['hits'] . ';prty:' . $Url['prty'] . ';field:story_id,story_cid,story_vid,story_addtime,story_hits,story_hits_day,story_hits_week,story_hits_month,story_continu,story_cont,story_title,story_page,story_stars,vod_id,vod_cid,vod_letters,vod_name,vod_mcid,vod_content,vod_pic,vod_director,vod_actor,vod_year,vod_gold,vod_hits,vod_addtime,vod_jumpurl;limit:' . $Url['limit'] . ';page:true;p:' . $Url['page'] . ';order:story_' . $Url['order'] . ' desc;');
			if ($ajax_list) {
				foreach ($ajax_list as $i => $val) {
					$list['data'][$i]['storyId'] = $val['story_id'];
					$list['data'][$i]['storyCid'] = $val['story_cid'];
					$list['data'][$i]['storyTitle'] = $val['story_title'];
					$list['data'][$i]['id'] = $val['vod_id'];
					$list['data'][$i]['title'] = $val['vod_name'];
					$list['data'][$i]['name'] = $val['list_name'];
					$list['data'][$i]['cid'] = $val['vod_cid'];
					$list['data'][$i]['addtime'] = date('Y-m-d H:i:s', $val['story_addtime']);
					if ($val['story_continu'] > 0) {
						$val['story_continu'] = (int)$val['story_continu'];
					} else {
						$val['story_continu'] = '完结';
					}
					$list['data'][$i]['continu'] = $val['story_continu'];
					$list['data'][$i]['pic'] = $val['vod_picurl'];
				}
				$list['limit'] = $Url['limit'];
				$list['current'] = $ajax_list[0]['page']['currentpage'];
				$list['count'] = $ajax_list[0]['page']['pagecount'];
				$list['total'] = $ajax_list[0]['page']['totalpage'];
				$list['info'] = 'ok';
				$list['status'] = 1;
			} else {
				$list['limit'] = $Url['limit'];
				$list['current'] = 1;
				$list['total'] = 0;
				$list['count'] = 0;
				$list['data'] = [];
				$list['info'] = 'error';
				$list['status'] = 0;
			}
			return json($list);
		}
	}

	/**
	 * 登录接口
	 */
	public function login()
	{
		$data = input('post.');
		$user = model('User');
		$info = $user->login($data['username'], $data['password']);
		if ($info['userid'] > 0) {
			return json(['data' => $info['userid'], 'msg' => "登录成功", 'rcode' => 1]);
		} else {
			switch ($info) {
				case -1:
					$error = '用户不存在！';
					break; //系统级别禁用
				case -2:
					$error = '密码错误！';
					break;
				case -3:
					$error = '用户被锁定！';
					break;
				default:
					$error = '未知错误！';
					break; // 0-接口参数错误（调试阶段使用）
			}
			return json(["msg" => $error, "rcode" => -1]);
		}
	}

	//随机数
	function randstr($length = 32, $type = 'numandalpha')
	{
		$role = array(
			'numandalpha' => '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',
			'num' => '0123456789',
			'captcha' => '2345678abcdefhijkmnpqrstuvwxyzABCDEFGHJKLMNPQRTUVWXY',
		);
		$characters = array_key_exists($type, $role) ? $role[$type] : $role['numandalpha'];
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, strlen($characters) - 1)];
		}
		return $randomString;
	}

	//生成图形验证码
	public function verify()
	{
		//随机码
		$key = $this->randstr(32, 'captcha');

		$config = [
			'length' => 4,
			'imageH' => 35,
			'imageW' => 120,
			'fontSize' => 16,
			'reset' => false,
			'expire' => 60,
			'useCurve' => false,
		];
		$captcha = new \captcha\Captcha($config);

		//构造返回参数
		$data['imgkey'] = $key;
		$data['base64img'] = $captcha->entry($key);
		return json(array('code' => 0, 'msg' => '获取成功', 'data' => $data));
	}

	/**
	 * 注册
	 */
	public function register()
	{
		$data = input('post.');
		//验证码验证
		$result = $this->checkcode($data['validate'], $data['key']);
		if (!$result) {
			return json(['msg' => "验证码错误", 'rcode' => -2]);
		}
		$result = $this->validate($data, 'User.reg');
		if ($result != 1) {
			return json(['msg' => $result, 'rcode' => -1]);
		}
		$user = model('User');
		$info = $user->reg($data['username'], $data['email'], $data['password']);
		if ($info['userid']) {
			return json(['data' => $info['userid'], 'msg' => "注册成功", 'rcode' => 1]);
		}
	}

	/**
	 * 验证图形验证码
	 */
	protected function checkcode($code, $id = '')
	{
		$captcha = new \captcha\Captcha();
		return $captcha->check($code, $id);
	}

	// 获取用户资料
	public function getuserinfo()
	{
		$Url = param_url();
		$rs = model("User");
		$user = $rs->getuserinfo($Url['uid']);
		if ($user) {
			return json($user);
		} else {
			return json((object)array());
		}
	}

	/**
	 * 获取评分和评论
	 */
	public function getCmScore()
	{
		$Url = param_url();
		$p = input('p/d', 1);
		$uid = input('uid/d', 0);
		$result = array();
		$total = 0;
		if (!empty($Url['id']) && !empty($Url['sid'])) {
			$rs = model("Cm");
			$where = array('cm_vid' => $Url['id'], 'cm_sid' => $Url['sid'], 'cm_pid' => 0, 'cm_status' => 1);
			$total = $rs->getcmcount($where);
			if ($total > 0) {
				$limit = $this->userconfig['user_cmnum'];
				$page = array('limit' => $limit, 'currentpage' => $p);
				$list = $rs->getcomments($where, $page);  // 查询所有结果集
				$data = $list->all();
				foreach ($list as $k => $v) {
					$data[$k]['cm_sub'] = $rs->getpidcomments(array('cm_pid' => $v['cm_id'], 'cm_sid' => $v['cm_sid'], 'cm_status' => 1));
					if ($data[$k]['cm_sub']) {
						foreach ($data[$k]['cm_sub'] as $kk => $var) {
							if ($var['cm_tid']) {
								$data[$k]['cm_sub'][$kk]['cm_tub'] = db('Cm')->alias('c')->join('user u', 'u.userid=c.cm_uid', 'LEFT')->where("cm_id = " . $var['cm_tid'] . "")->find();
							}
						}
					}
				}
				if ($data) {
					foreach ($data as $i => $val) {
						$lou = $total - (($p - 1) * $limit) - $i;
						$result['data']['comment'][$i]['floor'] = $lou;
						$result['data']['comment'][$i]['cm_id'] = $val['cm_id'];
						$result['data']['comment'][$i]['cm_content'] = remove_xss(strip_tags($val['cm_content']));
						$result['data']['comment'][$i]['nickname'] = $val['nickname'];
						$result['data']['comment'][$i]['cm_support'] = $val['cm_support'];
						$result['data']['comment'][$i]['cm_oppose'] = $val['cm_oppose'];
						$result['data']['comment'][$i]['cm_addtime'] = zanpian_from_time($val['cm_addtime']);
						$result['data']['comment'][$i]['cm_uid'] = $val['cm_uid'];
						$result['data']['comment'][$i]['cm_sid'] = $val['cm_sid'];
						// $result['data'][$i]['cm_subs'] = $val['cm_sub'];
						if ($val['api']) {
							$result['data']['comment'][$i]['avatar'] = $val['api'][0]['avatar'];
						}
						if (count($val['cm_sub']) > 0) {
							foreach ($val['cm_sub'] as $ii => $value) {
								$sublou = count($val['cm_sub']) - $ii;
								$result['data']['comment'][$i]['cm_sub'][$ii]['floor'] = $sublou;
								$result['data']['comment'][$i]['cm_sub'][$ii]['cm_id'] = $value['cm_id'];
								$result['data']['comment'][$i]['cm_sub'][$ii]['cm_content'] = remove_xss(strip_tags($value['cm_content']));
								$result['data']['comment'][$i]['cm_sub'][$ii]['nickname'] = $value['nickname'];
								$result['data']['comment'][$i]['cm_sub'][$ii]['cm_support'] = $value['cm_support'];
								$result['data']['comment'][$i]['cm_sub'][$ii]['cm_oppose'] = $value['cm_oppose'];
								$result['data']['comment'][$i]['cm_sub'][$ii]['cm_addtime'] = zanpian_from_time($value['cm_addtime']);
								$result['data']['comment'][$i]['cm_sub'][$ii]['cm_uid'] = $value['cm_uid'];
								$result['data']['comment'][$i]['cm_sub'][$ii]['cm_sid'] = $value['cm_sid'];
								if ($value['cm_tid']) {
									$result['data']['comment'][$i]['cm_sub'][$ii]['at']['nickname'] = $value['cm_tub']['nickname'];
									$result['data']['comment'][$i]['cm_sub'][$ii]['at']['cm_uid'] = $value['cm_tub']['cm_uid'];
								}
							}
						} else {
							$result['data']['comment'][$i]['cm_sub'] = [];
						}
					}
				}
				$result['data']['limit'] = $limit ? $limit : 15;
				$result['data']['total'] = $list->total();
				$result['data']['current'] = $list->currentPage();
				$result['data']['lastPage'] = $list->lastPage();
			}
			$result['data']['star'] = $this->getScore($Url['id']);
			$isFavoriteRemind = $this->getFavoriteRemind($Url['id'], $uid);
			$result['data']['loveid'] = $isFavoriteRemind['loveid'];
			$result['data']['remindid'] = $isFavoriteRemind['remindid'];
			$result['info'] = 'ok';
			$result['status'] = 1;
			return json($result);
		}
		return json($result);
	}
	/**
	 * 是否收藏订阅
	 */
	private function getFavoriteRemind($id, $uid)
	{
		$data = array();
		if ($uid) {
			$favorite = db('favorite')->where(array('favorite_vid' => $id, 'favorite_uid' => $uid))->value('favorite_id');
			if ($favorite) {
				$data['loveid'] = $id;
			}
			$remin = db('remind')->where(array('remind_vid' => $id, 'remind_uid' => $uid))->value('remind_id');
			if ($remin) {
				$data['remindid'] = $id;
			}
			return $data;
		}
	}

	/**
	 * 获取评分
	 */
	private function getScore($id)
	{
		$rs = model('VodMark'); // 获取其它数据信息
		$data = array();
		$markdata = $rs->getMark($id);
		$data = array();
		$vod_gold = db('vod')->where(array('vod_id' => $id, 'vod_status' => 1))->value('vod_gold');
		if ($markdata != null) {
			$data['a'] = (int)$markdata['F5'];
			$data['b'] = (int)$markdata['F4'];
			$data['c'] = (int)$markdata['F3'];
			$data['d'] = (int)$markdata['F2'];
			$data['e'] = (int)$markdata['F1'];
			$rate = $rs->getPingfen($markdata);
			if (count($rate) > 0) {
				$data['pinfen'] = $rate['R1'];
			} else {
				$data['a'] = 0;
				$data['b'] = 0;
				$data['c'] = 0;
				$data['d'] = 0;
				$data['e'] = 0;
				$data['pinfen'] = $vod_gold;
			}
		}
		return $data;
	}
	/**
	 * 获取评分
	 */
	private function getPingfen($data)
	{
		$f1 = $data['F1'];
		$f2 = $data['F2'];
		$f3 = $data['F3'];
		$f4 = $data['F4'];
		$f5 = $data['F5'];
		$pftotal = $f1 + $f2 + $f3 + $f4 + $f5;
		$array = array();
		if ($pftotal > 0) {
			$rating = ($f1 / $pftotal) * 1 + ($f2 / $pftotal) * 2 + ($f3 / $pftotal) * 3 + ($f4 / $pftotal) * 4 + ($f5 / $pftotal) * 5;
			$r1 = round($rating * 2, 1);
			$array["R1"] = number_format($r1, 1);
		}
		return $array;
	}

	/**
	 * 增加评分
	 */
	public function mark()
	{
		$vod_id = input('id/d', 0);
		$value = input('val/d', 0);
		$result = array("msg" => "提交评分失败", "rcode" => "-1");
		if (isset($vod_id) && isset($value)) {
			if ($value == "1" || $value == "2" || $value == "3" || $value == "4" || $value == "5") {
				//查询一下
				$rs = model("VodMark");
				$ip = ip2long(get_client_ip());
				$count = $rs->where(array('mark_vid' => $vod_id, 'mark_ip' => $ip))->count();
				if ($count > 0) { //存在
					$result['msg'] = "已经评分,请务重复评分";
					$result['rcode'] = -2;
				} else { //增加评分
					$mark = array();
					$mark['mark_vid'] = $vod_id;
					$mark['mark_ip'] = $ip;
					$mark['mark_addtime'] = time();
					$mark['F' . $value] = 1;
					$id = $rs->insertGetId($mark);
					if ($id > 0) {
						$result['msg'] = "提交评分成功";
						$result['rcode'] = 1;
						$data = $rs->getMark($vod_id);
						$rate = $this->getPingfen($data);
						if (count($rate) > 0) {
							Db::name('vod')->where('vod_id', $vod_id)->setField('vod_gold', $rate['R1']);
						}
					}
					//重新计算一下
				}
			}
		}
		return json($result);
	}

	public function love()
	{
		$id = input('id/d', 0); //影片ID
		$cid = input('cid/d', 0); //影片CID
		$uid = input('uid/d', 0);
		if ($uid && $id && $cid) {
			$favorite = db('favorite')->where(array('favorite_vid' => $id, 'favorite_uid' => $uid))->value('favorite_id');
			if ($favorite) {
				$result = db('favorite')->where(array('favorite_vid' => $id, 'favorite_uid' => $uid))->delete();
				if ($result) {
					return json(array("msg" => "取消收藏成功", "rcode" => 1));
				}
			} else {
				$data['favorite_vid'] = $id;
				$data['favorite_uid'] = $uid;
				$data['favorite_cid'] = getlistpid($cid);
				$data['favorite_addtime'] = time();
				$id = db('favorite')->insertGetId($data);
				if ($id > 0) {
					return json(array("msg" => "收藏成功", "rcode" => 1));
				}
			}
		}
		return json(array("msg" => "收藏失败", "rcode" => -1));
	}

	public function remind()
	{
		$id = input('id/d', 0); //影片ID
		$cid = input('cid/d', 0); //影片CID
		$uid = input('uid/d', 0);
		if ($uid && $id && $cid) {
			$favorite = db('remind')->where(array('remind_vid' => $id, 'remind_uid' => $uid))->value('remind_id');
			if ($favorite) {
				$result = db('remind')->where(array('remind_vid' => $id, 'remind_uid' => $uid))->delete();
				if ($result) {
					return json(array("msg" => "取消订阅成功", "rcode" => 1));
				}
			} else {
				$vod_addtime = get_vod_info($id, 'vod_id', 'vod_addtime');
				$data['remind_vid'] = $id;
				$data['remind_uid'] = $uid;
				$data['remind_cid'] = getlistpid($cid);
				$data['remind_addtime'] = time();
				$data['remind_uptime'] = $vod_addtime;
				$id = db('remind')->insertGetId($data);
				if ($id > 0) {
					return json(array("msg" => "订阅成功", "rcode" => 1));
				}
			}
		}
		return json(array("msg" => "订阅失败", "rcode" => -1));
	}

	// 通过视频拼音获取视频ID
	public function getVodId()
	{
		$Url = param_url();
		if (!empty($Url['pinyin'])) {
			$id = get_vod_info($Url['pinyin'], 'vod_letters', 'vod_id');
			return json(['data' => $id, 'info' => 'ok', 'status' => 1]);
		} else {
			return json(['data' => 0, 'info' => '拼音不正确', 'status' => 0]);
		}
	}

	public function newsList()
	{
		$Url = param_url();
		if ($Url['dir']) {
			$Url['id'] = getlist($Url['dir'], 'list_dir', 'list_id');
			$Url['dir'] = getlist($Url['id'], 'list_id', 'list_dir');
		} else {
			$Url['dir'] = getlist($Url['id'], 'list_id', 'list_dir');
		}
		config('model', $this->request->module() . '/' . $this->request->controller() . '/' . $this->request->action());
		config('params', array('id' => $Url['id'], 'dir' => $Url['dir'], 'p' => "zanpianpage"));
		config('currentpage', $Url['page']);
		$List = list_search(F('_data/list'), 'list_id=' . $Url['id']);
		if ($List && $List[0]['list_sid'] == 2) {
			$channel = $this->Lable_List($Url, $List[0]);
			$this->assign('param', $Url);
			$this->assign($channel);
			$ajax_list = zanpian_mysql_news('cid:' . $Url['id'] . ';letter:' . $Url['letter'] . ';news:' . $Url['news'] . ';did:' . $Url['did'] . ';day:' . $Url['day'] . ';wd:' . $Url['wd'] . ';name:' . $Url['name'] . ';field:news_id,news_cid,news_name,news_mid,news_color,news_pic,news_remark,news_hits,news_hits_day,news_hits_week,news_hits_month,news_stars,news_up,news_down,news_gold,news_golder,news_addtime,news_jumpurl,news_playname,news_clarity,news_playtime,news_playurl;limit:' . $Url['limit'] . ';page:true;p:' . $Url['page'] . ';order:news_' . $Url['order'] . ' desc;');
			if ($ajax_list) {
				foreach ($ajax_list as $key => $val) {
					$list['data'][$key]['id'] = $val['news_id'];
					$list['data'][$key]['cid'] = $val['news_cid'];
					$list['data'][$key]['name'] = $val['list_name'];
					$list['data'][$key]['title'] = $val['news_name'];
					$list['data'][$key]['remark'] = $val['news_remark'];
					$list['data'][$key]['addtime'] = date('Y-m-d H:i:s', $val['news_addtime']);
					if (!empty($val['news_pic'])) {
						$list['data'][$key]['pic'] = $val['news_picurl'];
					}
					if ($val['news_jumpurl']) {
						$list['data'][$key]['jump'] = $val['news_jumpurl'];
					}
				}
				if ($Url['page'] != '0') {
					$list['limit'] = input('limit/d', 0);
					$list['current'] = $ajax_list[0]['page']['currentpage'];
					$list['count'] = $ajax_list[0]['page']['pagecount'];
					$list['total'] = $ajax_list[0]['page']['totalpage'];
				}
				$list['info'] = 'ok';
				$list['status'] = 1;
			} else {
				if ($Url['page'] != '0') {
					$list['limit'] = 10;
					$list['current'] = 1;
					$list['total'] = 0;
					$list['count'] = 0;
				}
				$list['data'] = [];
				$list['info'] = 'error';
				$list['status'] = 0;
			}
			return json($list);
		}
	}
	public function newsDetail()
	{
		$Url = param_url();
		$array_detail = $this->get_cache_detail_news($Url['id'], $Url);
		$Url['page'] = !empty($Url['page']) ? $Url['page'] : 1;
		$detail = $array_detail;
		if ($detail) {
			$show = $detail['show'];
			$read = $detail['read'];
			$list['id'] = $read['news_id'];
			$list['cid'] = $show['list_id'];
			$list['name'] = $show['list_name'];
			$list['title'] = $read['news_name'];
			$list['keywords'] = $read['news_keywords'];
			$list['remark'] = $read['news_remark'];
			if (!empty($read['news_pic'])) {
				$list['pic'] = $read['news_picurl'];
			}
			$list['time'] = zanpian_from_time($read['news_addtime']);
			$list['addtime'] = date('Y-m-d H:i:s', $read['news_addtime']);
			$list['hits'] = $read['news_hits'];
			$list['inputer'] = $read['news_inputer'];
			if (!empty($read['tag'])) {
				foreach ($read['tag'] as $key => $val) {
					$list['tag'][$key] = $val['tag_name'];
				}
			}
			if (!empty($read['news_next'])) {
				$list['next']['id'] = $read['news_next']['news_id'];
				$list['next']['title'] = $read['news_next']['news_name'];
			}
			if (!empty($read['news_prev'])) {
				$list['prev']['id'] = $read['news_prev']['news_id'];
				$list['prev']['title'] = $read['news_prev']['news_name'];
			}
			if (!empty($read['news_playname'])) {
				$list['playname'] = $read['news_playname'];
			}
			if (!empty($read['news_clarity'])) {
				$list['clarity'] = $read['news_clarity'];
			}
			if (!empty($read['news_playname'])) {
				$list['playtime'] = $read['news_playtime'];
			}
			if (!empty($read['news_playname'])) {
				$list['playurl'] = $read['news_playurl'];
			}
			if (!empty($read['news_vodid'])) {
				$list['vodid'] = $read['news_vodid'];
			}
			if (!empty($read['news_newsid'])) {
				$list['newsid'] = $read['news_newsid'];
			}
			$list['content'] = $read['news_content'];
			return json(['data' => $list, 'info' => 'ok', 'status' => 1]);
		} else {
			return json(['data' => [], 'info' => 'error', 'status' => 0]);
		}
	}
	/**
	 * 单集剧情详情和详情列表
	 */
	public function storyread()
	{
		$Url = param_url();
		$vid = $Url['vid'];
		if (!empty($Url['pinyin']) && empty($vid)) {
			$vid = get_vod_info($Url['pinyin'], 'vod_letters', 'vod_id');
			$Url['id'] = get_story_info($vid, 'story_vid', 'story_id');
		} elseif (!empty($vid)) {
			$Url['id'] = get_story_info($vid, 'story_vid', 'story_id');
		}
		$array_detail = $this->get_cache_detail_story($Url['id']);
		$read = $array_detail['read'];
		if (!$Url['p']) {
			$list['vContent'] = strip_tags($read['vod_content']);
		}
		$Url['p'] = input('p/d', 1);
		if ($array_detail && $Url['p'] <= count($read['story_list'])) {
			$data = $read['story_list'][$Url['p']]; // 当前集
			$list['id'] = $read['story_id'];
			$list['vid'] = $read['vod_id'];
			$list['vTitle'] = $read['vod_name'];
			$list['pic'] = $read['vod_pic'];
			if (is_numeric($read['vod_continu'])) {
				if ($read['vod_continu'] > 0) {
					$read['vod_continu'] = (int)$read['vod_continu'];
				} else {
					$read['vod_continu'] = '完结';
				}
			}
			$list['year'] = $read['vod_year'];
			$list['mcid'] = mcat_json($read['vod_mcid'], $read['vod_cid']);
			$list['status'] = $read['vod_continu'];
			$lastplayurl = play_url_end($read['vod_url'], $read['vod_play']);
			$list['sid'] = $lastplayurl[0];
			$list['pid'] = $lastplayurl[1];
			$list['lastname'] = $lastplayurl[2];
			$list['gold'] = $read['vod_gold'];
			$list['actor'] = $read['vod_actor'];
			$list['name'] = str_replace(array("\r\n", "\n", "\r"), '', $data['story_name']);
			$list['title'] = $data['story_title'];
			$list['content'] = $data['story_info'];
			if ($Url['p'] == 1 && $data['story_page'] == 1) {
				$list['next'] = $data['story_page'] + 1;
			} elseif (count($read['story_list']) == $data['story_page']) {
				$list['prev'] = $data['story_page'] - 1;
			} else {
				$list['prev'] = $data['story_page'] - 1;
				$list['next'] = $data['story_page'] + 1;
			}
			$story = $this->storydetaillist($read['vod_id']);
			if ($story) {
				$list['storyNum'] = count($story);
			}
			return json(['data' => $list, 'info' => 'ok', 'status' => 1]);
		} else {
			return json(['data' => [], 'info' => 'error', 'status' => 0]);
		}
	}

	// 角色详情
	public function roleread()
	{
		$Url = param_url();
		$array_detail = $this->get_cache_detail_role($Url['id']);
		if ($array_detail) {
			$show = $array_detail['show'];
			$read = $array_detail['read'];
			$list['id'] = $read['role_id'];
			$list['cid'] = $read['role_cid'];
			$list['name'] = $show['list_name'];
			$list['title'] = $read['role_name'];
			$list['pic'] = $read['role_picurl'];
			$list['content'] = $read['role_content'];
			$list['starId'] = $read['star_id'];
			$list['starCid'] = $read['star_cid'];
			$list['starName'] = $read['star_name'];
			$list['starPic'] = $read['star_picurl'];
			$list['vid'] = $read['vod_id'];
			$list['vTitle'] = $read['vod_name'];
			return json(['data' => $list, 'info' => 'ok', 'status' => 1]);
		} else {
			return json(['data' => [], 'info' => 'error', 'status' => 0]);
		}
	}
	/**
	 * 获取观看记录 需要登录
	 */
	public function getplaylog()
	{
		$Url = param_url();
		$uid = $Url['uid'];
		if ($uid) {
			$where = array('log_uid' => $uid);
			$page = array('limit' => 10, 'currentpage' => $Url['p']);
			$rs = model("Playlog");
			$list = $rs->getplaylog($where, $page); //查询所有结果集
			if ($list->total() > 0) {
				foreach ($list as $key => $value) {
					$data[$key]['id'] = $value['log_id'];
					$data[$key]['title'] = $value['vod_name'];
					$data[$key]['name'] = $value['log_urlname'];
					$data[$key]['vid'] = $value['log_vid'];
					$data[$key]['pid'] = $value['log_pid'];
					if ($value['log_pid'] < $value['log_maxnum']) {
						$data[$key]['next'] = $value['log_pid'] + 1;
					}
				}
				$result = ['data' => $data, 'info' => 'ok', 'status' => 1];
				$result['rcode'] = $list->total();
			}
		} else {
			$result = ['data' => [], 'info' => 'ok', 'status' => 0];
		}
		return json($result);
	}
	/**
	 * 添加观看记录 需要登录
	 */
	public function addplaylog()
	{
		$uid = input('uid/d');
		if ($uid) {
			$data = input('post.vod_id/d');
			$vod_id = input('post.vod_id/d');
			Db::name('playlog')->where(array("log_vid" => $vod_id, "log_uid" => $uid))->delete(); //先删除
			$data = array();
			$data["log_vid"] = $vod_id;
			$data["log_sid"] = input('vod_sid/d');
			$data["log_pid"] = input('vod_pid/d');
			$data["log_urlname"] = input('url_name/s');
			$data["log_maxnum"] = input('vod_maxnum/d');
			$data["log_uid"] = input('uid/d');
			$data["log_addtime"] = time();
			Db::name('playlog')->insert($data);
			return json(array("rcode" => "1"));
		} else {
			return json(array("rcode" => "-1"));
		}
	}
	/**
	 * 清空观看记录 需登录
	 */
	public function emptyhistory()
	{
		$uid = input('uid/d');
		$result = array("rcode" => "-1");
		if ($uid) {
			Db::name('playlog')->where('log_uid', $uid)->delete();
			$result = array("rcode" => "1");
		}
		return json($result);
	}
	/**
	 * 删除观看记录 需登录
	 */
	public function delplaylog()
	{
		$uid = input('uid/d');
		$id = input('id/d');
		$result = array("rcode" => "-1");
		if ($uid && !empty($id)) {
			Db::name('playlog')->where(array('log_id' => $id, 'log_uid' => $uid))->delete();
			$result = array("rcode" => "1");
		}
		return json($result);
	}
	//评论顶踩	
	public function digg()
	{
		$Url = param_url();
		$comment_id = $Url['id'];
		$type = $Url['type'];
		$success = false;
		//不需要登录
		if (!empty($comment_id) && !empty($type)) {
			$ip = ip2long(get_client_ip());
			$count = Db::name('cm_opinion')->where(array('opinion_cmid' => $comment_id, 'opinion_ip' => $ip))->count();
			if ($count > 0) { //已经
				$result = array("msg" => "已经点评", "rcode" => "-1");
				$success = true;
			} else {
				$opinion = -1;
				$key = '';
				if ($type == 'sup') {
					$opinion = 1;
					$key = 'cm_support';
				} else if ($type == 'opp') {
					$opinion = 0;
					$key = 'cm_oppose';
				}
				if ($opinion >= 0) {
					$data['opinion_cmid'] = $comment_id;
					$data['opinion_opinion'] = $opinion;
					$data['opinion_addtime'] = time();
					$data['opinion_ip'] = $ip;
					$id = Db::name('cm_opinion')->insertGetId($data);
					if ($id > 0) {
						//修改值
						$addValue = Db::name('Cm')->where('cm_id', $comment_id)->setInc($key);
						$result = array("msg" => "成功点评", "rcode" => "1");
						$success = true;
					}
				}
			}
		}
		if (!$success) {
			$result = array("msg" => "点评失败", "rcode" => "-1");
		}
		return json($result);
	}

	// public function getAds()
	// {
	// 	$Url = param_url();
	// 	$where = array();
	// 	$where['ads_status'] = 1;
	// 	$where['ads_id'] = $Url['id'];
	// 	$array = Db::name('ads')->where($where)->select();
	// 	if ($array) {
	// 		$list['id'] = $array[0]['ads_id'];
	// 		$list['content'] = $array[0]['ads_content'];
	// 	}
	// 	return json(['data' => $list, 'info' => 'ok', 'status' => 1]);
	// }

	public function game()
	{
		/**
		 * @date 2016-06-14
		 * @description 游戏数据拉取接口Demo
		 */
		$Url = param_url();
		// $url                = ; //接口地址
		// $keyword            = urldecode();
		// $keyword            = urlencode($keyword); //查询关键字，需以URLEncode方式进行编码，totalList为渠道下所有游戏数据
		$data['channelId']  = 12633; //渠道号
		$data['query']      = $Url['wd'];
		$data['orderby']    = $Url['order']; //排序，update--按更新日期排序，hot--按热门排序
		$data['page']       = $Url['p']; //页号，默认为1
		$data['pageNum']    = $Url['limit']; //每页返回数据条数，默认为50，最多返回500条

		$post_data = json_encode($data);

		// echo $post_data;

		// 使用CURL拉取数据
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, 'https://tui.guopan.cn/api/queryGames.php');
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
		curl_setopt($ch, CURLOPT_TIMEOUT, 10);
		curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (compatible; MSIE 5.01; Windows NT 5.0)');
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
		$response = curl_exec($ch); //返回结果，数据为JSON格式
		curl_close($ch);
		$list = json_decode($response);
		// echo $response; //测试，打印返回数据
		return json($list);
	}

	// 从角色数据库获取数据
	private function get_cache_detail_role($id)
	{
		if (!$id) {
			return false;
		}
		//优先读取缓存数据
		if (config('data_cache_role')) {
			$array_detail = Cache::get('data_cache_role_' . $id);
			if ($array_detail) {
				return $array_detail;
			}
		}
		//未中缓存则从数据库读取
		$where = array();
		$where['role_id'] = $id;
		$where['role_status'] = array('eq', 1);
		$array = db('role')->field('r.*,st.*,story_id,story_cid,story_status,actor_id,actor_cid,actor_status,vod_id,vod_cid,vod_mcid,vod_name,vod_aliases,vod_title,vod_keywords,vod_actor,vod_director,vod_content,vod_pic,vod_play,vod_bigpic,vod_diantai,vod_tvcont,vod_tvexp,vod_area,vod_language,vod_year,vod_continu,vod_total,vod_isend,vod_addtime,vod_hits,vod_hits_day,vod_hits_week,vod_hits_month,vod_stars,vod_jumpurl,vod_letter,vod_gold,vod_golder,vod_isfilm,vod_filmtime,vod_length,vod_letters')->alias('r')->join('actor a', 'a.actor_vid = r.role_vid', 'LEFT')->join('vod v', 'v.vod_id = r.role_vid  and v.vod_status=1', 'LEFT')->join('story s', 's.story_vid = r.role_vid  and s.story_status=1', 'LEFT')->join('star st', 'st.star_name = r.role_star and st.star_status=1', 'LEFT')->where($where)->find();
		if ($array) {
			//解析标签
			$array_detail = $this->Lable_Role_Read($array);
			//print_r($array_detail) ;
			if (config('data_cache_actor')) {
				Cache::tag('model_role')->set('data_cache_role_' . $id, $array_detail, intval(config('data_cache_role')));
			}
			return $array_detail;
		}
		return false;
	}

	// 新闻从数据库获取数据
	private function get_cache_detail_news($id, $Url)
	{
		if (!$id) {
			return false;
		}
		//优先读取缓存数据
		if (config('data_cache_news')) {
			$array_detail = Cache::get('data_cache_news_' . $id);
			if ($array_detail) {
				return $array_detail;
			}
		}
		//未中缓存则从数据库读取
		$where = array();
		$where['news_id'] = $id;
		$where['news_status'] = array('eq', 1);
		$data = model('news')->where($where)->relation('newsrel,tag')->find();
		if (!empty($data)) {
			$array = $data->toArray();
			//解析标签
			$array_detail = $this->Lable_News_Read($array);
			if (config('data_cache_news')) {
				Cache::tag('model_news')->set('data_cache_news_' . $id, $array_detail, intval(config('data_cache_news')));
			}
			return $array_detail;
		}
		return false;
	}

	// 视频从数据库获取数据
	private function get_cache_detail($vod_id)
	{
		if (!$vod_id) {
			return false;
		}
		//优先读取缓存数据
		if (config('data_cache_vod')) {
			$array_detail = Cache::get('data_cache_vod_' . $vod_id);
			if ($array_detail) {
				return $array_detail;
			}
		}
		//未中缓存则从数据库读取
		$where = array();
		$where['vod_id'] = $vod_id;
		$where['vod_status'] = array('eq', 1);
		$array = db('vod')->field('v.*,story_id,story_cid,story_status,actor_id,actor_cid,actor_status')->alias('v')->join('story s', 's.story_vid = v.vod_id', 'LEFT')->join('actor a', 'a.actor_vid = v.vod_id', 'LEFT')->where($where)->find();
		if ($array) {
			//解析标签
			$array_detail = $this->Lable_Vod_Read($array);
			if (config('data_cache_vod')) {
				Cache::tag('model_vod')->set('data_cache_vod_' . $vod_id, $array_detail, intval(config('data_cache_vod')));
			}
			return $array_detail;
		}
		return false;
	}

	private function fd($val, $order = '', $len = 0)
	{
		$list['id'] = $val['vod_id'];
		$list['title'] = $val['vod_name'];
		if ($len == 100) {
			if ($order == 'hits') {
				$list['glod'] = $val['vod_gold'];
			} elseif ($order == 'hits_day') {
				$list['hits'] = $val['vod_hits_day'];
			} elseif ($order == 'hits_week') {
				$list['hits'] = $val['vod_hits_week'];
			} elseif ($order == 'hits_month') {
				$list['hits'] = $val['vod_hits_month'];
			} elseif ($order == 'addtime') {
				$list['listName'] = $val['list_name'];
				$list['listId'] = $val['list_id'];
				$list['mcid'] = mcat_json($val['vod_mcid'], $val['vod_cid']);
				$list['time'] = date('Y-m-d H:i:s', $val['vod_addtime']);
				$list['isDate'] = date('Y-m-d') == date('Y-m-d', $val['vod_addtime']) ? 1 : 0;
				$lastplayurl = play_url_end($val['vod_url'], $val['vod_play']);
				$list['sid'] = $lastplayurl[0];
				$list['pid'] = $lastplayurl[1];
				$list['lastname'] = $lastplayurl[2];
			}
		} else {
			$list['glod'] = $val['vod_gold'];
			$list['weekday'] = (int)$val['vod_weekday'];
			$list['year'] = $val['vod_year'];
			$list['area'] = $val['vod_area'];
			$list['pic'] = $val['vod_picurl'];
			$list['isDate'] = date('Y-m-d') == date('Y-m-d', $val['vod_addtime']) ? 1 : 0;
			if ($val['vod_bigpic']) {
				$list['smallPic'] = $val['vod_bigpicurl'];
			}
			if (is_numeric($val['vod_continu'])) {
				if ($val['vod_continu'] > 0) {
					$val['vod_continu'] = (int)$val['vod_continu'];
				} else {
					$val['vod_continu'] = '完结';
				}
			}
			$list['year'] = $val['vod_year'];
			$list['mcid'] = mcat_json($val['vod_mcid'], $val['vod_cid']);
			$list['status'] = $val['vod_continu'];
			$lastplayurl = play_url_end($val['vod_url'], $val['vod_play']);
			$list['sid'] = $lastplayurl[0];
			$list['pid'] = $lastplayurl[1];
			$list['lastname'] = $lastplayurl[2];
		}
		return $list;
	}

	/**
	 * 格式化输出数据
	 */
	private function format_data($data, $page = '0', $week = '', $order = '')
	{
		if ($data) {
			$k = 0;
			foreach ($data as $key => $val) {
				if ($week) {
					if ($val['vod_prty'] == 4) {
						$list['data'][$k] = $this->fd($val);
						$k = $k + 1;
					}
				} else {
					$list['data'][$key] = $this->fd($val, $order, count($data));
				}
			}
			if ($page != '0') {
				$list['limit'] = input('limit/d', 0);
				$list['current'] = $data[0]['page']['currentpage'];
				$list['count'] = $data[0]['page']['pagecount'];
				$list['total'] = $data[0]['page']['totalpage'];
			}
			$list['info'] = 'ok';
			$list['status'] = 1;
		} else {
			if ($page != '0') {
				$list['limit'] = 10;
				$list['current'] = 1;
				$list['total'] = 0;
				$list['count'] = 0;
			}
			$list['data'] = [];
			$list['info'] = 'error';
			$list['status'] = 0;
		}
		return json($list);
	}

	/**
	 * 获取视频角色列表
	 */
	private function roleListData($vid, $limit = 10)
	{
		$ajax_list = zanpian_mysql_role('vid:' . $vid . ';limit:' . $limit . ';');
		if ($ajax_list) {
			foreach ($ajax_list as $key => $val) {
				$list[$key]['roleId'] = $val['role_id'];
				$list[$key]['starId'] = $val['star_id'];
				$list[$key]['roleName'] = $val['role_name'];
				$list[$key]['starName'] = $val['star_name'];
				$list[$key]['rolePic'] = $val['role_picurl'];
				$list[$key]['starPic'] = $val['star_picurl'];
			}
			return $list;
		} else {
			return false;
		}
	}


	/**
	 * 获取视频新闻列表
	 */
	private function newsListData($name, $vid, $cid, $isText = '', $limit = 12)
	{
		$ajax_list = zanpian_mysql_news('cid:' . $cid . ';news:' . $name . ';did:' . $vid . ';field:news_id,news_cid,news_name,news_pic,news_addtime,news_jumpurl,news_playname,news_clarity,news_playtime,news_playurl;limit:' . $limit . ';order:news_addtime desc;');
		if ($ajax_list) {
			foreach ($ajax_list as $key => $val) {
				$list[$key]['id'] = $val['news_id'];
				$list[$key]['title'] = $val['news_name'];
				if ($isText) {
					$list[$key]['addtime'] = date('Y-m-d', $val['news_addtime']);
					$list[$key]['playname'] = $val['news_playname'];
					$list[$key]['clarity'] = $val['news_clarity'];
					$list[$key]['playtime'] = $val['news_playtime'];
				} else {
					$list[$key]['cid'] = $val['news_cid'];
					$list[$key]['name'] = $val['list_name'];
					$list[$key]['pic'] = $val['news_picurl'];
					$list[$key]['jump'] = $val['news_jumpurl'];
				}
			}
			return $list;
		} else {
			return false;
		}
	}

	/**
	 * 剧情详情页列表
	 */
	private function storydetaillist($vid, $order = "")
	{
		$story = $this->zanpian_mysql_vod_story($vid);
		if ($story) {
			$arr = $order ? array_reverse($story) : $story;
			foreach ($arr as $i => $val) {
				$name = str_replace(array("\r\n", "\n", "\r"), '', $val['story_name']);
				$pid = $val['story_pid'];
				$title = $val['story_title'];
				if ($order) {
					if ($i < 3) {
						$list[$i]['pid'] = $pid;
						$list[$i]['name'] = $name;
						$list[$i]['title'] = $title;
						$list[$i]['content'] = $val['story_info'];
					}
				} else {
					$list[$i]['pid'] = $i == 0 ? 1 : $pid;
					$list[$i]['name'] = $name;
				}
			}
			return $list;
		} else {
			return false;
		}
	}

	// 详情页调用剧情函数
	private function zanpian_mysql_vod_story($vid = "")
	{
		$where = array();
		$where['story_vid'] = $vid;
		$where['story_status'] = array('eq', 1);
		$array = db('story')->where($where)->find();
		if (!empty($array)) {
			$listarray = explode('||', $array['story_content']); //将每一集分组
			foreach ($listarray as $key => $jiinfo) {
				$jiinfoarray = explode('@@', $jiinfo);
				$arrays[$key]['story_name'] = $jiinfoarray[0];
				$arrays[$key]['story_title'] = $jiinfoarray[1];
				$arrays[$key]['story_info'] = $jiinfoarray[2];
				$arrays[$key]['story_id'] = $array['story_id'];
				if ($key != 0) {
					$arrays[$key]['story_pid'] = $key + 1;
				}
			}
			return $arrays;
		} else {
			return false;
		}
	}

	// 从剧情数据库获取数据
	private function get_cache_detail_story($id)
	{
		if (!$id) {
			return false;
		}
		//优先读取缓存数据
		if (config('data_cache_story')) {
			$array_detail = Cache::get('data_cache_story_' . $id);
			if ($array_detail) {
				return $array_detail;
			}
		}
		//未中缓存则从数据库读取
		$where = array();
		$where['story_id'] = $id;
		$where['story_status'] = array('eq', 1);
		$array = Db::name('story')->field('s.*,actor_id,actor_cid,actor_status,vod_id,vod_cid,vod_mcid,vod_name,vod_aliases,vod_title,vod_keywords,vod_actor,vod_director,vod_content,vod_pic,vod_play,vod_bigpic,vod_diantai,vod_tvcont,vod_tvexp,vod_area,vod_language,vod_year,vod_continu,vod_total,vod_isend,vod_addtime,vod_hits,vod_hits_day,vod_hits_week,vod_hits_month,vod_stars,vod_jumpurl,vod_letter,vod_gold,vod_golder,vod_isfilm,vod_filmtime,vod_length,vod_letters')->alias('s')->join('actor a', 'a.actor_vid = s.story_vid', 'LEFT')->join('vod v', 'v.vod_id = s.story_vid', 'LEFT')->where($where)->find();
		if ($array) {
			//解析标签
			$array_detail = $this->Lable_Story_Read($array);
			if (config('data_cache_story')) {
				Cache::tag('model_story')->set('data_cache_story_' . $id, $array_detail, intval(config('data_cache_story')));
			}
			return $array_detail;
		}
		return false;
	}
}
