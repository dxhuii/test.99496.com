<?php
namespace app\home\controller;
use app\common\controller\Home;	
use think\Request;
class Up extends Home{  
    public function show(){
		$month=input('month/d',date('Ym')).'01';
		$lastday=date('Y-m-d', strtotime("$month +1 month -1 day"));
		$firstday = date("Ym01",strtotime($month));	
        for($x=1;$x<3;$x++){
		$lastdate[]=strtotime("-".$x." month",strtotime($firstday));
        $update[]=strtotime("+".$x." month",strtotime($firstday));		
		}
		krsort($lastdate);
		$Url = param_url();
		if($Url['dir']){
			$Url['id'] = getlist($Url['dir'],'list_dir','list_id');
			$Url['dir'] = getlist($Url['id'],'list_id','list_dir');
		}else{
			$Url['dir'] = getlist($Url['id'],'list_id','list_dir');
		}
		$List = list_search(F('_data/list'),'list_id='.$Url['id']);	
		if($List){
		config('model',$this->request->module().'/'.$this->request->controller().'/'.$this->request->action());
		if($Url['type']){
		config('params',array('id'=>$List[0]['list_id'],'dir'=>$List[0]['list_dir'],'type'=>$Url['type'],'month'=>$Url['month'],'p'=>"zanpianpage"));
		}else{
		config('params',array('id'=>$List[0]['list_id'],'dir'=>$List[0]['list_dir'],'month'=>date('Ym',strtotime($month)),'p'=>"zanpianpage"));		
		}
		config('currentpage',$Url['page']);			
         if($List[0]['list_id']==2){
			$up_title="开播"; 
			 
		 }else{
			$up_title="上映"; 
			 
		 }			
	    $this->assign('param',$Url);
		$this->assign($List[0]);
		$this->assign('lastdate',$lastdate);
        $this->assign('update',$update);
        $this->assign('month',strtotime($month));	
        $this->assign('firstday',strtotime($firstday));	
        $this->assign('lastday',strtotime($lastday));	
        $this->assign('up_title',$up_title);			
		return view('/up_list');
		}
    }
}
// +----------------------------------------------------------------------
// | ZanPianCMS [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.zanpian.com All rights reserved.
// +----------------------------------------------------------------------
// | BBS:  <http://www.feifeicms.cc>
// +----------------------------------------------------------------------
