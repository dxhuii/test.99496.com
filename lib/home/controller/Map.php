<?php
namespace app\home\controller;
use app\common\controller\Home;	
class Map extends Home{  
    public function show(){
		$Url = param_url();
		if($Url['dir']){
			$Url['id'] = getlist($Url['dir'],'list_dir','list_id');
			$Url['dir'] = getlist($Url['id'],'list_id','list_dir');
		}
		config('currentpage',$Url['page']);
		$List = list_search(F('_data/list'),'list_id='.$Url['id']);
		if($List && $List[0]['list_sid']==11){
		$channel = $this->Lable_List($Url,$List[0]);
		$this->assign('page',$Url['page']);
		$this->assign($channel);
		$rss=$this->view->fetch('/'.$channel['list_skin']);
		return xml($rss);
		}else{	
		abort(404,'页面不存在');
		}
    }	
}
// +----------------------------------------------------------------------
// | ZanPianCMS [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.zanpian.com All rights reserved.
// +----------------------------------------------------------------------
// | BBS:  <http://www.feifeicms.cc>
// +----------------------------------------------------------------------
