<?php
namespace app\home\controller;
use app\common\controller\Home;	
class Ajax extends Home{
    public function index(){
		if($this->request->instance()->isAjax()){
        $Url = param_url();
		$this->assign('param',$Url);
		$array_list = list_search(F('_data/list'),'list_id='.$Url['cid']);
			$this->assign('param',$Url);
			$this->assign('list',$array_list[0]);
			return view('/'.$array_list[0]['list_skin_detail']."_ajax");
		}
    }
}
// +----------------------------------------------------------------------
// | ZanPianCMS [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.zanpian.com All rights reserved.
// +----------------------------------------------------------------------
// | BBS:  <http://www.feifeicms.cc>
// +----------------------------------------------------------------------
