<?php
namespace app\home\controller;
use app\common\controller\Home;
class Index extends Home{  
    public function index(){	
		if (!is_file(RUNTIME_PATH .'install/install.lock')) {
			return $this->success('您还没安装'.lang('zanpiancms_name').lang('zanpiancms_version').'程序，请运行 install.php 进入安装!', 'install/index/index');
		}
		$this->assign($this->Lable_Index());
		return view('/zp_index');
    }
}
// +----------------------------------------------------------------------
// | ZanPianCMS [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.zanpian.com All rights reserved.
// +----------------------------------------------------------------------
// | BBS:  <http://www.feifeicms.cc>
// +----------------------------------------------------------------------
