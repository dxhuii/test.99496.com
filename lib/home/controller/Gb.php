<?php
namespace app\home\controller;
use think\Db;
use app\common\controller\Home;
class Gb extends Home{ 
   	public function show(){
		$Url = param_url();	
		$JumpUrl = param_jump($Url);
		$JumpUrl['p'] = 'zanpianpage';
		config('model',$this->request->module().'/'.$this->request->controller().'/'.$this->request->action());
		config('params',$JumpUrl);
		config('currentpage',$Url['page']);
		
		if($Url['id']){
			$info = get_vod_find($Url['id'],'vod_id','vod_id,vod_name,vod_cid,vod_actor');
			if($info){
				$this->assign($info);
				$this->assign('gb_content','影片ID'.$info['vod_id'].'点播出现错误！名称：'.$info['vod_name'].' 主演：'.$info['vod_actor']);
			}
		}
		 if(user_islogin()){
		 $session =session('user_auth'); 
		 $this->assign($session);
		 }
		$this->assign('param',$Url); 
		$this->assign('page',$Url['p']);
		if (\think\Request::instance()->isAjax()){
		return view('/guestbook_ajax');
		}else{
		return view('/guestbook');	
		}
	}
    public function add(){
		$userconfig=F('_data/userconfig_cache');
		$rs = model("Gb");	
		if ($this->request->isPost()){
			if($userconfig['user_code']){
			    //验证码验证
			    $result=$this->checkcode(input('post.validate/s'));	
				if($result!=1){
				  return json(['msg'=>"验证码错误",'rcode'=>-1]);  
		        }
		} 
		$data['gb_uid']=user_islogin();	
		$data['gb_cid']=input('post.gb_cid/d');
		$data['gb_vid']=input('post.gb_vid/d',0);
		$data['gb_nickname']=htmlspecialchars(strip_tags(remove_xss(input('post.gb_nickname/s'))));
		$data['gb_title']=htmlspecialchars(strip_tags(remove_xss(input('post.gb_title/s'))));
		$data['gb_content']=htmlspecialchars(strip_tags(remove_xss(input('post.gb_content/s'))));
		$id=$rs->add($data);
		if($id==1){
			if($userconfig['user_check']==1){
			   return json(['msg'=>"添加留言成功,我们会尽快审核",'rcode'=>1]);  	
			}
		       return json(['msg'=>"添加留言成功",'rcode'=>1]);  	
		}else{
               return json(['msg'=>"添加留言失败".$id,'rcode'=>-1]); 
			 }
		}
		
	}		
	function checkcode($code, $id = 1) {
		if ($code) {
			$verify = new \org\Verify(array('reset' =>false));
			$result = $verify->check($code, $id);
			if (!$result) {
				return "验证码错误";
			}else{
			    return true;
			}
		} else {
			return "验证码为空";
		}
	}		
}
// +----------------------------------------------------------------------
// | ZanPianCMS [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.zanpian.com All rights reserved.
// +----------------------------------------------------------------------
// | BBS:  <http://www.feifeicms.cc>
// +----------------------------------------------------------------------


