<?php
namespace app\home\controller;
use app\common\controller\Home;	
use think\Cache;
use think\Db;
use think\Request;
class Special extends Home{
    public function show(){
		$Url = param_url();
		if($Url['listdir']){
			$Url['id'] = getlist($Url['listdir'],'list_dir','list_id');
			$Url['dir'] = getlist($Url['id'],'list_id','list_dir');
		}else{
			$Url['dir'] = getlist($Url['id'],'list_id','list_dir');
		}
		//config('jumpurl',zanpian_list_url($this->request->module().'/'.$this->request->controller().'/'.$this->request->action(),$JumpUrl,true,false));
		config('model',$this->request->module().'/'.$this->request->controller().'/'.$this->request->action());
		config('params',array('id'=>$Url['id'],'dir'=>$Url['dir'],'p'=>"zanpianpage"));
		config('currentpage',$Url['page']);
		$List = list_search(F('_data/list'),'list_id='.$Url['id']);	
		if($List){
		$channel = $this->Lable_List($Url,$List[0]);
        $this->assign('param',$Url);
		$this->assign($channel);
		return view('/'.$channel['list_skin']);
		}else{	
		abort(404,'页面不存在');
		}
    }	
    public function read(){
        $Url = param_url();
		if($Url['pinyin']){
			$Url['id'] = get_special_info($Url['pinyin'],'special_letters','special_id');
		}			
	    $array_detail = $this->get_cache_detail($Url['id']);
		if($array_detail){
			$this->assign($array_detail['show']);
			$this->assign($array_detail['read']);
			$this->assign('list_vod',$array_detail['list_vod']);
			$this->assign('list_news',$array_detail['list_news']);
			$this->assign('list_star',$array_detail['list_star']);
			return view('/'.$array_detail['read']['special_skin']);
		}else{
		abort(404,'页面不存在');
		}
    }	

// 从数据库获取数据
	private function get_cache_detail($id){
		if(!$id){ return false; }
		//优先读取缓存数据
		if(config('data_cache_special')){
			$array_detail = Cache::get('data_cache_special'.$id);
			if($array_detail){
				return $array_detail;
			}
		}
		//未中缓存则从数据库读取
		$where = array();
		$where['special_id'] = $id;
		$where['special_status'] = array('eq',1);
		$array =Db::name('special')->where($where)->find();
		if(!empty($array)){
			//解析标签
			$array_detail = $this->Lable_Special_Read($array);
     		//print_r($array_detail);
			if( config('data_cache_special') ){
				Cache::tag('model_special')->set('data_cache_special_'.$id,$array_detail,intval(config('data_cache_special'))); 
			}
			return $array_detail;
		}
		return false;
	}		
}
// +----------------------------------------------------------------------
// | ZanPianCMS [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.zanpian.com All rights reserved.
// +----------------------------------------------------------------------
// | BBS:  <http://www.feifeicms.cc>
// +----------------------------------------------------------------------
