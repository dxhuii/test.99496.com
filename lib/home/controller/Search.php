<?php
namespace app\home\controller;
use app\common\controller\Home;	
class Search extends Home{  
    public function index(){
		$Url = param_url();
		$mode= list_search(F('_data/modellist'),'id='.$Url['sid']);
		$JumpUrl = param_jump($Url);
		config('model',strtolower($this->request->module().'/'.$this->request->controller().'/'.$this->request->action()));
		config('currentpage',$Url['page']);
		config('params',array('wd'=>$Url['wd'],'p'=>"zanpianpage"));
		$channel = $this->Lable_Search($Url);
        $this->assign('param',$Url);
		$this->assign($channel);
		if (\think\Request::instance()->isAjax()&& !empty($Url['sid'])){
		$array_search['search_skin'] = 'search_'.$mode[0]['name'].'_ajax';	
		return view('/'.$array_search['search_skin']);
		}else{
		return view('/'.$channel['search_skin']);	
		}
    }
    // 联想搜索
    public function vod(){
		$wd = htmlspecialchars(input('q/s',''));
		$limit = !empty($Url['limit']) ? $Url['limit'] : '10';
		$where['vod_name|vod_letters|vod_aliases'] = array('like',$wd.'%');
		$rs=db('vod');
        $data=$rs->field('vod_id,vod_cid,vod_pic,vod_pic,vod_gold,vod_area,vod_content,vod_continu,vod_actor,vod_title,vod_language,vod_filmtime,vod_director,vod_year,vod_name,vod_title,vod_letters,vod_addtime')->where($where)->limit($limit)->order('vod_addtime desc')->select();
		$count = $rs->where($where)->count('vod_id');
		if($data){
			foreach($data as $key=>$val){
                $list['data'][$key]['vod_name'] = $val['vod_name'];
				$list['data'][$key]['vod_title'] = $val['vod_title'];
	            $list['data'][$key]['vod_url'] = zanpian_data_url('home/vod/read',array('id'=>$val['vod_id'],'pinyin'=>$val['vod_letters'],'cid'=>$val['vod_cid'],'dir'=>getlistname($val['vod_cid'],'list_dir'),'jumpurl'=>$val['vod_jumpurl']));
			}
			return json(['data'=>$list['data'],'info'=>'ok','status'=>1]);
		}else{
			return json(['data'=>$data,'info'=>'ok','status'=>0]);
			}
    }	
    // 联想搜索
    public function ajax(){
		$Url = param_url();
		$limit = !empty($Url['limit']) ? $Url['limit'] : '10';
		$where['vod_name|vod_letters|vod_aliases'] = array('like',$Url['wd'].'%');
		$rs=db('vod');
        $data=$rs->field('vod_id,vod_cid,vod_pic,vod_pic,vod_gold,vod_area,vod_content,vod_continu,vod_actor,vod_title,vod_language,vod_filmtime,vod_director,vod_year,vod_name,vod_title,vod_letters,vod_addtime')->where($where)->limit($limit)->order('vod_addtime desc')->select();
		$count = $rs->where($where)->count('vod_id');
		if($data){
			foreach($data as $key=>$val){
		     $data[$key]['list_id'] = $val['vod_cid'];
		     $data[$key]['list_name'] = getlistname($val['vod_cid'],'list_name');
		     $data[$key]['list_url'] = getlistname($val['vod_cid'],'list_url');
		     $data[$key]['vod_readurl'] = zanpian_data_url('home/vod/read',array('id'=>$val['vod_id'],'pinyin'=>$val['vod_letters'],'cid'=>$val['vod_cid'],'dir'=>getlistname($val['vod_cid'],'list_dir'),'jumpurl'=>$val['vod_jumpurl']));
		     $data[$key]['vod_picurl'] = zanpian_img_url($val['vod_pic']);
			}
			return jsonp($data);
		}
		}
		// 联想搜索
	public function s(){
		$Url = param_url();
		$wd = htmlspecialchars(input('q/s',''));
		$limit = !empty($Url['limit']) ? $Url['limit'] : '10';
		$where['vod_name|vod_letters|vod_actor|vod_keywords'] = array('like','%'.$wd.'%');
		$rs=\think\Db::name('vod');
    $data=$rs->field('vod_id,vod_cid,vod_pic,vod_pic,vod_gold,vod_area,vod_content,vod_continu,vod_actor,vod_title,vod_language,vod_filmtime,vod_director,vod_year,vod_name,vod_title,vod_letters,vod_addtime')->where($where)->limit($limit)->order('vod_addtime desc')->cache(600)->select();
		$count = $rs->where($where)->count('vod_id');
		if($data){
			foreach($data as $key=>$val){
				$list['data'][$key]['id'] = $val['vod_id'];
        $list['data'][$key]['name'] = $val['vod_name'];
				$list['data'][$key]['title'] = $val['vod_title'];
				$list['data'][$key]['year'] = $val['vod_year'];
				if(!strstr($val['vod_pic'],'://')){
					$val['vod_pic'] = config('upload_http_prefix').$val['vod_pic'];
				}
				$list['data'][$key]['pic'] = $val['vod_pic'];
				$list['data'][$key]['gold'] = $val['vod_gold'];
				if(is_numeric($val['vod_continu'])) {
					if($val['vod_continu'] > 0) {
						$val['vod_continu'] = '连载至'.$val['vod_continu'].'话';
					} else {
						$val['vod_continu'] = '完结';
					}
				} else {
					$val['vod_continu'] = $val['vod_title'];
				}
				$list['data'][$key]['continu'] = $val['vod_continu'];
				$list['data'][$key]['type'] = getlistname($val['vod_cid'], 'list_name');
	            $list['data'][$key]['url'] = zanpian_data_url('home/vod/read',array('id'=>$val['vod_id'],'pinyin'=>$val['vod_letters'],'cid'=>$val['vod_cid'],'dir'=>getlistname($val['vod_cid'],'list_dir'),'jumpurl'=>$val['vod_jumpurl']));
			}
			return json(['data'=>$list['data'],'info'=>'ok','status'=>1]);
		}else{
			return json(['data'=>$data,'info'=>'ok','status'=>0]);
		}
  }
	// 弹出信息层
  public function pop(){
		$Url = param_url();
		$wd = htmlspecialchars(input('q/s',''));
		$limit = !empty($Url['limit']) ? $Url['limit'] : '1';
		$where['vod_id'] = $wd;
		$rs=\think\Db::name('vod');
    $data=$rs->field('vod_id,vod_cid,vod_mcid,vod_pic,vod_pic,vod_gold,vod_area,vod_content,vod_continu,vod_actor,vod_title,vod_language,vod_filmtime,vod_director,vod_year,vod_name,vod_title,vod_letters,vod_addtime')->where($where)->limit($limit)->order('vod_addtime desc')->cache(600)->select();
		$count = $rs->where($where)->count('vod_id');
		if($data){
			foreach($data as $key=>$val){
				$list['data'][$key]['id'] = $val['vod_id'];
				$list['data'][$key]['cid'] = $val['vod_cid'];
        $list['data'][$key]['name'] = $val['vod_name'];
				$list['data'][$key]['year'] = $val['vod_year'];
				$list['data'][$key]['mcid'] = mcat_url($val['vod_mcid'],$val['vod_cid']);
				$list['data'][$key]['area'] = "<a target='_blank' href='".zanpian_type_url('home/vod/type',array('id'=>3,'dir'=>getlistdir(3),'mcid'=>"",'area'=>urlencode($val['vod_area']),'year'=>"",'letter'=>"",'order'=>""))."'>".$val['vod_area']."</a>";
				$list['data'][$key]['list_name'] = getlistname($val['vod_cid'],'list_name');
				$list['data'][$key]['list_url'] = getlistname($val['vod_cid'],'list_url');
				$list['data'][$key]['time'] = zanpian_from_time($val['vod_addtime']);
				$list['data'][$key]['actor'] = get_star_url(str_ireplace(array('未知','未录入'),'',$val['vod_actor']),'no',3);
				$list['data'][$key]['director'] = get_star_url(str_ireplace(array('未知','未录入'),'',$val['vod_director']),'no',1);
				$list['data'][$key]['gold'] = $val['vod_gold'];
				$list['data'][$key]['content'] = msubstr($val['vod_content'],0,120,h);
        $list['data'][$key]['url'] = zanpian_data_url('home/vod/read',array('id'=>$val['vod_id'],'pinyin'=>$val['vod_letters'],'cid'=>$val['vod_cid'],'dir'=>getlistname($val['vod_cid'],'list_dir'),'jumpurl'=>$val['vod_jumpurl']));
			}
			return json(['data'=>$list['data'],'info'=>'ok','status'=>1]);
		}else{
			return json(['data'=>$data,'info'=>'ok','status'=>0]);
		}
  }
	// 弹出信息层
  public function xcxdetail(){
		$Url = param_url();
		$wd = htmlspecialchars(input('q/s',''));
		$limit = !empty($Url['limit']) ? $Url['limit'] : '1';
		$where['vod_id'] = $wd;
		$rs=\think\Db::name('vod');
    $data=$rs->field('vod_id,vod_cid,vod_mcid,vod_pic,vod_pic,vod_gold,vod_area,vod_content,vod_continu,vod_actor,vod_title,vod_language,vod_filmtime,vod_total,vod_director,vod_year,vod_name,vod_title,vod_letters,vod_addtime')->where($where)->limit($limit)->order('vod_addtime desc')->cache(600)->select();
		$count = $rs->where($where)->count('vod_id');
		if($data){
			foreach($data as $key=>$val){
				$list['data'][$key]['type'] = getlistname($val['vod_cid'],'list_name');
				$list['data'][$key]['mcid'] = mcat_name($val['vod_mcid'],$val['vod_cid']);
				$list['data'][$key]['id'] = $val['vod_id'];
				$list['data'][$key]['cid'] = $val['vod_cid'];
        $list['data'][$key]['name'] = $val['vod_name'];
				$list['data'][$key]['area'] = $val['vod_area'];
				$list['data'][$key]['year'] = $val['vod_year'];
				if(!strstr($val['vod_pic'],'://')){
					$val['vod_pic'] = config('upload_http_prefix').$val['vod_pic'];
				}
				$list['data'][$key]['pic'] = $val['vod_pic'];
				if(is_numeric($val['vod_continu'])) {
					if($val['vod_continu'] > 0) {
						$val['vod_continu'] = '连载至'.$val['vod_continu'].'话';
					} else {
						$val['vod_continu'] = '完结';
					}
				} else {
					$val['vod_continu'] = $val['vod_title'];
				}
				$list['data'][$key]['continu'] = $val['vod_continu'];
				$list['data'][$key]['filmtime'] = $val['vod_filmtime'];
				$list['data'][$key]['time'] = zanpian_from_time($val['vod_addtime']);
				$list['data'][$key]['actor'] = str_ireplace(array('未知','未录入'),'',$val['vod_actor']);
				$list['data'][$key]['director'] = str_ireplace(array('未知','未录入'),'',$val['vod_director']);
				$list['data'][$key]['gold'] = $val['vod_gold'];
				$list['data'][$key]['content'] = msubstr($val['vod_content'],0,120,h);
			}
			return json(['data'=>$list['data'],'info'=>'ok','status'=>1]);
		}else{
			return json(['data'=>$data,'info'=>'ok','status'=>0]);
		}
	}
	// 是否为跳转
  public function xcxisjump(){
		return json(['status'=>0]);
	}
	/**
	 * 视频详情接口
	 * $id 视频ID 
	*/
  public function reactDetail(){
    header('Access-Control-Allow-Origin: *');
		$Url = param_url();
		$id = htmlspecialchars(input('q/s',''));
		$limit = !empty($Url['limit']) ? $Url['limit'] : '1';
		$where['vod_id'] = $id;
		$rs=\think\Db::name('vod');
    $data=$rs->field('*')->where($where)->limit($limit)->order('vod_addtime desc')->cache(600)->select();
		$count = $rs->where($where)->count('vod_id');
		if($data){
			foreach($data as $key=>$val){
				$list['data'][$key]['id'] = $val['vod_id'];
				$list['data'][$key]['cid'] = $val['vod_cid'];
        $list['data'][$key]['name'] = $val['vod_name'];
				$list['data'][$key]['area'] = $val['vod_area'];
				$list['data'][$key]['year'] = $val['vod_year'];
				$list['data'][$key]['type'] = getlistname($val['vod_cid'],'list_name');
				$list['data'][$key]['mcid'] = mcat_name($val['vod_mcid'],$val['vod_cid']);
				if(!strstr($val['vod_pic'],'://')){
					$val['vod_pic'] = config('upload_http_prefix').$val['vod_pic'];
				}
				$list['data'][$key]['pic'] = $val['vod_pic'];
				$list['data'][$key]['update_new'] = (int) $val['vod_continu'];
				if(is_numeric($val['vod_continu'])) {
					if($val['vod_continu'] > 0) {
						$val['vod_continu'] = '连载至'.$val['vod_continu'].'话';
					} else {
						$val['vod_continu'] = '完结';
					}
				} else {
					$val['vod_continu'] = $val['vod_title'];
				}
				$list['data'][$key]['continu'] = $val['vod_continu'];
				$list['data'][$key]['time'] = zanpian_from_time($val['vod_addtime']);
				$list['data'][$key]['update_date'] = date('Y-m-d H:i:s', $val['vod_addtime']);
				$list['data'][$key]['language'] = $val['vod_language'];
				$list['data'][$key]['total'] = (int) $val['vod_total'];
				$list['data'][$key]['actor'] = str_ireplace(array('未知','未录入'),'',$val['vod_actor']);
				$list['data'][$key]['director'] = str_ireplace(array('未知','未录入'),'',$val['vod_director']);
				$list['data'][$key]['gold'] = $val['vod_gold'];
				$list['data'][$key]['filmtime'] = date('Y-m-d', $val['vod_filmtime']);;
				$list['data'][$key]['aliases'] = $val['vod_aliases'];
				$list['data'][$key]['tag'] = $val['vod_keywords'];
				$list['data'][$key]['company'] = $val['vod_company'];
				$list['data'][$key]['website'] = $val['vod_website'];
				$list['data'][$key]['tvcont'] = $val['vod_tvcont'];
				$list['data'][$key]['repair'] = $val['vod_repair'];
				$list['data'][$key]['repairtitle'] = $val['vod_repairtitle'];
				$list['data'][$key]['pan'] = $val['vod_pan'];
				$list['data'][$key]['pantitle'] = $val['vod_pantitle'];
				$list['data'][$key]['content'] = msubstr($val['vod_content'],0,120,h);
			}
			return json(['data'=>$list['data'][0],'info'=>'ok','status'=>1]);
		}else{
			return json(['data'=>$data,'info'=>'error','status'=>0]);
		}
	}

	/**
	 * 追番表
	 */
  public function reactWeek(){
    header('Access-Control-Allow-Origin: *');
		$Url = param_url();
		$limit = !empty($Url['limit']) ? $Url['limit'] : 1000;
		$where['vod_prty'] = 4;
		$rs=\think\Db::name('vod');
    $data=$rs->field('vod_id,vod_cid,vod_mcid,vod_pic,vod_pic,vod_gold,vod_area,vod_content,vod_continu,vod_weekday,vod_prty,vod_play,vod_url,vod_actor,vod_title,vod_language,vod_filmtime,vod_director,vod_year,vod_name,vod_title,vod_letters,vod_addtime')->where($where)->limit($limit)->order('vod_addtime desc')->cache(600)->select();
		$count = $rs->where($where)->count('vod_id');
		if($data){
			foreach($data as $key=>$val){
				$list['data'][$key]['id'] = $val['vod_id'];
				$list['data'][$key]['title'] = $val['vod_name'];
				$list['data'][$key]['isDate'] = date('Y-m-d')==date('Y-m-d',$val['vod_addtime']) ? 1 : 0;
				$list['data'][$key]['weekday'] = (int) $val['vod_weekday'];
				$list['data'][$key]['area'] = $val['vod_area'];
				if(!strstr($val['vod_pic'],'://')){
					$val['vod_pic'] = config('upload_http_prefix').$val['vod_pic'];
				}
				$list['data'][$key]['pic'] = $val['vod_pic'];
				if(is_numeric($val['vod_continu'])) {
					if($val['vod_continu'] > 0) {
						$val['vod_continu'] = (int) $val['vod_continu'];
					} else {
						$val['vod_continu'] = '完结';
					}
				} else {
					$val['vod_continu'] = $val['vod_title'];
				}
				$lastplayurl = play_url_end($val['vod_url'],$val['vod_play']);
				$list['data'][$key]['sid'] = $lastplayurl[0];
				$list['data'][$key]['pid'] = $lastplayurl[1];
				$list['data'][$key]['status'] = $val['vod_continu'];
			}
			return json(['data'=>$list['data'],'info'=>'ok','status'=>1]);
		}else{
			return json(['data'=>$data,'info'=>'error','status'=>0]);
		}
	}
}
// +----------------------------------------------------------------------
// | ZanPianCMS [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.zanpian.com All rights reserved.
// +----------------------------------------------------------------------
// | BBS:  <http://www.feifeicms.cc>
// +----------------------------------------------------------------------
