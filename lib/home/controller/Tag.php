<?php
namespace app\home\controller;
use app\common\controller\Home;
use think\Cache;
use think\Request;
class Tag extends Home{ 
    public function show(){
		$Url = param_url();
		if($Url['dir']){
			$mode= list_search(F('_data/modellist'),'name='.$Url['dir']);	
		}else{
			$mode= list_search(F('_data/modellist'),'id='.$Url['id']);
		}
		if($mode[0]){
		config('model',$this->request->module().'/'.$this->request->controller().'/'.$this->request->action());
		config('params',array('id'=>$mode[0]['id'],'dir'=>$mode[0]['name'],'tag'=>urlencode($Url['tag']),'p'=>"zanpianpage"));
		config('currentpage',$Url['page']);			
		$channel = $this->Lable_Tag($Url,$mode[0]);
        $this->assign('param',$Url);
		$this->assign($channel);
		//AJAX页面加载
		if (\think\Request::instance()->isAjax()){
		$channel['list_skin_ajax']=$channel['list_skin']."_ajax";
		return view('/'.$channel['list_skin_ajax']);
		}else{
		return view('/'.$channel['list_skin']);	
		}
		}else{	
		abort(404,'页面不存在');
		}
    } 	
}
// +----------------------------------------------------------------------
// | ZanPianCMS [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.zanpian.com All rights reserved.
// +----------------------------------------------------------------------
// | BBS:  <http://www.feifeicms.cc>
// +----------------------------------------------------------------------
