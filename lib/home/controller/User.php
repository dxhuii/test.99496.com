<?php
namespace app\home\controller;
use think\Controller;
class User extends Controller{  
   public function login(){
	    return view(ROOT_PATH.'tpl' . DS . 'user' . DS .config('user_theme').DS.'login_ajax');
    }
   public function reg(){
	    return view(ROOT_PATH.'tpl' . DS . 'user' . DS .config('user_theme').DS.'reg_ajax');
    }
   public function loginout(){
	    $jump=input('jump/d',0);
		cookie('user_auth', null);
		cookie('user_auth_sign', null);
		session('user_auth', null);
		session('user_auth_sign', null);
		session('user_auth_temp',null);
		if($jump){
			return json(["msg"=>"退出成功","redir"=>zanpian_user_url('user/login/index'),"rcode" =>"1",'wantredir'=>1]);	
		}else{
		    return json(["msg"=>"退出成功","rcode" =>1]);	
		}	
    }
}
// +----------------------------------------------------------------------
// | ZanPianCMS [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.zanpian.com All rights reserved.
// +----------------------------------------------------------------------
// | BBS:  <http://www.feifeicms.cc>
// +----------------------------------------------------------------------
