<?php
namespace app\home\controller;
use app\common\controller\Home;	
use think\Cache;
use think\Request;
class Menu extends Home{  
    public function show(){
		$Url = param_url();
		if($Url['listdir']){
			$Url['id'] = getlist($Url['listdir'],'list_dir','list_id');
			$Url['dir'] = getlist($Url['id'],'list_id','list_dir');
		}else{
			$Url['dir'] = getlist($Url['id'],'list_id','list_dir');
		}
		//config('jumpurl',zanpian_list_url($this->request->module().'/'.$this->request->controller().'/'.$this->request->action(),$JumpUrl,true,false));
		config('model',$this->request->module().'/'.$this->request->controller().'/'.$this->request->action());
		config('params',array('id'=>$Url['id'],'dir'=>$Url['dir'],'p'=>"zanpianpage"));
		config('currentpage',$Url['page']);
		$List = list_search(F('_data/list'),'list_id='.$Url['id']);	
		if($List){
		$channel = $this->Lable_List($Url,$List[0]);
        $this->assign('param',$Url);
		$this->assign($channel);
		return view('/'.$channel['list_skin']);
		}else{	
		abort(404,'页面不存在');
		}
    }
    //列表检索
    public function type(){
		$Url = param_url();
		if($Url['listdir']){
			$Url['id'] = getlist($Url['listdir'],'list_dir','list_id');
			$Url['dir'] = getlist($Url['id'],'list_id','list_dir');
		}		
		$JumpUrl = param_jump($Url);
		$JumpUrl['p'] = 'zanpianpage';
		config('jumpurl',zanpian_url($this->request->module().'/'.$this->request->controller().'/'.$this->request->action(),$JumpUrl,true,false));
		config('currentpage',$Url['page']);
		$List = list_search(F('_data/list'),'list_id='.$Url['id']);
		if($List){
		$channel = $this->Lable_List($Url,$List[0]);
		$channel['thisurl'] = str_replace('zanpianpage',$param['page'],zanpian_url($this->request->module().'/'.$this->request->controller().'/'.$this->request->action(),array('id'=>$channel['list_id'],'listdir'=>$channel['list_dir'],'mcid'=>$Url['mcid'],'area'=>urlencode($Url['area']),'langaue'=>urlencode($Url['langaue']),'year'=>$Url['year'],'letter'=>$Url['letter'],'order'=>'vod_'.$Url['order'],'p'=>$param['page']),true,false));
        $this->assign('param',$Url);
		$this->assign($channel);
		//AJAX页面加载
		if (\think\Request::instance()->isAjax()){
		$channel['list_skin_ajax']=$channel['list_skin_type']."_ajax";
		return view('/'.$channel['list_skin_ajax']);
		}else{
		return view('/'.$channel['list_skin_type']);	
		}
		}else{	
		abort(404,'页面不存在');
		}
    }		
}
// +----------------------------------------------------------------------
// | ZanPianCMS [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.zanpian.com All rights reserved.
// +----------------------------------------------------------------------
// | BBS:  <http://www.feifeicms.cc>
// +----------------------------------------------------------------------
