<?php
namespace app\home\controller;
use app\common\controller\Home;	
use think\Cache;
use think\Db;
use think\Request;
class Star extends Home{
    public function show(){
		$Url = param_url();
		if($Url['dir']){
			$Url['id'] = getlist($Url['dir'],'list_dir','list_id');
			$Url['dir'] = getlist($Url['id'],'list_id','list_dir');
		}else{
			$Url['dir'] = getlist($Url['id'],'list_id','list_dir');
		}
		//config('jumpurl',zanpian_list_url($this->request->module().'/'.$this->request->controller().'/'.$this->request->action(),$JumpUrl,true,false));
		config('model',$this->request->module().'/'.$this->request->controller().'/'.$this->request->action());
		config('params',array('id'=>$Url['id'],'dir'=>$Url['dir'],'p'=>"zanpianpage"));
		config('currentpage',$Url['page']);
		$List = list_search(F('_data/list'),'list_id='.$Url['id']);	
		if($List && $List[0]['list_sid']==3){
		$channel = $this->Lable_List($Url,$List[0]);
        $this->assign('param',$Url);
		$this->assign($channel);
		return view('/'.$channel['list_skin']);
		}else{	
		abort(404,'页面不存在');
		}
    }
  //列表检索
    public function type(){
		$Url = param_url();
		if($Url['order']=="addtime"){
		$Url['order']="";
		}
		if($Url['dir']){
			$Url['id'] = getlist($Url['dir'],'list_dir','list_id');
			$Url['dir'] = getlist($Url['id'],'list_id','list_dir');
		}else{
			$Url['dir'] = getlist($Url['id'],'list_id','list_dir');
		}	
		config('model',$this->request->module().'/'.$this->request->controller().'/'.$this->request->action());
		config('params',array('id'=>$Url['id'],'dir'=>$Url['dir'],'sex'=>$Url['sex'],'zy'=>$Url['zy'],'area'=>$Url['area'],'letter'=>$Url['letter'],'order'=>$Url['order'],'p'=>"zanpianpage"));
		config('currentpage',$Url['page']);
		$List = list_search(F('_data/list'),'list_id='.$Url['id']);
		if($List && $List[0]['list_sid']==3){
		$channel = $this->Lable_List($Url,$List[0]);
		$channel['thisurl']="";
		$channel['thisurl'] = str_replace('zanpianpage',$Url['page'],zanpian_url($this->request->module().'/'.$this->request->controller().'/'.$this->request->action(),config('params')));
        $this->assign('param',$Url);
		$this->assign($channel);
		//AJAX页面加载
		if (\think\Request::instance()->isAjax()){
		$channel['list_skin_ajax']=$channel['list_skin_type']."_ajax";
		return view('/'.$channel['list_skin_ajax']);
		}else{
		return view('/'.$channel['list_skin_type']);	
		}
		}else{	
		abort(404,'页面不存在');
		}
    }	  	
    public function read(){
        $Url = param_url();
		if($Url['pinyin']){
			$Url['id'] = get_star_info($Url['pinyin'],'star_letters','star_id');
		}			
	    $array_detail = $this->get_cache_detail($Url['id']);
		if($array_detail){
			$this->assign($array_detail['show']);
			$this->assign($array_detail['read']);
			return view('/'.$array_detail['read']['star_skin_detail']);
		}else{
		abort(404,'页面不存在');
		}
    }
    public function ajax(){
		if($this->request->instance()->isAjax()){
        $Url = param_url();
		if($Url['pinyin']){
			$Url['id'] = get_star_info($Url['pinyin'],'star_letters','star_id');
		}			
	    $array_detail = $this->get_cache_detail($Url['id']);
		if($array_detail){
			$this->assign($array_detail['show']);
			$this->assign($array_detail['read']);
			return view('/'.$array_detail['read']['star_skin_detail']."_ajax");
		}else{
		abort(404,'页面不存在');
		}
		}
    }	
    public function work(){
        $Url = param_url();
		if($Url['pinyin']){
			$Url['id'] = get_star_info($Url['pinyin'],'star_letters','star_id');
		}
		if($Url['vcid']){
			$Url['vdir']=getlist($Url['vcid'],'list_id','list_dir');
		}else{
			$Url['vcid']=getlist($Url['vdir'],'list_dir','list_id');
			
		}	
	    $array_detail = $this->get_cache_detail($Url['id']);
		config('model',$this->request->module().'/'.$this->request->controller().'/'.$this->request->action());
		config('params',array('id'=>$array_detail['read']['star_id'],'pinyin'=>$array_detail['read']['star_letters'],'cid'=>$array_detail['read']['star_cid'],'dir'=>getlistname($array_detail['read']['star_cid'],'list_dir'),'vcid'=>$Url['vcid'],'vdir'=>$Url['vdir'],'p'=>"zanpianpage"));
		config('currentpage',$Url['page']);			
		if($array_detail){
			$this->assign('param',$Url);
            $this->assign('page',$Url['page']);
			$this->assign($array_detail['show']);
			$this->assign($array_detail['read']);
			$this->assign('thisurl',str_replace('zanpianpage',$Url['page'],zanpian_data_url('home/star/work',array('id'=>$array_detail['read']['star_id'],'pinyin'=>$array_detail['read']['star_letters'],'cid'=>$array_detail['read']['star_cid'],'dir'=>getlistname($array_detail['read']['star_cid'],'list_dir'),'p'=>$Url['page']))));
			return view('/'.$array_detail['read']['star_skin_detail'].'_work');
		}else{
		abort(404,'页面不存在');
		}
    }
    public function info(){
        $Url = param_url();
		if($Url['pinyin']){
			$Url['id'] = get_star_info($Url['pinyin'],'star_letters','star_id');
		}		
	    $array_detail = $this->get_cache_detail($Url['id']);
		config('model',$this->request->module().'/'.$this->request->controller().'/'.$this->request->action());
		config('params',array('id'=>$array_detail['read']['star_id'],'pinyin'=>$array_detail['read']['star_letters'],'cid'=>$array_detail['read']['star_cid'],'dir'=>getlistname($array_detail['read']['star_cid'],'list_dir'),'p'=>"zanpianpage"));
		config('currentpage',$Url['page']);	
		if($array_detail){
			$this->assign('page',$Url['page']);
			$this->assign($array_detail['show']);
			$this->assign($array_detail['read']);
			$this->assign('thisurl',str_replace('zanpianpage',$Url['page'],zanpian_data_url('home/star/info',array('id'=>$array_detail['read']['star_id'],'pinyin'=>$array_detail['read']['star_letters'],'cid'=>$array_detail['read']['star_cid'],'dir'=>getlistname($array_detail['read']['star_cid'],'list_dir'),'p'=>$Url['page']))));
			return view('/'.$array_detail['read']['star_skin_detail'].'_info');
		}else{
		abort(404,'页面不存在');
		}
    }	
    public function hz(){
        $Url = param_url();
		if($Url['pinyin']){
			$Url['id'] = get_star_info($Url['pinyin'],'star_letters','star_id');
		}		
	    $array_detail = $this->get_cache_detail($Url['id']);
		config('model',$this->request->module().'/'.$this->request->controller().'/'.$this->request->action());
		config('params',array('id'=>$array_detail['read']['star_id'],'pinyin'=>$array_detail['read']['star_letters'],'cid'=>$array_detail['read']['star_cid'],'dir'=>getlistname($array_detail['read']['star_cid'],'list_dir'),'p'=>"zanpianpage"));
		config('currentpage',$Url['page']);	
		if($array_detail){
			$this->assign('param',$Url);
            $this->assign('page',$Url['page']);
			$this->assign($array_detail['show']);
			$this->assign($array_detail['read']);
			$this->assign('thisurl',str_replace('zanpianpage',$Url['page'],zanpian_data_url('home/star/hz',array('id'=>$array_detail['read']['star_id'],'pinyin'=>$array_detail['read']['star_letters'],'cid'=>$array_detail['read']['star_cid'],'dir'=>getlistname($array_detail['read']['star_cid'],'list_dir'),'p'=>$Url['page']))));
			return view('/'.$array_detail['read']['star_skin_detail'].'_hz');
		}else{
		abort(404,'页面不存在');
		}
    }	
    public function role(){
        $Url = param_url();
		if($Url['pinyin']){
			$Url['id'] = get_star_info($Url['pinyin'],'star_letters','star_id');
		}	
	    $array_detail = $this->get_cache_detail($Url['id']);
		config('model',$this->request->module().'/'.$this->request->controller().'/'.$this->request->action());
		config('params',array('id'=>$array_detail['read']['star_id'],'pinyin'=>$array_detail['read']['star_letters'],'cid'=>$array_detail['read']['star_cid'],'dir'=>getlistname($array_detail['read']['star_cid'],'list_dir'),'p'=>"zanpianpage"));
		config('currentpage',$Url['page']);	
		if($array_detail){
			$this->assign('param',$Url);
            $this->assign('page',$Url['page']);
			$this->assign($array_detail['show']);
			$this->assign($array_detail['read']);
			$this->assign('thisurl',str_replace('zanpianpage',$Url['page'],zanpian_data_url('home/star/role',array('id'=>$array_detail['read']['star_id'],'pinyin'=>$array_detail['read']['star_letters'],'cid'=>$array_detail['read']['star_cid'],'dir'=>getlistname($array_detail['read']['star_cid'],'list_dir'),'p'=>$Url['page']))));
			return view('/'.$array_detail['read']['star_skin_detail'].'_role');
		}else{
		abort(404,'页面不存在');
		}
    }
    public function news(){
        $Url = param_url();
		if($Url['pinyin']){
			$Url['id'] = get_star_info($Url['pinyin'],'star_letters','star_id');
		}	
	    $array_detail = $this->get_cache_detail($Url['id']);
		config('model',$this->request->module().'/'.$this->request->controller().'/'.$this->request->action());
		config('params',array('id'=>$array_detail['read']['star_id'],'pinyin'=>$array_detail['read']['star_letters'],'cid'=>$array_detail['read']['star_cid'],'dir'=>getlistname($array_detail['read']['star_cid'],'list_dir'),'p'=>"zanpianpage"));
		config('currentpage',$Url['page']);	
		if($array_detail){
			$this->assign('param',$Url);
            $this->assign('page',$Url['page']);
			$this->assign($array_detail['show']);
			$this->assign($array_detail['read']);
			$this->assign('thisurl',str_replace('zanpianpage',$Url['page'],zanpian_data_url('home/star/news',array('id'=>$array_detail['read']['star_id'],'pinyin'=>$array_detail['read']['star_letters'],'cid'=>$array_detail['read']['star_cid'],'dir'=>getlistname($array_detail['read']['star_cid'],'list_dir'),'p'=>$Url['page']))));
			return view('/'.$array_detail['read']['star_skin_detail'].'_news');
		}else{
		abort(404,'页面不存在');
		}
    }		

// 从数据库获取数据
	private function get_cache_detail($id){
		if(!$id){ return false; }
		//优先读取缓存数据
		if(config('data_cache_star')){
			$array_detail = Cache::get('data_cache_star'.$id);
			if($array_detail){
				return $array_detail;
			}
		}
		//未中缓存则从数据库读取
		$where = array();
		$where['star_id'] = $id;
		$where['star_status'] = array('eq',1);
		$array = $array =Db::name('star')->where($where)->find();
		if(!empty($array)){
			//解析标签
			$array_detail = $this->Lable_Star_Read($array);
			if( config('data_cache_star') ){
				Cache::tag('model_star')->set('data_cache_star_'.$id,$array_detail,intval(config('data_cache_star'))); 
			}
			return $array_detail;
		}
		return false;
	}		
}
// +----------------------------------------------------------------------
// | ZanPianCMS [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.zanpian.com All rights reserved.
// +----------------------------------------------------------------------
// | BBS:  <http://www.feifeicms.cc>
// +----------------------------------------------------------------------
