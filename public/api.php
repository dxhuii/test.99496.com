<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------
// 解决跨域问题
$origin = isset($_SERVER['HTTP_ORIGIN']) ? $_SERVER['HTTP_ORIGIN'] : '';
$allow_origin = array(
    'https://m.99496.com',
    'https://www.99496.com',
    'https://api.99496.com',
    'https://dev.99496.com',
    'https://www.dddm.tv',
    'https://api.dddm.tv',
    'https://www.5ddm.com',
    'https://www.kanfan.net',
    'http://a.99496.com',
    'http://localhost:4000',
    'http://localhost:4001'
);
header("Content-Type: text/html; charset=UTF-8");
if (in_array($origin, $allow_origin)) {
    header('Access-Control-Allow-Origin:' . $origin);
}

// header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Credentials: true');
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
header('Access-Control-Allow-Methods: GET, POST');

error_reporting(E_ALL & ~E_NOTICE);
error_reporting(0);
// [ 应用入口文件 ]
@set_time_limit(300);
@ini_set("memory_limit", '-1');
$token = 'xichen_plain';
$params = $_POST;
if(array_key_exists('sign',$params) || array_key_exists('Sign',$params)){
    $sign = isset($params['sign'])?$params['sign']:$params['Sign'];
    unset($params['sign']);
    unset($params['Sign']);
    $sign2 = makeSign($params,$token);
    if($sign == $sign2) {
        $check = true;
    } else {
        $check = false;
    }
} else {
    $check = false;
}

// 如果存在
if ($check !== false) {
    // 定义入口文件目录
    define('URL_PATH', __DIR__ . '/');
    // 定义PUBLIC目录名称
    define('PUBLIC_URL', basename(dirname(__FILE__)));
    // 定义入口文件所在位置
    define('PUBLIC_PATH', '');
    // 定义应用目录
    define('APP_PATH', __DIR__ . '/../lib/');
    // 加载框架引导文件
    require __DIR__ . '/../thinkphp/start.php';
} else {
    http_response_code(404);
    exit;
}

function makeSign($params, $token)
{
    if (array_key_exists('sign', $params)) {
        unset($params['sign']);
    }
    ksort($params);
    $str = '';
    foreach ($params as $key => $value) {
        $str .= $value;
    }

    return md5($str . $token);
}
