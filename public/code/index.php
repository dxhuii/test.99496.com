<?php
$sub_from=$_SERVER["HTTP_HOST"]; 
if($sub_from<>'www.99496.com'){
    echo("error"); 
    die(); 
}    
    $PNG_TEMP_DIR = dirname(__FILE__).DIRECTORY_SEPARATOR.'file'.DIRECTORY_SEPARATOR;
    $PNG_WEB_DIR = '/code/file/';
    include "qrlib.php";    
    if (!file_exists($PNG_TEMP_DIR))
        mkdir($PNG_TEMP_DIR);
    $filename = $PNG_TEMP_DIR.'code.png'; 
    $errorCorrectionLevel = 'H';
    if (isset($_REQUEST['level']) && in_array($_REQUEST['level'], array('L','M','Q','H')))
        $errorCorrectionLevel = $_REQUEST['level'];    
         $matrixPointSize = 8;
    if (isset($_REQUEST['size']))
        $matrixPointSize = min(max((int)$_REQUEST['size'], 1), 8);

    if (isset($_REQUEST['data'])) { 
    
        if (trim($_REQUEST['data']) == '')
          die('');
            
        $filename = $PNG_TEMP_DIR.''.md5($_REQUEST['data'].'|'.$errorCorrectionLevel.'|'.$matrixPointSize).'.png';
        QRcode::png($_REQUEST['data'], $filename, $errorCorrectionLevel, $matrixPointSize, 8);    
        
    } else {       
        QRcode::png('PHP QR Code :)', $filename, $errorCorrectionLevel, $matrixPointSize, 8);    
        
    }    
    echo 'document.writeln("<img  width=\"200\" height=\"200\" src=\"'.$PNG_WEB_DIR.basename($filename).'\">")';  
  
