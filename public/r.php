<?php
error_reporting(0);
set_time_limit(600);

/**
 * 判断是否为HTTPS请求
 *
 * @return bool
 */
function IsHttps()
{
    if (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https') {
        return true;
    } elseif (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') {
        return true;
    } elseif (isset($_SERVER['HTTP_HTTPS']) && $_SERVER['HTTP_HTTPS'] == 'on') {
        return true;
    } elseif (isset($_SERVER['REQUEST_SCHEME']) && $_SERVER['REQUEST_SCHEME'] === 'https') {
        return true;
    } elseif (isset($_SERVER['SERVER_PORT']) && $_SERVER['SERVER_PORT'] === '443') {
        return true;
    } else {
        return false;
    }
}

/**
 * 获取IP地址
 *
 *
 * @return string
 */
function getIPAddress(){
    if(!empty($_SERVER["HTTP_CLIENT_IP"])){
        $cip = $_SERVER["HTTP_CLIENT_IP"];
    }else if(!empty($_SERVER["HTTP_X_FORWARDED_FOR"])){
        $cip = $_SERVER["HTTP_X_FORWARDED_FOR"];
    }
    else if(!empty($_SERVER["REMOTE_ADDR"]))
    {
        $cip = $_SERVER["REMOTE_ADDR"];
    }else {
        $cip = "";
    }
    preg_match("/[\d\.]{7,15}/", $cip, $cips);
    $cip = $cips[0] ? $cips[0] : '';
    unset($cips);
    return $cip;
}

// 解决跨域问题
$origin = isset($_SERVER['HTTP_ORIGIN']) ? $_SERVER['HTTP_ORIGIN'] : '';
$allow_origin = array(
    'https://m.99496.com',
    'https://www.99496.com',
    'https://api.99496.com',
    'https://dev.99496.com',
    'https://www.dddm.tv',
    'https://api.dddm.tv',
    'https://www.5ddm.com',
    'https://www.kanfan.net',
    'http://a.99496.com',
    'http://test.99496.com',
    'http://localhost:4000',
    'http://localhost:4001'
);
header("Content-Type: text/html; charset=UTF-8");
if (in_array($origin, $allow_origin)) {
    header('Access-Control-Allow-Origin:' . $origin);
}

// header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Credentials: true');
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
header('Access-Control-Allow-Methods: GET, POST');


unset($host);
unset($_REQUEST['domain']);
unset($_POST['domain']);
unset($_GET['domain']);

define('APP_URL', "http://test.99496.com/api.php");

class getApiData
{
    public $apiurl;
    public $token;
    public $clienttoken;
    public $baseData = array();

    function __construct()
    {
        $this->apiurl = APP_URL;

        $this->token = 'xichen_plain';
    }

    function _submit($commit_url, $paramss)
    {
        $postdata = http_build_query($paramss);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
        curl_setopt($ch, CURLOPT_URL, $commit_url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $ret = curl_exec($ch);
        curl_close($ch);

        return $ret;
    }

    function _get($commit_url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $commit_url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $ret = curl_exec($ch);
        curl_close($ch);

        return $ret;
    }

    function _sign($params, $token)
    {
        if (array_key_exists('sign', $params)) {
            unset($params['sign']);
        }
        ksort($params);
        $str = '';
        foreach ($params as $key => $value) {
            $str .= $value;
        }

        return md5($str . $token);
    }

    function getData($data)
    {
        //返回请求数据
        $this->baseData['timestamp'] = time();

        $paramsData = array_merge($this->baseData, $data);
        $paramsData['sign'] = $this->_sign($paramsData, $this->token);

        $result = $this->_submit($this->apiurl.'?s='.$paramsData['s'], $paramsData);

        return $result;
    }
}

//获取post数据
$paramss = array();
$ip = GetIPAddress();
if (! empty($_POST)) {
    $paramss = $_POST;
    $defaultArray = array(
        "Model"         => "web",
        "SystemName"    => "H5",
        "Version"       => "1.0",
        "Format"        => "json",
    );

    $paramss = array_merge($defaultArray, $paramss);
    $apiData = new getApiData();
    $paramss['Ip'] = $ip;

    echo $apiData->getData($paramss);
}
