<?php
namespace addons\database;
use app\common\library\Menu;
use think\Addons;
/**
 * 数据库插件
 */
class Database extends Addons{

    /**
     * 插件安装方法
     * @return bool
     */
    public function install(){
        $menu = [
            [
                'name'    => 'admin/database/index',
                'title'   => '数据库管理',
                'icon'    => 'fa fa-imdb',
                'remark'  => '可在线进行一些简单的数据库表优化或修复,查看表结构和数据。也可以进行SQL语句的操作',
                'sublist' => [
                    ['name' => 'admin/database/query', 'title' => '查询'],
                ]
            ]
        ];
        Menu::create($menu, 'admin/data/show');
        return true;
    }

    /**
     * 插件卸载方法
     * @return bool
     */
    public function uninstall(){
        Menu::delete('admin/database/index');
        return true;
    }

}
