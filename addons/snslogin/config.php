<?php

return array (
  0 => 
  array (
    'name' => 'api_login_auth',
    'title' => '绑定用户',
    'type' => 'select',
    'content' => 
    array (
      1 => '开启',
      0 => '关闭',
    ),
    'value' => '1',
    'rule' => 'required',
    'msg' => '',
    'tip' => '',
    'ok' => '',
    'extend' => '',
  ),
  1 => 
  array (
    'name' => 'api_qq_key',
    'title' => 'QQ:App_Id',
    'type' => 'string',
    'content' => 
    array (
    ),
    'value' => '101364273',
    'rule' => 'required',
    'msg' => '',
    'tip' => 'App_Id',
    'ok' => '',
    'extend' => '',
  ),
  2 => 
  array (
    'name' => 'api_qq_secret',
    'title' => 'QQ:App_Key',
    'type' => 'string',
    'content' => 
    array (
    ),
    'value' => 'd9d3b7af966b5ead61b52b09854d7fc7',
    'rule' => 'required',
    'msg' => '',
    'tip' => 'APP_Key',
    'ok' => '',
    'extend' => '',
  ),
  3 => 
  array (
    'name' => 'api_qq_callback',
    'title' => 'QQ回调地址',
    'type' => 'string',
    'content' => 
    array (
    ),
    'value' => 'https://www.99496.com/index.php/user-snslogin-callback.html',
    'rule' => 'required',
    'msg' => '',
    'tip' => '一般值需要修改域名即可',
    'ok' => '',
    'extend' => '',
  ),
  4 => 
  array (
    'name' => 'api_weibo_key',
    'title' => '微博:App_Key',
    'type' => 'string',
    'content' => 
    array (
    ),
    'value' => '111884427',
    'rule' => 'required',
    'msg' => '',
    'tip' => '微博:App_Key',
    'ok' => '',
    'extend' => '',
  ),
  5 => 
  array (
    'name' => 'api_weibo_secret',
    'title' => '微博:App_Key',
    'type' => 'string',
    'content' => 
    array (
    ),
    'value' => '563a21ed5dd3808ce9eeff91745ceaae',
    'rule' => 'required',
    'msg' => '',
    'tip' => 'App_Secret',
    'ok' => '',
    'extend' => '',
  ),
  6 => 
  array (
    'name' => 'api_weibo_callback',
    'title' => '微博回调地址',
    'type' => 'string',
    'content' => 
    array (
    ),
    'value' => 'https://www.99496.com/index.php/user-snslogin-callback.html',
    'rule' => 'required',
    'msg' => '',
    'tip' => '一般只需要修改域名即可',
    'ok' => '',
    'extend' => '',
  ),
);