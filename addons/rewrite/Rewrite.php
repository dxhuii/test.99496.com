<?php
namespace addons\rewrite;
use app\common\library\Menu;
use think\Addons;
/**
 * 数据库插件
 */
class Rewrite extends Addons{

    /**
     * 插件安装方法
     * @return bool
     */
    public function install(){
        $menu = [
            [
                'name'    => 'admin/rewrite/index',
                'title'   => '伪静态生成',
                'icon'    => 'fa fa-bolt',
                'remark'  => '.htaccess规则一般为：apache或者IIS6.0+Rewrite3 使用<br />
.conf 格式规则一般为：nginx的伪静态格式<br />
httpd.ini    规则一般为：一般情况为IIS6.0+Rewrite2的伪静态格式<br />
web.config一般为IIS7以上环境使用，请使用.htaccess规则删除RewriteBase /然后在IIS中导入规则即可',
				'sublist' => [
                    ['name' => 'admin/rewrite/htaccess', 'title' => 'htaccess规则'],
                    ['name' => 'admin/rewrite/nginx', 'title' => 'Nginx规则'],
                    ['name' => 'admin/rewrite/httpd', 'title' => 'httpd.ini规则'],						
                ]
            ]
        ];
        Menu::create($menu, 'admin/config/config');
        return true;
    }

    /**
     * 插件卸载方法
     * @return bool
     */
    public function uninstall(){
        Menu::delete('admin/rewrite/index');
        return true;
    }

}
