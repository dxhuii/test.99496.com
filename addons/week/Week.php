<?php
namespace addons\week;
use app\common\library\Menu;
use think\Addons;
/**
 * 数据库插件
 */
class Week extends Addons{
    /**
     * 插件安装方法
     * @return bool
     */
    public function install(){
        $menu = [
            [
                'name'    => 'admin/week/index',
                'title'   => '周期采集',
                'remark'  => '',
				'sort'  => 4,
				'type' => 2
            ]
        ];
        Menu::create($menu, 'admin/collect/info');
        return true;
    }
    /**
     * 插件卸载方法
     * @return bool
     */
    public function uninstall(){
        Menu::delete('admin/week/index');
        return true;
    }

}
