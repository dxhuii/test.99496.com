<?php

return array (
  0 => 
  array (
    'name' => 'barrage_status',
    'title' => '弹幕开启关闭',
    'type' => 'select',
    'content' => 
    array (
      0 => '关闭',
      1 => '开启',
    ),
    'value' => '1',
    'rule' => 'required',
    'msg' => '',
    'tip' => '',
    'ok' => '',
    'extend' => '',
  ),
  1 => 
  array (
    'name' => 'barrage_order_type',
    'title' => '弹幕展示方式',
    'type' => 'select',
    'content' => 
    array (
      'cm_addtime' => '时间',
      'cm_id' => 'ID',
      'cm_support' => '支持',
    ),
    'value' => 'cm_addtime',
    'rule' => 'required',
    'msg' => '',
    'tip' => '',
    'ok' => '',
    'extend' => '',
  ),
  2 => 
  array (
    'name' => 'barrage_limit',
    'title' => '弹幕显示数量',
    'type' => 'string',
    'content' => 
    array (
    ),
    'value' => '30',
    'rule' => 'required',
    'msg' => '',
    'tip' => '弹幕显示数量',
    'ok' => '',
    'extend' => '',
  ),
  3 => 
  array (
    'name' => 'barrage_info',
    'title' => '弹幕内容长度',
    'type' => 'string',
    'content' => 
    array (
    ),
    'value' => '20',
    'rule' => 'required',
    'msg' => '',
    'tip' => '弹幕内容截取长度',
    'ok' => '',
    'extend' => '',
  ),
  4 => 
  array (
    'name' => 'barrage_time',
    'title' => '发送间隔时间',
    'type' => 'string',
    'content' => 
    array (
    ),
    'value' => '4000',
    'rule' => 'required',
    'msg' => '',
    'tip' => '两条弹幕间隔时间',
    'ok' => '',
    'extend' => '',
  ),
  5 => 
  array (
    'name' => 'barrage_speed',
    'title' => '弹幕延迟时间',
    'type' => 'string',
    'content' => 
    array (
    ),
    'value' => '10',
    'rule' => 'required',
    'msg' => '',
    'tip' => '发送弹幕延时时间',
    'ok' => '',
    'extend' => '',
  ),
  6 => 
  array (
    'name' => 'barrage_color',
    'title' => '弹幕文字颜色',
    'type' => 'string',
    'content' => 
    array (
    ),
    'value' => '#fff ',
    'rule' => 'required',
    'msg' => '',
    'tip' => '弹幕文字颜色',
    'ok' => '',
    'extend' => '',
  ),
  7 => 
  array (
    'name' => 'barrage_opencolor',
    'title' => '发送弹幕颜色',
    'type' => 'string',
    'content' => 
    array (
    ),
    'value' => '#F00',
    'rule' => 'required',
    'msg' => '',
    'tip' => '自己发送弹幕文字颜色',
    'ok' => '',
    'extend' => '',
  ),
  8 => 
  array (
    'name' => 'barrage_bottom',
    'title' => '弹幕底部高度',
    'type' => 'string',
    'content' => 
    array (
    ),
    'value' => '',
    'rule' => '',
    'msg' => '',
    'tip' => '距离底部高度,单位px,默认随机',
    'ok' => '',
    'extend' => '',
  ),
  9 => 
  array (
    'name' => 'barrage_close',
    'title' => '弹幕关闭按钮',
    'type' => 'select',
    'content' => 
    array (
      0 => '关闭',
      1 => '开启',
    ),
    'value' => '1',
    'rule' => 'required',
    'msg' => '',
    'tip' => '弹幕内容显示关闭按钮',
    'ok' => '',
    'extend' => '',
  ),
  10 => 
  array (
    'name' => 'barrage_oncomm',
    'title' => '获取全部评论',
    'type' => 'select',
    'content' => 
    array (
      0 => '关闭',
      1 => '开启',
    ),
    'value' => '1',
    'rule' => 'required',
    'msg' => '',
    'tip' => '该视频不存在评论获取所有评论',
    'ok' => '',
    'extend' => '',
  ),
);