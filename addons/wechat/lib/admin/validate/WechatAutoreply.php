<?php
// +----------------------------------------------------------------------
// | ZanPianCMS [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.zanpian.com All rights reserved.
// +----------------------------------------------------------------------
// | BBS:  <http://www.feifeicms.cc>
// +----------------------------------------------------------------------
namespace app\common\validate;
class WechatAutoreply extends Base{
 protected $rule = [
        ['auto_title', 'require|unique:wechat_autoreply', '发送标题不能为空|发送标题已存在'],
        ['auto_text', 'require|unique:wechat_autoreply', '回复内容不能为空|回复内容已存在'],
    ];
}