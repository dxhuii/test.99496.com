<?php
namespace app\common\library\wechat;
use app\common\model\Page;
use app\common\model\WechatConfig;
use EasyWeChat\Message\News;
use EasyWeChat\Message\Transfer;
use think\Log;
/**
 * Wechat服务类
 */
class Wechat{	
    public function __construct(){
        
    }
    public function searchmsg($message){
	   $search=F('_data/wechatsearch');
	   if($search){
          $searchs=array();		   
		  foreach($search as $key=>$val){
			if(strpos($message->Content,$val['auto_text'])!==false){	
			 $searchs['msg']=str_replace($val['auto_text'],"",$message->Content);
             $searchs['data'] = $val;
			 break;
			}    
		  }
		  if($searchs){
			return $searchs;    
		  }else{		  
			return false;  
		  } 
	   }
	   return false;
    }	
    // 微信指令查询
    public function msg($message){
		$config=F('_data/wechatconfig_cache');
		$response=array();
	    //存在完全匹配指令
	    $data = db('wechat_autoreply')->alias('a')->join('wechat_response r','r.response_eventkey=a.auto_eventkey','LEFT')->where(['auto_text' => $message->Content, 'auto_status' =>1])->find();
		if($data['response_eventkey'] && $data['response_status']=1){
			 $response = $this->command($message,$data);     
	    }else{
            if(is_numeric($message->Content)){
			    $response=vodpwd($message->Content);			
			}
            if(!$response){			
	            $searchs=$this->searchmsg($message);
	            if($searchs['msg'] && $searchs['data']){
		           $response = $this->command($searchs['msg'],$searchs['data']);
	            }
			}
        }	
        if($response){
			return $response; 
		}else{
			Log::write($message->Content,'notice');
	        return $this->send(errormsg($message->Content));
		}		
    }
    public function command($message,$data){
		   $config=F('_data/wechatconfig_cache');
           if (isset($data['response_type'])){
			   $content=json_decode($data['response_content'],true);
			   switch($data['response_type']){
				    case 'text':
					return $content['content'];
                    break;
                    case 'news':
                        return $this->send(news($content));
				    break;
                    case 'select':
					    return $this->send(select($content));						
				    break;
                    case 'search':
					    return $this->send(search($message,$content));						
				    break;					
                    case 'find':
					    return $this->send(find($content));						
				    break;					
                    default:
                    break;					
               }
	       }
    }
	public function send($data){
		if(is_array($data)){
	        foreach($data as $key=>$val){
		       $news = new News();
		       $news->title = $val['title'];
               $news->url = $val['url'];
               $news->image = zanpian_img_url($val['image']);
               $news->description = $val['description'];
               $response[] = $news;
	        }
			return $response;
		}else{
			return $data;
		}
	   
	  
	}	
}   
require APP_PATH.'common/library/wechat/Wechat_sys.php';