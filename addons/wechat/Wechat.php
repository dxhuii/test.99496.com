<?php
namespace addons\wechat;
use app\common\library\Menu;
use think\Addons;
/**
 * 微信公众号插件
 */
class Wechat extends Addons{

    /**
     * 插件安装方法
     * @return bool
     */
    public function install(){
        $menu = [
            [
                'name'    => 'admin/wechat/index',
                'title'   => '微信管理',
                'icon'    => 'fa fa-wechat',
                'remark'  => '',
				'sort'  => 7,
				'sublist' => [
                    [
                        'name'    => 'admin/wechat.config/index',
                        'title'   => '配置管理',
                        'icon'    => 'fa fa-cog',
						'remark'  => '1.微信公众平台：不同帐号类型权限是不同的，个人订阅号，无法添加，菜单的。2.微信回调地址：http://www.zanpiancms.com/index.php?s=wechat-index-api 只需要修改域名后将地址同时复制到微信公众平台中。3.开启搜索视频：代表微信用户回复的内容没有在回复列表中内容匹配进行视频搜索。否则提示没有相关指令。',
                        'sublist' => [
                            ['name' => 'admin/wechat.config/up', 'title' => '更新配置'],
                        ]
                    ],
                    [
                        'name'    => 'admin/wechat.menu/index',
                        'title'   => '菜单管理',
                        'icon'    => 'fa fa-list',
                        'sublist' => [
                            ['name' => 'admin/wechat.menu/edit', 'title' => '修改菜单'],
                            ['name' => 'admin/wechat.menu/sync', 'title' => '同步菜单'],
                        ]
                    ],
                    [
                        'name'    => 'admin/wechat.autoreply/index',
                        'title'   => '回复管理',
                        'icon'    => 'fa fa-reply-all',
						'remark'  => '回复内容：代表微信用户发送过来的内容',
                        'sublist' => [
                            ['name' => 'admin/wechat.autoreply/add', 'title' => '添加回复'],
                            ['name' => 'admin/wechat.autoreply/edit', 'title' => '编辑回复'],
                            ['name' => 'admin/wechat.autoreply/del', 'title' => '删除回复'],
                            ['name' => 'admin/wechat.autoreply/status', 'title' => '状态设置'],
                        ]
                    ],
                    [
                        'name'    => 'admin/wechat.response/index',
                        'title'   => '资源管理',
                        'icon'    => 'fa fa-list-alt',
                        'sublist' => [
                            ['name' => 'admin/wechat.response/add', 'title' => '添加资源'],
                            ['name' => 'admin/wechat.response/edit', 'title' => '编辑资源'],
                            ['name' => 'admin/wechat.response/del', 'title' => '删除资源'],
                            ['name' => 'admin/wechat.response/status', 'title' => '状态设置'],
                        ]
                    ]
                ]
            ]
        ];
        Menu::create($menu);
        return true;
    }

    /**
     * 插件卸载方法
     * @return bool
     */
    public function uninstall(){
        Menu::delete('admin/wechat/index');
        return true;
    }

}
