--
-- 插入微信相关配置
--
INSERT INTO `__PREFIX__module` (`module_id`, `module_name`, `module_title`, `module_value`, `module_addtime`, `module_uptime`) VALUES
(NULL, 'wechatconfig', '微信配置', '{"app_id":"AppID","app_secret":"AppSecret","token":"Token","aes_key":"AESKey","callback":"http:\\/\\/www.zanpiancms.com\\/index.php?s=wechat-index-api","debug":"0","level":"debug","wechat_url":"www.zanpiancms.com","wechat_defaultvod":"1","wechat_defaultmsg":"\\u611f\\u8c22\\u60a8\\u5173\\u6ce8\\"\\u8d5e\\u7247CMS\\"\\uff01\\r\\n\\u67e5\\u89c6\\u9891\\u5185\\u5bb9\\u8bf7\\u53d1\\u9001\\uff1a@\\u7535\\u5f71|\\u7535\\u89c6\\u5267|\\u52a8\\u6f2b|\\u7efc\\u827a+\\u89c6\\u9891\\u540d\\r\\n\\u67e5\\u660e\\u661f\\u8bf7\\u53d1\\u9001\\uff1a@\\u660e\\u661f+\\u660e\\u661f\\u540d\\r\\n\\u67e5\\u8be2\\u6700\\u65b0\\uff1a\\u6700\\u65b0\\u89c6\\u9891|\\u6700\\u65b0\\u660e\\u661f|\\u6700\\u65b0\\u5267\\u60c5|\\u6700\\u65b0\\u8d44\\u8baf\\r\\n\\u67e5\\u8be2\\u70ed\\u95e8\\uff1a\\u70ed\\u95e8\\u89c6\\u9891|\\u4eba\\u6c14\\u660e\\u661f|\\u70ed\\u95e8\\u8d44\\u8baf\\r\\n\\u968f\\u673a\\u63a8\\u8350\\u8bf7\\u53d1\\uff1a\\u968f\\u673a\\u89c6\\u9891|\\u968f\\u673a\\u660e\\u661f|\\u968f\\u673a\\u8d44\\u8baf\\r\\n\\u5982\\u9700\\u5e2e\\u52a9\\u8bf7\\u53d1\\uff1a\\u5e2e\\u52a9\\r\\n\\u66f4\\u591a\\u5c3d\\u5728<a href=\\"http:\\/\\/www.zanpiancms.com\\">http:\\/\\/www.zanpiancms.com<\\/a>","wechat_errormsg":"\\u62b1\\u6b49\\uff1a\\u6ca1\\u6709\\u627e\\u5230\\u76f8\\u5173\\u5185\\u5bb9\\r\\n\\u641c\\u7d22\\u5c3d\\u91cf\\u4e0d\\u8981\\u8f93\\u5165\\u5168\\u6587\\u5982\\u679c\\u5b9e\\u5728\\u6ca1\\u6709\\r\\n\\u8bf7\\u8bbf\\u95ee\\u4e3b\\u9875\\uff1a<a href=\\"http:\\/\\/www.zanpiancms.com\\">http:\\/\\/www.zanpiancms.com<\\/a>"}', 1502901249, 1505054765),
(NULL, 'wechatmenu', '微信菜单', '[{"name":"网站导航","sub_button":[{"name":"最新剧情","type":"click","key":"599a513a5624b"},{"name":"最新明星","type":"click","key":"599a50ec5d0d3"},{"name":"最新资讯","type":"click","key":"599a50aa745a0"},{"name":"最新视频","type":"click","key":"599a505a8fd7f"}]},{"name":"热门内容","sub_button":[{"name":"热门资讯","type":"click","key":"599a50be894af"},{"name":"人气明星","type":"click","key":"599a510f90798"},{"name":"人气视频","type":"click","key":"599a507ea606e"}]},{"name":"关于程序","sub_button":[{"name":"微信帮助","type":"click","key":"599a500cc3ef7"},{"name":"程序说明","type":"click","key":"599a560966769"},{"name":"官方论坛","type":"view","url":"http:\\/\\/www.feifeicms.cc"},{"name":"程序演示","type":"view","url":"http:\\/\\/www.zanpiancms.com\\/"}]}]', 1497398820, 1505055150);

--
-- 插入微信行为日志
--

INSERT INTO `__PREFIX__action` (`id`, `name`, `title`, `remark`, `rule`, `log`, `type`, `status`, `update_time`) VALUES
(NULL, 'admin/wechat.config/up', '更新微信配置', '更新微信配置', NULL, NULL, 1, 1, 0),
(NULL, 'admin/wechat.autoreply/add', '添加微信回复', '添加微信回复', NULL, NULL, 1, 1, 0),
(NULL, 'admin/wechat.autoreply/edit', '编辑微信回复', '编辑微信回复', NULL, NULL, 1, 1, 0),
(NULL, 'admin/wechat.autoreply/del', '删除微信回复', '删除微信回复', NULL, NULL, 1, 1, 0),
(NULL, 'admin/wechat.autoreply/status', '微信回复状态设置', '微信回复状态设置', NULL, NULL, 1, 1, 0),
(NULL, 'admin/wechat.response/add', '添加微信回复资源', '添加微信回复资源', NULL, NULL, 1, 1, 0),
(NULL, 'admin/wechat.response/edit', '编辑微信回复资源', '编辑微信回复资源', NULL, NULL, 1, 1, 0),
(NULL, 'admin/wechat.response/del', '删除微信回复资源', '删除微信回复资源', NULL, NULL, 1, 1, 0),
(NULL, 'admin/wechat.response/status', '微信回复资源状态设置', '微信回复资源状态设置', NULL, NULL, 1, 1, 0),
(NULL, 'admin/wechat.menu/edit', '编辑微信菜单', '编辑微信菜单', NULL, NULL, 1, 1, 0),
(NULL, 'admin/wechat.menu/sync', '同步微信菜单', '同步微信菜单', NULL, NULL, 1, 1, 0);

CREATE TABLE IF NOT EXISTS `__PREFIX__wechat_autoreply` (
  `auto_id` int(10) NOT NULL AUTO_INCREMENT,
  `auto_title` varchar(100) NOT NULL DEFAULT '' COMMENT '标题',
  `auto_text` varchar(100) NOT NULL DEFAULT '' COMMENT '触发文本',
  `auto_type` enum('text','image','news','voice','video','music','link','app') NOT NULL DEFAULT 'text' COMMENT '回复类型',
  `auto_eventkey` varchar(50) NOT NULL DEFAULT '' COMMENT '响应事件',
  `auto_remark` varchar(255) NOT NULL DEFAULT '' COMMENT '备注',
  `auto_addtime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '添加时间',
  `auto_uptime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `auto_status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态',
  PRIMARY KEY (`auto_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='微信自动回复表' AUTO_INCREMENT=1 ;

INSERT INTO `__PREFIX__wechat_autoreply` (`auto_id`, `auto_title`, `auto_text`, `auto_type`, `auto_eventkey`, `auto_remark`, `auto_addtime`, `auto_uptime`, `auto_status`) VALUES
(1, '帮助', '帮助', 'text', '599a500cc3ef7', '', 1503285806, 1503285806, 1),
(2, '最新视频', '视频', 'text', '599a505a8fd7f', '', 1503285825, 1503285946, 1),
(3, '热门视频', '热门视频', 'text', '599a507ea606e', '', 1503285843, 1503285843, 1),
(4, '随机视频', '随机视频', 'text', '599a509769172', '', 1503285858, 1503285858, 1),
(5, '明星', '明星', 'text', '599a50ec5d0d3', '', 1503285930, 1503285930, 1),
(6, '热门明星', '人气明星', 'text', '599a510f90798', '', 1503285988, 1503285988, 1),
(7, '随机明星', '随机明星', 'text', '599a51235440f', '', 1503286015, 1503286015, 1),
(8, '资讯', '资讯', 'text', '599a50aa745a0', '', 1503286028, 1503286028, 1),
(9, '热门资讯', '热门资讯', 'text', '599a50be894af', '', 1503286057, 1503286057, 1),
(10, '随机资讯', '随机资讯', 'text', '599a50dbec617', '', 1503286083, 1503286083, 1),
(11, '演员表', '演员表', 'text', '599a514a04d69', '', 1503286101, 1503286101, 1),
(12, '角色', '角色', 'text', '599a5159bb715', '', 1503286114, 1503286114, 1),
(13, '搜索全部视频', '@视频', 'text', '599a518216929', '', 1503286179, 1503286201, 1),
(14, '搜索电视剧', '@电视剧', 'text', '599a51aa748db', '', 1503286219, 1503286219, 1),
(15, '搜索电影', '@电影', 'text', '599a51bfbcfff', '', 1503286236, 1503286236, 1),
(16, '搜索动漫', '@动漫', 'text', '599a51e59f099', '', 1503286251, 1503286251, 1),
(17, '搜索综艺', '@综艺', 'text', '599a51d7dcc68', '', 1503286268, 1503286268, 1),
(18, '搜索明星', '@明星', 'text', '599a51fa82509', '', 1503286285, 1503286285, 1),
(19, '搜索资讯', '@资讯', 'text', '599a5206d4d4c', '', 1503286305, 1503286305, 1);


CREATE TABLE IF NOT EXISTS `__PREFIX__wechat_context` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `openid` varchar(64) NOT NULL DEFAULT '',
  `type` varchar(30) NOT NULL DEFAULT '' COMMENT '类型',
  `eventkey` varchar(64) NOT NULL DEFAULT '',
  `command` varchar(64) NOT NULL DEFAULT '',
  `message` varchar(255) NOT NULL DEFAULT '' COMMENT '内容',
  `refreshtime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '最后刷新时间',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `uptime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `openid` (`openid`) USING BTREE
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='微信上下文表' AUTO_INCREMENT=1 ;


INSERT INTO `__PREFIX__wechat_context` (`id`, `openid`, `type`, `eventkey`, `command`, `message`, `refreshtime`, `addtime`, `uptime`) VALUES
(1, 'otHa2v9NhjfpW0_zWOWjBQgD8VaA', '', '599a50be894af', '', '', 1503949818, 0, 0),
(2, 'otHa2v3fRsCWME-9XRbh8TN4woDA', '', '599a507ea606e', '', '', 1503288840, 0, 0),
(3, 'otHa2v60kkX4B18rQSLZoLvcAF2c', '', '599a513a5624b', '', '', 1503288771, 0, 0),
(4, 'otHa2v4D7UDGmEAwtpLZpMxnQj-E', '', '599a507ea606e', '', '', 1503288869, 0, 0),
(5, 'otHa2vw6lCVz1_SsFB6IzF7ne3J8', '', '599a50aa745a0', '', '', 1503629857, 0, 0),
(6, 'otHa2v0hwPyNm4NKKfdn0UAMWzK0', '', '599a505a8fd7f', '', '', 1503289627, 0, 0),
(7, 'otHa2v3PvQs8H7lIZth-f3mcG2Yk', '', '599a507ea606e', '', '', 1503289817, 0, 0),
(8, 'otHa2v3phy7zDg5qo_0o2sdlO-Es', '', '599a505a8fd7f', '', '', 1503290323, 0, 0),
(9, 'otHa2v94j1PTVnkCIPzShUIBmoDg', '', '599a510f90798', '', '', 1503290126, 0, 0),
(10, 'otHa2v1v-gcgG7Vkm94545Bc880k', '', '599a50ec5d0d3', '', '', 1503290313, 0, 0),
(11, 'otHa2v53Ta51SL-3GfK2CcmFQ74A', '', '599a560966769', '', '', 1503294788, 0, 0),
(12, 'otHa2v9VZ1S3phj001kErPL51Dcw', '', '599a50be894af', '', '', 1503302957, 0, 0),
(13, 'otHa2v5p0c7lh3jCPMiUTYPR375o', '', '599a50ec5d0d3', '', '', 1504014062, 0, 0),
(14, 'otHa2v3LAT2IzmbGKKgGXY1f9-cI', '', '599a505a8fd7f', '', '', 1503306446, 0, 0),
(15, 'otHa2v0MGZOKGgtg-yTbrdHUwB_k', '', '599a513a5624b', '', '', 1503307018, 0, 0),
(16, 'otHa2v_buxV9Z9KOqy-tnUAK0hzc', '', '599a500cc3ef7', '', '', 1503328106, 0, 0),
(17, 'otHa2vyiuGcw_tOAYTCX5CGyDKbE', '', '599a560966769', '', '', 1503362391, 0, 0),
(18, 'otHa2v6lClSDvwBvKq-lGFfeirSw', '', '599a505a8fd7f', '', '', 1503563288, 0, 0);

CREATE TABLE IF NOT EXISTS `__PREFIX__wechat_response` (
  `response_id` int(10) NOT NULL AUTO_INCREMENT,
  `response_title` varchar(100) NOT NULL DEFAULT '' COMMENT '资源名',
  `response_eventkey` varchar(128) NOT NULL DEFAULT '' COMMENT '事件',
  `response_type` enum('text','image','news','voice','video','music','link','app','search','relation','select','find') NOT NULL DEFAULT 'text' COMMENT '类型',
  `response_content` text NOT NULL COMMENT '内容',
  `response_remark` varchar(255) NOT NULL DEFAULT '' COMMENT '备注',
  `response_addtime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `response_uptime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `response_status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态',
  PRIMARY KEY (`response_id`),
  UNIQUE KEY `event` (`response_eventkey`) USING BTREE
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='微信资源表' AUTO_INCREMENT=1 ;


INSERT INTO `__PREFIX__wechat_response` (`response_id`, `response_title`, `response_eventkey`, `response_type`, `response_content`, `response_remark`, `response_addtime`, `response_uptime`, `response_status`) VALUES
(1, '帮助文档', '599a500cc3ef7', 'text', '{"content":"微信相关指令：\\r\\n查最新内容输入：视频|明星|资讯|演员表|角色\\r\\n查热门内容输入：热门视频|人气明星|热门资讯\\r\\n随机推荐输入：随机视频|随机明星|随机资讯\\r\\n搜索指定栏目内容请在前面加@\\r\\n视频|电视剧|电影|动漫|综艺|明星|资讯\\r\\n然后跟上关键词。\\r\\n如果帮助请输入：帮助\\r\\n查看更多内容\\r\\n请访问<a href=\\"http:\\/\\/www.zanpiancms.com\\">http:\\/\\/www.zanpiancms.com<\\/a>\\r\\n"}', '', 1503285260, 1503292382, 1),
(2, '最新视频', '599a505a8fd7f', 'select', '{"model":"vod","order":"addtime","type":"desc","limit":"5"}', '', 1503285338, 1503285357, 1),
(3, '热门视频', '599a507ea606e', 'select', '{"model":"vod","order":"hits","type":"desc","limit":"5"}', '', 1503285374, 1503285374, 1),
(4, '随机视频', '599a509769172', 'select', '{"model":"vod","order":"rand","type":"desc","limit":"5"}', '', 1503285399, 1503285399, 1),
(5, '最新资讯', '599a50aa745a0', 'select', '{"model":"news","order":"addtime","type":"desc","limit":"5"}', '', 1503285418, 1503285418, 1),
(6, '热门资讯', '599a50be894af', 'select', '{"model":"news","order":"hits","type":"desc","limit":"5"}', '', 1503285438, 1503285438, 1),
(7, '随机资讯', '599a50dbec617', 'select', '{"model":"news","order":"rand","type":"desc","limit":"5"}', '', 1503285467, 1503285467, 1),
(8, '最新明星', '599a50ec5d0d3', 'select', '{"model":"star","order":"addtime","type":"desc","limit":"5"}', '', 1503285484, 1503285484, 1),
(9, '热门明星', '599a510f90798', 'select', '{"model":"star","order":"hits","type":"desc","limit":"5"}', '', 1503285519, 1503285519, 1),
(10, '随机明星', '599a51235440f', 'select', '{"model":"star","order":"rand","type":"desc","limit":"5"}', '', 1503285539, 1503285539, 1),
(11, '最新剧情', '599a513a5624b', 'select', '{"model":"story","order":"addtime","type":"desc","limit":"5"}', '', 1503285562, 1503285562, 1),
(12, '最新演员表', '599a514a04d69', 'select', '{"model":"actor","order":"addtime","type":"desc","limit":"5"}', '', 1503285578, 1503285578, 1),
(13, '最新角色', '599a5159bb715', 'select', '{"model":"role","order":"addtime","type":"desc","limit":"5"}', '', 1503285593, 1503285593, 1),
(14, '搜索视频', '599a518216929', 'search', '{"model":"vod","cid":"","type":"vague","limit":"5"}', '', 1503285634, 1503285651, 1),
(15, '搜索电视剧', '599a51aa748db', 'search', '{"model":"vod","cid":"2","type":"vague","limit":"5"}', '', 1503285674, 1503285674, 1),
(16, '搜索电影', '599a51bfbcfff', 'search', '{"model":"vod","cid":"1","type":"vague","limit":"5"}', '', 1503285695, 1503285695, 1),
(17, '搜索综艺', '599a51d7dcc68', 'search', '{"model":"vod","cid":"4","type":"vague","limit":"5"}', '', 1503285719, 1503285719, 1),
(18, '搜索动漫', '599a51e59f099', 'search', '{"model":"vod","cid":"3","type":"vague","limit":"5"}', '', 1503285733, 1503285733, 1),
(19, '搜索明星', '599a51fa82509', 'search', '{"model":"star","cid":"","type":"","limit":"5"}', '', 1503285754, 1503285754, 1),
(20, '搜索新闻', '599a5206d4d4c', 'search', '{"model":"news","cid":"","type":"","limit":"5"}', '', 1503285766, 1503285766, 1),
(21, '程序说明', '599a560966769', 'news', '{"title":"赞片CMSV7.0商业版","image":"http:\\/\\/yanzheng.97bike.com\\/weixincms.jpg","description":"赞片商业版程序本着一切为草根站长的宗旨经过4年的的改进、开发更新，2017我们迎来了全新的面貌，全新构架的赞片程序7.0版本，强大的程序性价比，超强的功能使程序的用户团体已超千人，是目前影视导航cms首选程序。目前赞片程序集成了视频、文章、明星、剧情、演员表、角色、电视节目、专题等丰富功能模块构架的内容关联是影视导航网站首选程序，程序采用最新更安全稳定的ThinkPHP5.0.7框架开发，为API开发而设计的高性能框架。","url":"http:\\/\\/www.zanpiancms.com\\/"}', '', 1503286793, 1503286793, 1);