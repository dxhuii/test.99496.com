<?php
return array(
    array(
        'name'    => 'loco_pwd',
        'title'   => '接口密码',
        'type'    => 'string',
        'content' =>
        array(
        ),
        'value'   => 'eA6$V7Gy',
        'rule'    => 'required',
        'msg'     => '',
        'tip'     => '火车头接口使用的密码',
        'ok'      => '',
        'extend'  => '',
    ),
);
