<?php
namespace addons\hot;
use app\common\library\Menu;
use think\Addons;
/**
 * 数据库插件
 */
class Hot extends Addons{
    /**
     * 插件安装方法
     * @return bool
     */
    public function install(){
        $menu = [
            [
                'name'    => 'admin/hot/index',
                'title'   => '百度指数采集',
                'remark'  => '',
				'sort'  => 5,
				'type' => 2
            ]
        ];
        Menu::create($menu, 'admin/collect/info');
        return true;
    }
    /**
     * 插件卸载方法
     * @return bool
     */
    public function uninstall(){
        Menu::delete('admin/hot/index');
        return true;
    }

}
