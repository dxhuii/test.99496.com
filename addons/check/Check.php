<?php
namespace addons\check;
use app\common\library\Menu;
use think\Addons;
/**
 * 数据库插件
 */
class Check extends Addons{

    /**
     * 插件安装方法
     * @return bool
     */
    public function install(){
        $menu = [
            [
                'name'    => 'admin/check/index',
                'title'   => '重名检查',
                'icon'    => 'fa fa-telegram',
                'remark'  => '',
				'sort'  => 11,
				'sublist' => [
                    ['name' => 'admin/vodcheck/index', 'title' => '视频检查','type'=>2],
                    ['name' => 'admin/starcheck/index', 'title' => '明星检查','type'=>2],					
                ]
            ]
        ];
        Menu::create($menu, 'admin/vod/show');
        return true;
    }

    /**
     * 插件卸载方法
     * @return bool
     */
    public function uninstall(){
        Menu::delete('admin/check/index');
        return true;
    }

}
