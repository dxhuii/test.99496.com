<?php
return array (
  'config_group_list' => 
  array (
    1 => '网站设置',
    2 => '系统设置',
    3 => '伪静态',
    4 => '缓存设置',
    5 => '附件设置',
    6 => '视频设置',
    7 => '采集设置',
    99 => '高级设置',
  ),
  'auth_config' => 
  array (
    'AUTH_ON' => '1',
    'AUTH_TYPE' => '2',
  ),
  'data_backup_path' => 'databack',
  'data_backup_part_size' => '20971520',
  'data_backup_compress' => '1',
  'data_backup_compress_level' => '9',
  'allow_visit' => 
  array (
    0 => 'index/index',
  ),
  'deny_visit' => 
  array (
    0 => '',
  ),
  'admin_allow_ip' => '',
  'site_name' => '99496动漫(9站)',
  'site_url' => 'https://www.99496.com',
  'data_cache_port' => '6379',
  'site_description' => '99496动漫(9站)。',
  'contrast_play' => '0',
  'empty_playurl' => '1',
  'site_keyword' => '免费动漫,99496,9站',
  'site_status' => true,
  'site_icp' => '沪ICP备16032125号-3',
  'data_cache_host' => '127.0.0.1',
  'home_pagenum' => '2',
  'admin_order_type' => 'addtime',
  'config_type_list' => 
  array (
    'text' => '单行文本,varchar',
    'char' => '文本char,char',
    'string' => '字符串,int',
    'password' => '密码,varchar',
    'textarea' => '数组文本框,text',
    'notextarea' => '非数组文本框,text',
    'longtext' => '文本框longtext,longtext',
    'bool' => '布尔型,int',
    'select' => '选择,varchar',
    'num' => '数字int,int',
    'smallint' => '数字smallint,smallint',
    'tinyint' => '数字tinyint,smallint',
    'mediumint' => '数字mediumint,mediumint',
    'decimal' => '金额,decimal',
    'tags' => '标签,varchar',
    'datetime' => '时间控件,int',
    'date' => '日期控件,varchar',
    'editor' => '编辑器,text',
    'bind' => '模型绑定,int',
    'pctheme' => '获取网站模版,varchar',
    'mobiletheme' => '获取手机模版,varchar',
    'usertheme' => '获取会员模版,varchar',
    'html' => 'HTML样式,html',
  ),
  'document_position' => 
  array (
    1 => '首页推荐',
    3 => '封面推荐',
    2 => '列表推荐',
    4 => '季番推荐',
  ),
  'site_path' => '/',
  'site_title' => '99496动漫(9站)。',
  'site_copyright' => '© 99496动漫[99496.COM] 所有内容均收集引用于互联网公开的资源，本站只提供引用，不参与视频制作上传，不提供视频储存下载。<br>
若本站的引用侵犯了您的利益，请联系我们核查所实后将在第一时间删除。欢迎对本站引用内容进行监督，共创绿色健康互联网。</br> 联系邮箱：916091535@qq.com 或联系QQ916091535 © 2019 <a href="/" style="font-family:Verdana, Arial, Helvetica, sans-serif;font-weight:bold;color:#f60;">99496<span style="color:#c00;">.Com</span></a>. <a href="http://www.miibeian.gov.cn/" target="_blank">沪ICP备16032125号-1</a> 沪网文（2017）1295-032号 <a target="_blank" href="http://www.beian.gov.cn/portal/registerSystemInfo?recordcode=34130202000190" style="position: relative;top: 5px;display: inline-block;margin-left:5px;font-size:12px;"><img src="http://ww3.sinaimg.cn/large/628d024fgw1f63puhjy82j200k00k3y9.jpg" style="float:left;"><span style="float:left;height:20px;line-height:20px;margin: 0px 0px 0px 5px; color:#939393;">皖公网安备 34130202000190 号</span></a>',
  'site_hotkeywords' => '',
  'site_tongji' => '',
  'site_email' => '916091535@qq.com',
  'default_theme' => '#!xichen',
  'mobile_url' => 'https://m.99496.com',
  'user_theme' => 'new',
  'site_cssjsurl' => '//cos.mdb6.com/static/',
  'admin_time_edit' => '1',
  'admin_ads_file' => 'runtime/ad',
  'admin_list_pages' => '30',
  'news_images' => '4',
  'admin_letters' => '2',
  'keywords_openadd' => '0',
  'keywords_opencai' => '0',
  'upload_path' => 'uploads',
  'upload_style' => 'Y-m-d',
  'pic_pinyin' => '0',
  'upload_class' => 'jpg,gif,png,jpeg,bmp',
  'upload_http_down' => '20',
  'upload_http' => '0',
  'upload_http_news' => '0',
  'upload_thumb' => '0',
  'upload_thumb_size' => '220/307',
  'upload_thumb_pos' => '1',
  'upload_water' => '0',
  'upload_water_pct' => '75',
  'upload_water_pos' => '1',
  'upload_water_img' => 'water.gif',
  'upload_ftp' => '0',
  'upload_ftp_del' => '1',
  'upload_ftp_host' => '183.131.79.155',
  'upload_ftp_user' => 'zanpianpicftp',
  'upload_ftp_pass' => 'Fy8651621',
  'upload_ftp_port' => '56743',
  'upload_ftp_dir' => '/',
  'upload_http_prefix' => 'https://u.mdb6.com/uploads/',
  'player_http' => '0',
  'player_width' => '100%',
  'player_height' => '442px',
  'player_second' => '0',
  'player_playad' => 'http://cdn.97bike.com/loading.html',
  'play_collect' => '0',
  'rand_tag' => '0',
  'player_collect_name' => '0',
  'player_collect_time' => '5',
  'rand_hits' => '9999',
  'rand_updown' => '9',
  'rand_gold' => '10',
  'rand_golder' => '9',
  'replace_play' => 
  array (
    'ykyun' => 'youyun',
    'youku' => 'yuku',
    'iqiyi' => 'qiyi',
  ),
  'caiji_pic' => '0',
  'caiji_play' => '1',
  'caiji_datatime' => '1',
  'caiji_addtime' => '1',
  'api_cai_limit' => '5',
  'caiji_vodtime' => '1',
  'mobile_theme' => '#!v.1.1',
  'user_status' => '1',
  'api_pic' => 'http://api.97bike.com/pic.php?url=',
  'http_api' => 
  array (
    1 => 'v.97bike.com',
    2 => 'v1.97bike.com',
    3 => 'v2.97bike.com',
    4 => 'v3.97bike.com',
    5 => 'v4.97bike.com',
  ),
  'play_area' => '大陆,日本,香港,台湾,美国,韩国,泰国,新加坡,马来西亚,印度,英国,法国,加拿大,西班牙,俄罗斯,其它',
  'play_year' => '2020,2019,2018,2017,2016,2015,2014,2013,2012,2011,2010,2009,2008,2007,2006,2005,2004,2003,2002,2001,2000,1990,1980,1970,1960,1950,1940',
  'play_language' => '国语,日语,英语,粤语,闽南语,韩语,国语/粤语,其它',
  'url_html_suffix' => 'html',
  'story_cidarray' => 
  array (
    0 => '',
  ),
  'actor_cidarray' => 
  array (
    0 => 'all',
  ),
  'role_cidarray' => 
  array (
    0 => 'all',
  ),
  'news_pic' => '0',
  'auto_tag' => '0',
  'week_list' => 
  array (
    1 => '星期一',
    2 => '星期二',
    3 => '星期三',
    4 => '星期四',
    5 => '星期五',
    6 => '星期六',
    7 => '星期七',
  ),
  'copyright_txt' => '该视频由于版权限制，暂不提供播放。',
  'force_collect' => '0',
  'tpl_id' => 
  array (
    1 => '1',
    2 => '2',
    3 => '3',
    4 => '4',
    5 => '5',
    6 => '6',
    7 => '7',
    8 => '8',
    9 => '9',
    10 => '10',
    11 => '11',
    12 => '12',
    13 => '13',
    14 => '14',
    15 => '15',
    16 => '16',
    17 => '57',
    18 => '60',
    19 => '58',
    20 => '56',
    21 => '59',
  ),
  'data_cache_vod' => '86400',
  'data_cache_story' => '86400',
  'data_cache_actor' => '86400',
  'data_cache_role' => '86400',
  'data_cache_tv' => '86400',
  'data_cache_star' => '86400',
  'data_cache_news' => '86400',
  'data_cache_foreach' => '600',
  'data_cache_type' => 'file',
  'show_error_msg' => false,
  'tpl_cache' => false,
  'strip_space' => true,
  'config_app_debug' => '0',
  'config_app_trace' => '0',
  'log_type' => 'test',
  'url_rewrite' => '1',
  'user_rewrite' => '1',
  'user_rewrite_route' => 
  array (
    'user-reg-index' => 'user/reg',
    'user-reg-index-api-1' => 'user/regsns',
    'user-reg-agreement' => 'user/agreement',
    'user-login-index' => 'user/login',
    'user-snslogin-qq' => 'user/qq',
    'user-snslogin-weibo' => 'user/weibo',
    'user-login-forgetpwd' => 'user/forgetpwd',
    'user-login-repass-code-(#char)' => 'user/repass/($code)',
    'user-center-index' => 'member/',
    'user-center-info' => 'member/info',
    'user-center-email' => 'member/email',
    'user-center-avatar' => 'member/avatar',
    'user-center-syncs' => 'member/syncs',
    'user-center-pwd' => 'member/pwd',
    'user-center-remind' => 'member/remind',
    'user-center-love' => 'member/love',
    'user-center-cm' => 'member/cm',
    'user-center-gb' => 'member/gb',
    'user-center-playlog' => 'member/playlog',
    'user-center-msg' => 'member/msg',
    'user-center-order' => 'member/order',
    'user-center-score' => 'member/score',
    'home-search-index' => 'search/',
    'home-tv-read-id-(#num)-pinyin-(#char)-cid-(#num)-dir-(#char)' => 'tv/($id)',
    'home-tv-read-id-(#num)-pinyin-(#char)-cid-(#num)-dir-(#char)-p-(#num)' => 'tv/($id)-($p)',
    'home-tag-show-id-(#num)-dir-(#char)-tag-(#vhar)' => 'tag/($id)-$tag',
    'home-tag-show-id-(#num)-dir-(#char)-tag-(#vhar)-p-(#num)' => 'tag/($id)-$tag-($p)',
    'home-map-show-id-(#num)-dir-(#char)' => 'map/($dir)',
    'home-gb-show' => 'gb/',
    'home-gb-show-id-(#num)' => 'gb/($id)',
    'home-gb-show-id-(#num)-p-(#num)' => 'gb/($id)-($p)',
    'home-search-index-wd-(#vhar)-p-(#num)' => 'search/($wd)-($p)',
    'home-search-index-wd-(#vhar)' => 'search/($wd)',
    'user-home-index-id-(#num)' => 'user/($id)/',
    'home-vod-read-id-(#num)-pinyin-(#char)-cid-(#num)-dir-(#char)' => 'ac/($id)/',
    'home-vod-filmtime-id-(#num)-pinyin-(#char)-cid-(#num)-dir-(#char)' => 'time/($id)/',
    'home-vod-news-id-(#num)-pinyin-(#char)-cid-(#num)-dir-(#char)-p-(#num)' => 'ac/($id)/news/($p)/',
    'home-vod-news-id-(#num)-pinyin-(#char)-cid-(#num)-dir-(#char)' => 'ac/($id)/news/',
    'home-vod-play-id-(#num)-pinyin-(#char)-cid-(#num)-dir-(#char)-sid-(#char)-pid-(#num)' => 'ac/($id)/($sid)-($pid)',
    'home-news-read-id-(#num)-cid-(#num)-dir-(#char)-p-(#num)' => 'article/($id)-($p)',
    'home-news-read-id-(#num)-cid-(#num)-dir-(#char)' => 'article/($id)',
    'home-story-read-id-(#num)-pinyin-(#char)-cid-(#num)-dir-(#char)' => 'plot/($id)/',
    'home-story-read-id-(#num)-pinyin-(#char)-cid-(#num)-dir-(#char)-p-(#num)' => 'plot/($id)/($p)/',
    'home-actor-read-id-(#num)-pinyin-(#char)-cid-(#num)-dir-(#char)' => 'actor/($id)/',
    'home-role-read-id-(#num)-pinyin-(#char)-cid-(#num)-dir-(#char)' => 'role/($id)/',
    'home-star-read-id-(#num)-pinyin-(#char)-cid-(#num)-dir-(#char)' => 'star/($id)/',
    'home-star-work-id-(#num)-pinyin-(#char)-cid-(#num)-dir-(#char)-p-(#num)' => 'star/($id)/work/($p)',
    'home-star-work-id-(#num)-pinyin-(#char)-cid-(#num)-dir-(#char)' => 'star/($id)/work/',
    'home-star-hz-id-(#num)-pinyin-(#char)-cid-(#num)-dir-(#char)-p-(#num)' => 'star/($id)/partner/($p)/',
    'home-star-hz-id-(#num)-pinyin-(#char)-cid-(#num)-dir-(#char)' => 'star/($id)/partner/',
    'home-star-news-id-(#num)-pinyin-(#char)-cid-(#num)-dir-(#char)-p-(#num)' => 'star/($id)/news/($p)/',
    'home-star-news-id-(#num)-pinyin-(#char)-cid-(#num)-dir-(#char)' => 'star/($id)/news/',
    'home-star-role-id-(#num)-pinyin-(#char)-cid-(#num)-dir-(#char)-p-(#num)' => 'star/($id)/role/($p)/',
    'home-star-role-id-(#num)-pinyin-(#char)-cid-(#num)-dir-(#char)' => 'star/($id)/role/',
    'home-vod-type-id-(#num)-dir-(#char)-mcid-(#vhar)-area-(#vhar)-year-(#vhar)-letter-(#vhar)-order-(#vhar)-p-(#vhar)' => '($dir)/index_($mcid)_($area)_($year)_($letter)_($order)_($p)',
    'home-vod-type-id-(#num)-dir-(#char)-mcid-(#vhar)-area-(#vhar)-year-(#vhar)-letter-(#vhar)-order-(#vhar)' => '($dir)/index_($mcid)_($area)_($year)_($letter)_($order)',
    'home-star-type-id-(#num)-dir-(#char)-sex-(#vhar)-zy-(#vhar)-area-(#vhar)-letter-(#vhar)-order-(#vhar)-p-(#vhar)' => '($dir)/($sex)_($zy)_($area)_($letter)_($order)_($p)',
    'home-star-type-id-(#num)-dir-(#char)-sex-(#vhar)-zy-(#vhar)-area-(#vhar)-letter-(#vhar)-order-(#vhar)' => '($dir)/($sex)_($zy)_($area)_($letter)_($order)',
    'home-vod-show-id-(#num)-dir-(#char)-p-(#num)' => '($dir)/index($p)',
    'home-vod-show-id-(#num)-dir-(#char)' => '($dir)/',
    'home-news-show-id-(#num)-dir-(#char)-p-(#num)' => '($dir)/index($p)',
    'home-news-show-id-(#num)-dir-(#char)' => '($dir)/',
    'home-story-show-id-(#num)-dir-(#char)-p-(#num)' => '($dir)/index($p)',
    'home-story-show-id-(#num)-dir-(#char)' => '($dir)/',
    'home-actor-show-id-(#num)-dir-(#char)-p-(#num)' => '($dir)/index($p)',
    'home-actor-show-id-(#num)-dir-(#char)' => '($dir)/',
    'home-role-show-id-(#num)-dir-(#char)-p-(#num)' => '($dir)/index($p)',
    'home-role-show-id-(#num)-dir-(#char)' => '($dir)/',
    'home-star-show-id-(#num)-dir-(#char)-p-(#num)' => '($dir)/index($p)',
    'home-star-show-id-(#num)-dir-(#char)' => '($dir)/',
    'home-special-show-id-(#num)-dir-(#char)-p-(#num)' => '($dir)/index($p)',
    'home-special-show-id-(#num)-dir-(#char)' => '($dir)/',
    'home-special-read-id-(#num)-pinyin-(#char)-cid-(#num)-dir-(#char)' => '($dir)/($pinyin)/',
    'home-tv-show-id-(#num)-dir-(#char)-p-(#num)' => '($dir)/index($p)',
    'home-tv-show-id-(#num)-dir-(#char)' => '($dir)/',
    'home-up-show-id-(#num)-dir-(#char)-month-(#num)' => '($dir)/($month)/',
    'home-up-show-id-(#num)-dir-(#char)-month-(#num)-p-(#num)' => '($dir)/($month)/($p)/',
    'home-up-show-id-(#num)-dir-(#char)-type-(#char)' => '($dir)/($id)/($type)/',
    'home-up-show-id-(#num)-dir-(#char)-type-(#char)-p-(#num)' => '($dir)/($id)/($type)/($p)/',
    'home-my-show-id-(#num)-dir-(#char)' => '($dir)',
  ),
  'player_cloud' => 'http://cdn.97bike.com/player/',
  'player_api' => 'https://www.ikanfan.cn/api/play.php?type=',
  'html_cache_on' => '0',
  'html_cache_index' => '3600',
  'html_cache_vod_list' => '3600',
  'html_cache_vod_type' => '0',
  'html_cache_vod_read' => '0',
  'html_cache_vod_play' => '0',
  'html_cache_vod_other' => '0',
  'html_cache_news_list' => '0',
  'html_cache_news_read' => '0',
  'html_cache_star_list' => '0',
  'html_cache_star_type' => '0',
  'html_cache_star_read' => '0',
  'html_cache_star_other' => '0',
  'html_cache_story_list' => '0',
  'html_cache_story_read' => '0',
  'html_cache_actor_list' => '0',
  'html_cache_actor_read' => '0',
  'html_cache_role_list' => '0',
  'html_cache_role_read' => '0',
  'html_cache_tv_list' => '0',
  'html_cache_tv_read' => '0',
  'html_cache_special_list' => '0',
  'html_cache_special_read' => '0',
  'html_cache_my_list' => '3600',
  'zanpiancms_login' => '=wHM%1T1HX!zxo',
  'data_cache_password' => '',
  'data_cache_prefix' => 'xichen_',
  'upload_graph' => '0',
  'upload_graph_http' => '0',
  'upload_graph_domain' => 
  array (
    0 => 'sinaimg.cn',
    1 => 'loli.net',
    2 => 'mdb6.com',
  ),
  'upload_graph_type' => 'sina',
  'upload_graph_control' => '0',
  'upload_sina_cookie' => '',
  'app_debug' => false,
  'app_trace' => false,
  'route_rules' => 
  array (
    'user/reg/index' => 
    array (
      'find' => 1,
      'replace' => 'user/regsns',
    ),
    'user/reg/agreement' => 
    array (
      'find' => 1,
      'replace' => 'user/agreement',
    ),
    'user/login/index' => 
    array (
      'find' => 1,
      'replace' => 'user/login',
    ),
    'user/snslogin/qq' => 
    array (
      'find' => 1,
      'replace' => 'user/qq',
    ),
    'user/snslogin/weibo' => 
    array (
      'find' => 1,
      'replace' => 'user/weibo',
    ),
    'user/login/forgetpwd' => 
    array (
      'find' => 1,
      'replace' => 'user/forgetpwd',
    ),
    'user/login/repass/code' => 
    array (
      'find' => 
      array (
        0 => '$code',
      ),
      'replace' => 'user/repass/$code',
    ),
    'user/center/index' => 
    array (
      'find' => 1,
      'replace' => 'member/',
    ),
    'user/center/info' => 
    array (
      'find' => 1,
      'replace' => 'member/info',
    ),
    'user/center/email' => 
    array (
      'find' => 1,
      'replace' => 'member/email',
    ),
    'user/center/avatar' => 
    array (
      'find' => 1,
      'replace' => 'member/avatar',
    ),
    'user/center/syncs' => 
    array (
      'find' => 1,
      'replace' => 'member/syncs',
    ),
    'user/center/pwd' => 
    array (
      'find' => 1,
      'replace' => 'member/pwd',
    ),
    'user/center/remind' => 
    array (
      'find' => 1,
      'replace' => 'member/remind',
    ),
    'user/center/love' => 
    array (
      'find' => 1,
      'replace' => 'member/love',
    ),
    'user/center/cm' => 
    array (
      'find' => 1,
      'replace' => 'member/cm',
    ),
    'user/center/gb' => 
    array (
      'find' => 1,
      'replace' => 'member/gb',
    ),
    'user/center/playlog' => 
    array (
      'find' => 1,
      'replace' => 'member/playlog',
    ),
    'user/center/msg' => 
    array (
      'find' => 1,
      'replace' => 'member/msg',
    ),
    'user/center/order' => 
    array (
      'find' => 1,
      'replace' => 'member/order',
    ),
    'user/center/score' => 
    array (
      'find' => 1,
      'replace' => 'member/score',
    ),
    'home/search/index' => 
    array (
      'find' => 1,
      'replace' => 'search/',
    ),
    'home/tv/read/id/pinyin/cid/dir' => 
    array (
      'find' => 
      array (
        0 => '$id',
        1 => '$pinyin',
        2 => '$cid',
        3 => '$dir',
      ),
      'replace' => 'tv/$id',
    ),
    'home/tv/read/id/pinyin/cid/dir/p' => 
    array (
      'find' => 
      array (
        0 => '$id',
        1 => '$pinyin',
        2 => '$cid',
        3 => '$dir',
        4 => '$p',
      ),
      'replace' => 'tv/$id-$p',
    ),
    'home/tag/show/id/dir/tag' => 
    array (
      'find' => 
      array (
        0 => '$id',
        1 => '$dir',
        2 => '$tag',
      ),
      'replace' => 'tag/$id-$tag',
    ),
    'home/tag/show/id/dir/tag/p' => 
    array (
      'find' => 
      array (
        0 => '$id',
        1 => '$dir',
        2 => '$tag',
        3 => '$p',
      ),
      'replace' => 'tag/$id-$tag-$p',
    ),
    'home/map/show/id/dir' => 
    array (
      'find' => 
      array (
        0 => '$id',
        1 => '$dir',
      ),
      'replace' => 'map/$dir',
    ),
    'home/gb/show' => 
    array (
      'find' => 1,
      'replace' => 'gb/',
    ),
    'home/gb/show/id' => 
    array (
      'find' => 
      array (
        0 => '$id',
      ),
      'replace' => 'gb/$id',
    ),
    'home/gb/show/id/p' => 
    array (
      'find' => 
      array (
        0 => '$id',
        1 => '$p',
      ),
      'replace' => 'gb/$id-$p',
    ),
    'home/search/index/wd/p' => 
    array (
      'find' => 
      array (
        0 => '$wd',
        1 => '$p',
      ),
      'replace' => 'search/$wd-$p',
    ),
    'home/search/index/wd' => 
    array (
      'find' => 
      array (
        0 => '$wd',
      ),
      'replace' => 'search/$wd',
    ),
    'user/home/index/id' => 
    array (
      'find' => 
      array (
        0 => '$id',
      ),
      'replace' => 'user/$id/',
    ),
    'home/vod/read/id/pinyin/cid/dir' => 
    array (
      'find' => 
      array (
        0 => '$id',
        1 => '$pinyin',
        2 => '$cid',
        3 => '$dir',
      ),
      'replace' => 'ac/$id/',
    ),
    'home/vod/filmtime/id/pinyin/cid/dir' => 
    array (
      'find' => 
      array (
        0 => '$id',
        1 => '$pinyin',
        2 => '$cid',
        3 => '$dir',
      ),
      'replace' => 'time/$id/',
    ),
    'home/vod/news/id/pinyin/cid/dir/p' => 
    array (
      'find' => 
      array (
        0 => '$id',
        1 => '$pinyin',
        2 => '$cid',
        3 => '$dir',
        4 => '$p',
      ),
      'replace' => 'ac/$id/news/$p/',
    ),
    'home/vod/news/id/pinyin/cid/dir' => 
    array (
      'find' => 
      array (
        0 => '$id',
        1 => '$pinyin',
        2 => '$cid',
        3 => '$dir',
      ),
      'replace' => 'ac/$id/news/',
    ),
    'home/vod/play/id/pinyin/cid/dir/sid/pid' => 
    array (
      'find' => 
      array (
        0 => '$id',
        1 => '$pinyin',
        2 => '$cid',
        3 => '$dir',
        4 => '$sid',
        5 => '$pid',
      ),
      'replace' => 'ac/$id/$sid-$pid',
    ),
    'home/news/read/id/cid/dir/p' => 
    array (
      'find' => 
      array (
        0 => '$id',
        1 => '$cid',
        2 => '$dir',
        3 => '$p',
      ),
      'replace' => 'article/$id-$p',
    ),
    'home/news/read/id/cid/dir' => 
    array (
      'find' => 
      array (
        0 => '$id',
        1 => '$cid',
        2 => '$dir',
      ),
      'replace' => 'article/$id',
    ),
    'home/story/read/id/pinyin/cid/dir' => 
    array (
      'find' => 
      array (
        0 => '$id',
        1 => '$pinyin',
        2 => '$cid',
        3 => '$dir',
      ),
      'replace' => 'plot/$id/',
    ),
    'home/story/read/id/pinyin/cid/dir/p' => 
    array (
      'find' => 
      array (
        0 => '$id',
        1 => '$pinyin',
        2 => '$cid',
        3 => '$dir',
        4 => '$p',
      ),
      'replace' => 'plot/$id/$p/',
    ),
    'home/actor/read/id/pinyin/cid/dir' => 
    array (
      'find' => 
      array (
        0 => '$id',
        1 => '$pinyin',
        2 => '$cid',
        3 => '$dir',
      ),
      'replace' => 'actor/$id/',
    ),
    'home/role/read/id/pinyin/cid/dir' => 
    array (
      'find' => 
      array (
        0 => '$id',
        1 => '$pinyin',
        2 => '$cid',
        3 => '$dir',
      ),
      'replace' => 'role/$id/',
    ),
    'home/star/read/id/pinyin/cid/dir' => 
    array (
      'find' => 
      array (
        0 => '$id',
        1 => '$pinyin',
        2 => '$cid',
        3 => '$dir',
      ),
      'replace' => 'star/$id/',
    ),
    'home/star/work/id/pinyin/cid/dir/p' => 
    array (
      'find' => 
      array (
        0 => '$id',
        1 => '$pinyin',
        2 => '$cid',
        3 => '$dir',
        4 => '$p',
      ),
      'replace' => 'star/$id/work/$p',
    ),
    'home/star/work/id/pinyin/cid/dir' => 
    array (
      'find' => 
      array (
        0 => '$id',
        1 => '$pinyin',
        2 => '$cid',
        3 => '$dir',
      ),
      'replace' => 'star/$id/work/',
    ),
    'home/star/hz/id/pinyin/cid/dir/p' => 
    array (
      'find' => 
      array (
        0 => '$id',
        1 => '$pinyin',
        2 => '$cid',
        3 => '$dir',
        4 => '$p',
      ),
      'replace' => 'star/$id/partner/$p/',
    ),
    'home/star/hz/id/pinyin/cid/dir' => 
    array (
      'find' => 
      array (
        0 => '$id',
        1 => '$pinyin',
        2 => '$cid',
        3 => '$dir',
      ),
      'replace' => 'star/$id/partner/',
    ),
    'home/star/news/id/pinyin/cid/dir/p' => 
    array (
      'find' => 
      array (
        0 => '$id',
        1 => '$pinyin',
        2 => '$cid',
        3 => '$dir',
        4 => '$p',
      ),
      'replace' => 'star/$id/news/$p/',
    ),
    'home/star/news/id/pinyin/cid/dir' => 
    array (
      'find' => 
      array (
        0 => '$id',
        1 => '$pinyin',
        2 => '$cid',
        3 => '$dir',
      ),
      'replace' => 'star/$id/news/',
    ),
    'home/star/role/id/pinyin/cid/dir/p' => 
    array (
      'find' => 
      array (
        0 => '$id',
        1 => '$pinyin',
        2 => '$cid',
        3 => '$dir',
        4 => '$p',
      ),
      'replace' => 'star/$id/role/$p/',
    ),
    'home/star/role/id/pinyin/cid/dir' => 
    array (
      'find' => 
      array (
        0 => '$id',
        1 => '$pinyin',
        2 => '$cid',
        3 => '$dir',
      ),
      'replace' => 'star/$id/role/',
    ),
    'home/vod/type/id/dir/mcid/area/year/letter/order/p' => 
    array (
      'find' => 
      array (
        0 => '$id',
        1 => '$dir',
        2 => '$mcid',
        3 => '$area',
        4 => '$year',
        5 => '$letter',
        6 => '$order',
        7 => '$p',
      ),
      'replace' => '$dir/index_$mcid_$area_$year_$letter_$order_$p',
    ),
    'home/vod/type/id/dir/mcid/area/year/letter/order' => 
    array (
      'find' => 
      array (
        0 => '$id',
        1 => '$dir',
        2 => '$mcid',
        3 => '$area',
        4 => '$year',
        5 => '$letter',
        6 => '$order',
      ),
      'replace' => '$dir/index_$mcid_$area_$year_$letter_$order',
    ),
    'home/star/type/id/dir/sex/zy/area/letter/order/p' => 
    array (
      'find' => 
      array (
        0 => '$id',
        1 => '$dir',
        2 => '$sex',
        3 => '$zy',
        4 => '$area',
        5 => '$letter',
        6 => '$order',
        7 => '$p',
      ),
      'replace' => '$dir/$sex_$zy_$area_$letter_$order_$p',
    ),
    'home/star/type/id/dir/sex/zy/area/letter/order' => 
    array (
      'find' => 
      array (
        0 => '$id',
        1 => '$dir',
        2 => '$sex',
        3 => '$zy',
        4 => '$area',
        5 => '$letter',
        6 => '$order',
      ),
      'replace' => '$dir/$sex_$zy_$area_$letter_$order',
    ),
    'home/vod/show/id/dir/p' => 
    array (
      'find' => 
      array (
        0 => '$id',
        1 => '$dir',
        2 => '$p',
      ),
      'replace' => '$dir/index$p',
    ),
    'home/vod/show/id/dir' => 
    array (
      'find' => 
      array (
        0 => '$id',
        1 => '$dir',
      ),
      'replace' => '$dir/',
    ),
    'home/news/show/id/dir/p' => 
    array (
      'find' => 
      array (
        0 => '$id',
        1 => '$dir',
        2 => '$p',
      ),
      'replace' => '$dir/index$p',
    ),
    'home/news/show/id/dir' => 
    array (
      'find' => 
      array (
        0 => '$id',
        1 => '$dir',
      ),
      'replace' => '$dir/',
    ),
    'home/story/show/id/dir/p' => 
    array (
      'find' => 
      array (
        0 => '$id',
        1 => '$dir',
        2 => '$p',
      ),
      'replace' => '$dir/index$p',
    ),
    'home/story/show/id/dir' => 
    array (
      'find' => 
      array (
        0 => '$id',
        1 => '$dir',
      ),
      'replace' => '$dir/',
    ),
    'home/actor/show/id/dir/p' => 
    array (
      'find' => 
      array (
        0 => '$id',
        1 => '$dir',
        2 => '$p',
      ),
      'replace' => '$dir/index$p',
    ),
    'home/actor/show/id/dir' => 
    array (
      'find' => 
      array (
        0 => '$id',
        1 => '$dir',
      ),
      'replace' => '$dir/',
    ),
    'home/role/show/id/dir/p' => 
    array (
      'find' => 
      array (
        0 => '$id',
        1 => '$dir',
        2 => '$p',
      ),
      'replace' => '$dir/index$p',
    ),
    'home/role/show/id/dir' => 
    array (
      'find' => 
      array (
        0 => '$id',
        1 => '$dir',
      ),
      'replace' => '$dir/',
    ),
    'home/star/show/id/dir/p' => 
    array (
      'find' => 
      array (
        0 => '$id',
        1 => '$dir',
        2 => '$p',
      ),
      'replace' => '$dir/index$p',
    ),
    'home/star/show/id/dir' => 
    array (
      'find' => 
      array (
        0 => '$id',
        1 => '$dir',
      ),
      'replace' => '$dir/',
    ),
    'home/special/show/id/dir/p' => 
    array (
      'find' => 
      array (
        0 => '$id',
        1 => '$dir',
        2 => '$p',
      ),
      'replace' => '$dir/index$p',
    ),
    'home/special/show/id/dir' => 
    array (
      'find' => 
      array (
        0 => '$id',
        1 => '$dir',
      ),
      'replace' => '$dir/',
    ),
    'home/special/read/id/pinyin/cid/dir' => 
    array (
      'find' => 
      array (
        0 => '$id',
        1 => '$pinyin',
        2 => '$cid',
        3 => '$dir',
      ),
      'replace' => '$dir/$pinyin/',
    ),
    'home/tv/show/id/dir/p' => 
    array (
      'find' => 
      array (
        0 => '$id',
        1 => '$dir',
        2 => '$p',
      ),
      'replace' => '$dir/index$p',
    ),
    'home/tv/show/id/dir' => 
    array (
      'find' => 
      array (
        0 => '$id',
        1 => '$dir',
      ),
      'replace' => '$dir/',
    ),
    'home/up/show/id/dir/month' => 
    array (
      'find' => 
      array (
        0 => '$id',
        1 => '$dir',
        2 => '$month',
      ),
      'replace' => '$dir/$month/',
    ),
    'home/up/show/id/dir/month/p' => 
    array (
      'find' => 
      array (
        0 => '$id',
        1 => '$dir',
        2 => '$month',
        3 => '$p',
      ),
      'replace' => '$dir/$month/$p/',
    ),
    'home/up/show/id/dir/type' => 
    array (
      'find' => 
      array (
        0 => '$id',
        1 => '$dir',
        2 => '$type',
      ),
      'replace' => '$dir/$id/$type/',
    ),
    'home/up/show/id/dir/type/p' => 
    array (
      'find' => 
      array (
        0 => '$id',
        1 => '$dir',
        2 => '$type',
        3 => '$p',
      ),
      'replace' => '$dir/$id/$type/$p/',
    ),
    'home/my/show/id/dir' => 
    array (
      'find' => 
      array (
        0 => '$id',
        1 => '$dir',
      ),
      'replace' => '$dir',
    ),
  ),
  'rewrite_rules' => 
  array (
    1 => 
    array (
      'rewrite' => 'user/reg',
      'url' => 'user-reg-index',
    ),
    2 => 
    array (
      'rewrite' => 'user/regsns',
      'url' => 'user-reg-index-api-1',
    ),
    3 => 
    array (
      'rewrite' => 'user/agreement',
      'url' => 'user-reg-agreement',
    ),
    4 => 
    array (
      'rewrite' => 'user/login',
      'url' => 'user-login-index',
    ),
    5 => 
    array (
      'rewrite' => 'user/qq',
      'url' => 'user-snslogin-qq',
    ),
    6 => 
    array (
      'rewrite' => 'user/weibo',
      'url' => 'user-snslogin-weibo',
    ),
    7 => 
    array (
      'rewrite' => 'user/forgetpwd',
      'url' => 'user-login-forgetpwd',
    ),
    8 => 
    array (
      'rewrite' => 'user/repass/([a-zA-Z0-9\\-]+)',
      'url' => 'user-login-repass-code-$1',
    ),
    9 => 
    array (
      'rewrite' => 'member/',
      'url' => 'user-center-index',
    ),
    10 => 
    array (
      'rewrite' => 'member/info',
      'url' => 'user-center-info',
    ),
    11 => 
    array (
      'rewrite' => 'member/email',
      'url' => 'user-center-email',
    ),
    12 => 
    array (
      'rewrite' => 'member/avatar',
      'url' => 'user-center-avatar',
    ),
    13 => 
    array (
      'rewrite' => 'member/syncs',
      'url' => 'user-center-syncs',
    ),
    14 => 
    array (
      'rewrite' => 'member/pwd',
      'url' => 'user-center-pwd',
    ),
    15 => 
    array (
      'rewrite' => 'member/remind',
      'url' => 'user-center-remind',
    ),
    16 => 
    array (
      'rewrite' => 'member/love',
      'url' => 'user-center-love',
    ),
    17 => 
    array (
      'rewrite' => 'member/cm',
      'url' => 'user-center-cm',
    ),
    18 => 
    array (
      'rewrite' => 'member/gb',
      'url' => 'user-center-gb',
    ),
    19 => 
    array (
      'rewrite' => 'member/playlog',
      'url' => 'user-center-playlog',
    ),
    20 => 
    array (
      'rewrite' => 'member/msg',
      'url' => 'user-center-msg',
    ),
    21 => 
    array (
      'rewrite' => 'member/order',
      'url' => 'user-center-order',
    ),
    22 => 
    array (
      'rewrite' => 'member/score',
      'url' => 'user-center-score',
    ),
    23 => 
    array (
      'rewrite' => 'search/',
      'url' => 'home-search-index',
    ),
    24 => 
    array (
      'rewrite' => 'tv/([0-9]+)',
      'url' => 'home-tv-read-id-$1',
    ),
    25 => 
    array (
      'rewrite' => 'tv/([0-9]+)-([0-9]+)',
      'url' => 'home-tv-read-id-$1-p-$2',
    ),
    26 => 
    array (
      'rewrite' => 'tag/([0-9]+)-(.*)',
      'url' => 'home-tag-show-id-$1',
    ),
    27 => 
    array (
      'rewrite' => 'tag/([0-9]+)-(.*)-([0-9]+)',
      'url' => 'home-tag-show-id-$1-p-$2',
    ),
    28 => 
    array (
      'rewrite' => 'map/([a-zA-Z0-9\\-]+)',
      'url' => 'home-map-show-dir-$1',
    ),
    29 => 
    array (
      'rewrite' => 'gb/',
      'url' => 'home-gb-show',
    ),
    30 => 
    array (
      'rewrite' => 'gb/([0-9]+)',
      'url' => 'home-gb-show-id-$1',
    ),
    31 => 
    array (
      'rewrite' => 'gb/([0-9]+)-([0-9]+)',
      'url' => 'home-gb-show-id-$1-p-$2',
    ),
    32 => 
    array (
      'rewrite' => 'search/(.*)-([0-9]+)',
      'url' => 'home-search-index-wd-$1-p-$2',
    ),
    33 => 
    array (
      'rewrite' => 'search/(.*)',
      'url' => 'home-search-index-wd-$1',
    ),
    34 => 
    array (
      'rewrite' => 'user/([0-9]+)/',
      'url' => 'user-home-index-id-$1',
    ),
    35 => 
    array (
      'rewrite' => 'ac/([0-9]+)/',
      'url' => 'home-vod-read-id-$1',
    ),
    36 => 
    array (
      'rewrite' => 'time/([0-9]+)/',
      'url' => 'home-vod-filmtime-id-$1',
    ),
    37 => 
    array (
      'rewrite' => 'ac/([0-9]+)/news/([0-9]+)/',
      'url' => 'home-vod-news-id-$1-p-$2',
    ),
    38 => 
    array (
      'rewrite' => 'ac/([0-9]+)/news/',
      'url' => 'home-vod-news-id-$1',
    ),
    39 => 
    array (
      'rewrite' => 'ac/([0-9]+)/([a-zA-Z0-9\\-]+)-([0-9]+)',
      'url' => 'home-vod-play-id-$1-sid-$2-pid-$3',
    ),
    40 => 
    array (
      'rewrite' => 'article/([0-9]+)-([0-9]+)',
      'url' => 'home-news-read-id-$1-p-$2',
    ),
    41 => 
    array (
      'rewrite' => 'article/([0-9]+)',
      'url' => 'home-news-read-id-$1',
    ),
    42 => 
    array (
      'rewrite' => 'plot/([0-9]+)/',
      'url' => 'home-story-read-id-$1',
    ),
    43 => 
    array (
      'rewrite' => 'plot/([0-9]+)/([0-9]+)/',
      'url' => 'home-story-read-id-$1-p-$2',
    ),
    44 => 
    array (
      'rewrite' => 'actor/([0-9]+)/',
      'url' => 'home-actor-read-id-$1',
    ),
    45 => 
    array (
      'rewrite' => 'role/([0-9]+)/',
      'url' => 'home-role-read-id-$1',
    ),
    46 => 
    array (
      'rewrite' => 'star/([0-9]+)/',
      'url' => 'home-star-read-id-$1',
    ),
    47 => 
    array (
      'rewrite' => 'star/([0-9]+)/work/([0-9]+)',
      'url' => 'home-star-work-id-$1-p-$2',
    ),
    48 => 
    array (
      'rewrite' => 'star/([0-9]+)/work/',
      'url' => 'home-star-work-id-$1',
    ),
    49 => 
    array (
      'rewrite' => 'star/([0-9]+)/partner/([0-9]+)/',
      'url' => 'home-star-hz-id-$1-p-$2',
    ),
    50 => 
    array (
      'rewrite' => 'star/([0-9]+)/partner/',
      'url' => 'home-star-hz-id-$1',
    ),
    51 => 
    array (
      'rewrite' => 'star/([0-9]+)/news/([0-9]+)/',
      'url' => 'home-star-news-id-$1-p-$2',
    ),
    52 => 
    array (
      'rewrite' => 'star/([0-9]+)/news/',
      'url' => 'home-star-news-id-$1',
    ),
    53 => 
    array (
      'rewrite' => 'star/([0-9]+)/role/([0-9]+)/',
      'url' => 'home-star-role-id-$1-p-$2',
    ),
    54 => 
    array (
      'rewrite' => 'star/([0-9]+)/role/',
      'url' => 'home-star-role-id-$1',
    ),
    55 => 
    array (
      'rewrite' => '([a-zA-Z0-9\\-]+)/index_(.*)_(.*)_(.*)_(.*)_(.*)_(.*)',
      'url' => 'home-vod-type-dir-$1-mcid-$2-area-$3-year-$4-letter-$5-order-$6-p-$7',
    ),
    56 => 
    array (
      'rewrite' => '([a-zA-Z0-9\\-]+)/index_(.*)_(.*)_(.*)_(.*)_(.*)',
      'url' => 'home-vod-type-dir-$1-mcid-$2-area-$3-year-$4-letter-$5-order-$6',
    ),
    57 => 
    array (
      'rewrite' => '([a-zA-Z0-9\\-]+)/(.*)_(.*)_(.*)_(.*)_(.*)_(.*)',
      'url' => 'home-star-type-dir-$1-sex-$2-zy-$3-area-$4-letter-$5-order-$6-p-$7',
    ),
    58 => 
    array (
      'rewrite' => '([a-zA-Z0-9\\-]+)/(.*)_(.*)_(.*)_(.*)_(.*)',
      'url' => 'home-star-type-dir-$1-sex-$2-zy-$3-area-$4-letter-$5-order-$6',
    ),
    59 => 
    array (
      'rewrite' => '([a-zA-Z0-9\\-]+)/index([0-9]+)',
      'url' => 'home-vod-show-dir-$1-p-$2',
    ),
    60 => 
    array (
      'rewrite' => '([a-zA-Z0-9\\-]+)/',
      'url' => 'home-vod-show-dir-$1',
    ),
    61 => 
    array (
      'rewrite' => '([a-zA-Z0-9\\-]+)/index([0-9]+)',
      'url' => 'home-news-show-dir-$1-p-$2',
    ),
    62 => 
    array (
      'rewrite' => '([a-zA-Z0-9\\-]+)/',
      'url' => 'home-news-show-dir-$1',
    ),
    63 => 
    array (
      'rewrite' => '([a-zA-Z0-9\\-]+)/index([0-9]+)',
      'url' => 'home-story-show-dir-$1-p-$2',
    ),
    64 => 
    array (
      'rewrite' => '([a-zA-Z0-9\\-]+)/',
      'url' => 'home-story-show-dir-$1',
    ),
    65 => 
    array (
      'rewrite' => '([a-zA-Z0-9\\-]+)/index([0-9]+)',
      'url' => 'home-actor-show-dir-$1-p-$2',
    ),
    66 => 
    array (
      'rewrite' => '([a-zA-Z0-9\\-]+)/',
      'url' => 'home-actor-show-dir-$1',
    ),
    67 => 
    array (
      'rewrite' => '([a-zA-Z0-9\\-]+)/index([0-9]+)',
      'url' => 'home-role-show-dir-$1-p-$2',
    ),
    68 => 
    array (
      'rewrite' => '([a-zA-Z0-9\\-]+)/',
      'url' => 'home-role-show-dir-$1',
    ),
    69 => 
    array (
      'rewrite' => '([a-zA-Z0-9\\-]+)/index([0-9]+)',
      'url' => 'home-star-show-dir-$1-p-$2',
    ),
    70 => 
    array (
      'rewrite' => '([a-zA-Z0-9\\-]+)/',
      'url' => 'home-star-show-dir-$1',
    ),
    71 => 
    array (
      'rewrite' => '([a-zA-Z0-9\\-]+)/index([0-9]+)',
      'url' => 'home-special-show-dir-$1-p-$2',
    ),
    72 => 
    array (
      'rewrite' => '([a-zA-Z0-9\\-]+)/',
      'url' => 'home-special-show-dir-$1',
    ),
    73 => 
    array (
      'rewrite' => '([a-zA-Z0-9\\-]+)/([a-zA-Z0-9\\-]+)/',
      'url' => 'home-special-read-dir-$1-pinyin-$2',
    ),
    74 => 
    array (
      'rewrite' => '([a-zA-Z0-9\\-]+)/index([0-9]+)',
      'url' => 'home-tv-show-dir-$1-p-$2',
    ),
    75 => 
    array (
      'rewrite' => '([a-zA-Z0-9\\-]+)/',
      'url' => 'home-tv-show-dir-$1',
    ),
    76 => 
    array (
      'rewrite' => '([a-zA-Z0-9\\-]+)/([0-9]+)/',
      'url' => 'home-up-show-dir-$1-month-$2',
    ),
    77 => 
    array (
      'rewrite' => '([a-zA-Z0-9\\-]+)/([0-9]+)/([0-9]+)/',
      'url' => 'home-up-show-dir-$1-month-$2-p-$3',
    ),
    78 => 
    array (
      'rewrite' => '([a-zA-Z0-9\\-]+)/([0-9]+)/([a-zA-Z0-9\\-]+)/',
      'url' => 'home-up-show-dir-$1-id-$2-type-$3',
    ),
    79 => 
    array (
      'rewrite' => '([a-zA-Z0-9\\-]+)/([0-9]+)/([a-zA-Z0-9\\-]+)/([0-9]+)/',
      'url' => 'home-up-show-dir-$1-id-$2-type-$3-p-$4',
    ),
    80 => 
    array (
      'rewrite' => '([a-zA-Z0-9\\-]+)',
      'url' => 'home-my-show-dir-$1',
    ),
  ),
);
?>